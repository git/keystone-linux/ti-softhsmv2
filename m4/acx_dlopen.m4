# $Id: acx_dlopen.m4 4829 2010-07-29 11:29:33Z rb $

AC_DEFUN([ACX_DLOPEN],[
  AC_CHECK_FUNC(dlopen, [AC_DEFINE(HAVE_DLOPEN,1,[Define if you have dlopen])],
  [
    AC_CHECK_LIB([dl],[dlopen], 
      [AC_DEFINE(HAVE_DLOPEN,1,[Define if you have dlopen])
      LIBS="$LIBS -ldl"],
      [AC_CHECK_FUNC(LoadLibrary, 
        [if test $ac_cv_func_LoadLibrary = yes; then
          AC_DEFINE(HAVE_LOADLIBRARY, 1, [Whether LoadLibrary is available])
        fi
        ], [AC_MSG_ERROR(No dynamic library loading support)]
      )]
    )
  ])
])
