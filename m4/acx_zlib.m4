# $Id$

AC_DEFUN([ACX_ZLIB],[
	AC_ARG_WITH(zlib,
        	AC_HELP_STRING([--with-zlib=PATH],[Specify prefix of path of zlib]),
		[
			ZLIB_PATH="$withval"
		],
		[
			ZLIB_PATH="/usr"
		])

	AC_MSG_CHECKING(what are the zlib includes)
	ZLIB_INCLUDES="-I$ZLIB_PATH/include"
	AC_MSG_RESULT($ZLIB_INCLUDES)

	AC_MSG_CHECKING(what are the zlib libs)
	ZLIB_LIBS="-lz"
	AC_MSG_RESULT($BOTAN_LIBS)

	tmp_CPPFLAGS=$CPPFLAGS
	tmp_LIBS=$LIBS

	CPPFLAGS="$CPPFLAGS $ZLIB_INCLUDES"
	LIBS="$LIBS $ZLIB_LIBS"

	AC_LANG_PUSH([C++])
	AC_LINK_IFELSE(
		[AC_LANG_PROGRAM(
			[#include <zlib.h>
			#include <zconf.h>],
		)],
		[AC_MSG_RESULT([checking for zlib >= v$1.$2.$3 ... yes])],
		[AC_MSG_RESULT([checking for zlib >= v$1.$2.$3 ... no])
		 AC_MSG_ERROR([Missing the correct version of the zlib library])]
	)
	AC_LANG_POP([C++])

	CPPFLAGS=$tmp_CPPFLAGS
	LIBS=$tmp_LIBS

	AC_SUBST(ZLIB_INCLUDES)
	AC_SUBST(ZLIB_LIBS)
])
