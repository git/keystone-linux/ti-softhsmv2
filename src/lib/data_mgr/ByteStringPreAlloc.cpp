/* $Id: ByteString.cpp 54 2010-04-14 12:10:06Z rijswijk $ */

/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * Copyright (c) 2010 SURFnet bv
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
  ByteStringPreAlloc.h

  A string class derived from byte strings to manage externally allocated data
 *****************************************************************************/

#include <algorithm>
#include <string>
#include <stdio.h>
#include "config.h"
#include "log.h"
#include "ByteStringPreAlloc.h"

// Constructor
ByteStringPreAlloc::ByteStringPreAlloc(unsigned char* bytes, size_t maxLength, size_t currentLength /*= 0*/)
: bytes (bytes), maxLength (maxLength), currentLength (currentLength)
{

}

// Append data
ByteStringPreAlloc& ByteStringPreAlloc::operator+=(const ByteStringPreAlloc& append)
{
	if (currentLength + append.size() > maxLength) {
		throw -2;
	}

	memcpy(&byte_str()[currentLength], append.const_byte_str(), append.size());

	currentLength += append.size();

	return *this;
}

ByteStringPreAlloc& ByteStringPreAlloc::operator+=(const unsigned char byte)
{
	if (currentLength + 1 > maxLength) {
		throw -2;
	}

	byte_str()[currentLength] = byte;

	currentLength += 1;

	return *this;
}

// Array operator
unsigned char& ByteStringPreAlloc::operator[](size_t pos)
{
	if (pos > maxLength) throw -2;

	return byte_str()[pos];
}

// Return the byte string data
unsigned char* ByteStringPreAlloc::byte_str()
{
	return &bytes[0];
}

// Return the const byte string
const unsigned char* ByteStringPreAlloc::const_byte_str() const
{
	return (const unsigned char*) &bytes[0];
}

// The size of the byte string in bytes
size_t ByteStringPreAlloc::size() const
{
	return currentLength;
}

void ByteStringPreAlloc::resize(const size_t newSize)
{
	if (newSize > maxLength) throw -2;

	currentLength = newSize;
}

