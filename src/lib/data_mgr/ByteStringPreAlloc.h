/* $Id: ByteStringPreAlloc.h $ */

/*
 * Copyright (c) 2013 Texas Instruments
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
  ByteStringPreAlloc.h

  A string class derived from byte strings to manage externally allocated data
 *****************************************************************************/

#ifndef _SOFTHSM_V2_BYTESTRINGPREALLOC_H
#define _SOFTHSM_V2_BYTESTRINGPREALLOC_H

#include "ByteString.h"

class ByteStringPreAlloc: public ByteString
{
public:
	// Constructors
	ByteStringPreAlloc(unsigned char* bytes, size_t maxLength, size_t currentLength = 0);

	// Destructor
	virtual ~ByteStringPreAlloc() { }

	// Append data
	virtual ByteStringPreAlloc& operator+=(const ByteStringPreAlloc& append);
	virtual ByteStringPreAlloc& operator+=(const unsigned char byte);

	// Array operator
	virtual unsigned char& operator[](size_t pos);

	// Return the byte string
	virtual unsigned char* byte_str();

	// Return the const byte string
	virtual const unsigned char* const_byte_str() const;

	// Return the size in bytes
	virtual size_t size() const;

	// Resize
	virtual void resize(const size_t newSize);

private:
	unsigned char *bytes;
	int currentLength;
	int maxLength;

};

#endif // !_SOFTHSM_V2_BYTESTRINGPREALLOC_H

