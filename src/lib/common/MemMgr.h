//----------------------------------------------------------------
// Statically-allocated memory manager
//
// by Eli Bendersky (eliben@gmail.com)
//  
// This code is in the public domain.
//----------------------------------------------------------------
// NOTE: Modified by TI to wrap into C++ class

#ifndef __MEMMGR_H__
#define __MEMMGR_H__

#include "MapRegion.h"
#include "MutexFactory.h"
#include <memory>

//
// Memory manager: dynamically allocates memory from 
// a fixed pool that is allocated statically at link-time.
// 
// Usage: after calling memmgr_init() in your 
// initialization routine, just use memmgr_alloc() instead
// of malloc() and memmgr_free() instead of free().
// Naturally, you can use the preprocessor to define 
// malloc() and free() as aliases to memmgr_alloc() and 
// memmgr_free(). This way the manager will be a drop-in 
// replacement for the standard C library allocators, and can
// be useful for debugging memory allocation problems and 
// leaks.
//
// Preprocessor flags you can define to customize the 
// memory manager:
//
// POOL_SIZE
//    Size of the pool for new allocations. This is 
//    effectively the heap size of the application, and can 
//    be changed in accordance with the available memory 
//    resources.
//
// MIN_POOL_ALLOC_QUANTAS
//    Internally, the memory manager allocates memory in
//    quantas roughly the size of two ulong objects. To
//    minimize pool fragmentation in case of multiple allocations
//    and deallocations, it is advisable to not allocate
//    blocks that are too small.
//    This flag sets the minimal ammount of quantas for 
//    an allocation. If the size of a ulong is 4 and you
//    set this flag to 16, the minimal size of an allocation
//    will be 4 * 2 * 16 = 128 bytes
//    If you have a lot of small allocations, keep this value
//    low to conserve memory. If you have mostly large 
//    allocations, it is best to make it higher, to avoid 
//    fragmentation.
//
// 

#define MEMMGR_POOL_SIZE (38*1024)
#define MIN_POOL_ALLOC_QUANTAS 2

typedef unsigned char byte;
typedef unsigned long ulong;
typedef ulong Align;

union mem_header_union
{
    struct 
    {
        // Pointer to the next block in the free list
        //
        union mem_header_union* next;

        // Size of the block (in quantas of sizeof(mem_header_t))
        //
        ulong size; 
    } s;

    // Used to align headers in memory to a boundary
    //
    Align align_dummy;
};

typedef union mem_header_union mem_header_t;

class MemMgr
{
	public:
		// Return the one-and-only instance
		static MemMgr* i();

		// Destructor
		virtual ~MemMgr();

		// Check if the MemMgr is valid
		bool isValid()
		{
			return valid;
		}

		// memory alloc
		void* alloc(ulong nBytes);

		// free
		void free(void* ap);

		// Prints statistics about the current state of the memory mgr
		void printStats();

	private:
		// Constructor
		MemMgr();

		static std::auto_ptr<MemMgr> instance;

		// status
		bool valid;

		mem_header_t base;

		mem_header_t* freep;

		byte *pool;

		MapRegion *map;

        Mutex* mutex;

		ulong pool_free_pos;

		mem_header_t *getMemFromPool(ulong nquantas);
};

#endif // __MEMMGR_H__

