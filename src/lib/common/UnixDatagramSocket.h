/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <cerrno>
#include <sys/socket.h>
#include <sys/un.h>
#include <string>

#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108
#endif

class UnixDatagramSocket
{
public:
	/* Send only socket */
	UnixDatagramSocket();

	/* Send recv socket */
	UnixDatagramSocket(const std::string socketName);

	UnixDatagramSocket(const struct sockaddr_un *addr);

	~UnixDatagramSocket();

	int sendData (const char *data, int dataLen, const std::string toSocketName);
	
	int sendData (const char *data, int dataLen, const struct sockaddr_un *to);

	/* Returns: 1 => success (external FD), 0 => success, -1 => error, -2 => timeout */
	int waitData (int &datagramSize, struct timeval *timeOut = NULL, int externalFD = -1);

	int recvData (char *data, int dataLen, struct sockaddr_un *from = 0, bool peek = false);

	std::string errnoStr() {return strerror(errnoVal);}

	int errnoVal;

private:
	int fd;
	bool bind;
	fd_set  readfds;
	struct sockaddr_un localAddr;

	void socketInit ();
	int checkAndCreatePath(const std::string path);
};
