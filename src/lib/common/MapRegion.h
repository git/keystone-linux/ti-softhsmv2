/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _SOFTHSM_V2_MAPREGION_H
#define _SOFTHSM_V2_MAPREGION_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "config.h"
#include "log.h"

typedef enum lockType {
	lockTypeRead,
	lockTypeWrite,
} lockType_e;

typedef enum rwMode {
	readMode = O_RDONLY | O_SYNC,
	readWriteMode = O_RDWR | O_SYNC,
} rwMode_e;

class MapRegion {
public:
	MapRegion(void *address, off_t length, rwMode mode)
	{
		int map_mode;
		off_t page_size, mask;

		if (mode == readMode) {
			map_mode = PROT_READ;
		} else if (mode == readWriteMode) {
			map_mode = PROT_READ | PROT_WRITE;
		} else {
			ERROR_MSG ("Invalid mode parameter 0x%x", mode);
			goto close_n_exit;
		}

		fd = open("/dev/mem", mode);
		if (fd == -1) {
			ERROR_MSG ("Can't open /dev/mem (%s)", strerror(errno));
			goto close_n_exit;
		}

		page_size = sysconf(_SC_PAGESIZE);
		mask = page_size - 1;

		mapLength = length + ((off_t) address & mask);

		vbaddr = mmap(0, mapLength, map_mode, MAP_SHARED, fd, (off_t) address & ~mask);
		if(vbaddr == (void *) -1) {
			ERROR_MSG ("mmap failed for address 0x%x (%s)", address, strerror(errno));
			goto close_n_exit;
		}

		vaddr = (void *) ((unsigned long int)vbaddr + ((unsigned long int)address & mask));

		userLength = length;

		valid = true;
		locked = false;
		return;
close_n_exit:
		valid = false;
		if (vbaddr != MAP_FAILED) munmap(vbaddr, mapLength);
		if (fd != -1) close(fd);
		locked = false;
		vbaddr = NULL;
		vaddr  = NULL;
		mapLength = 0;
		userLength = 0;
	}

	MapRegion(const std::string &fileName, rwMode mode)
	{
		struct stat stat;
		int map_mode;
		int open_mode = (int) mode;

		if (mode == readMode) {
			map_mode = PROT_READ;
		} else if (mode == readWriteMode) {
			map_mode = PROT_READ | PROT_WRITE;
			open_mode |= O_CREAT;
		} else {
			ERROR_MSG ("Invalid mode parameter 0x%x", mode);
			goto close_n_exit;
		}

		fd = open (fileName.c_str(), open_mode, (S_IRWXU | S_IRWXG | S_IRWXO));
		if (fd == -1) {
			ERROR_MSG("Image file open error (%s)", strerror(errno));
			goto close_n_exit;
		}

		if (fstat (fd, &stat) == -1) {
			ERROR_MSG("Error getting file stat for %s (%s)", fileName.c_str(), strerror(errno));
			goto close_n_exit;
		}

		if (!S_ISREG (stat.st_mode)) {
			ERROR_MSG("%s is not a regular file", fileName.c_str());
			goto close_n_exit;
		}

		mapLength = userLength = stat.st_size;

		vaddr = vbaddr = (char *)mmap (0, mapLength, map_mode, MAP_SHARED, fd, 0);
		if (vbaddr == MAP_FAILED) {
			ERROR_MSG("MMAP failed for file %s (%s)", fileName.c_str(), strerror(errno));
			goto close_n_exit;
		}

		valid = true;
		locked = false;
		return;
close_n_exit:
		valid = false;
		if (vbaddr != MAP_FAILED) munmap(vbaddr, mapLength);
		if (fd != -1) close(fd);
		locked = false;
		vbaddr = NULL;
		vaddr  = NULL;
		mapLength = 0;
		userLength = 0;
	}

	~MapRegion()
	{
		if (!valid) return;
		unlock();
		munmap(vbaddr, mapLength);
		close (fd);

	}

	void *getPtr()
	{
		return (valid) ? vaddr : 0;
	}

	int getLength()
	{
		return (valid) ? userLength : 0;
	}

	bool isValid()
	{
		return valid;
	}

	bool isLocked()
	{
		return (valid) ? locked : false;
	}

	bool lock(bool block = true, lockType_e type = lockTypeRead)
	{
		struct flock fl;
		fl.l_type = (type == lockTypeWrite) ? F_WRLCK : F_RDLCK;
		fl.l_whence = SEEK_SET;
		fl.l_start = 0;
		fl.l_len = 0;
		fl.l_pid = 0;

		if (locked || !valid) return false;

		if (fcntl(fd, block ? F_SETLKW : F_SETLK, &fl) != 0)
		{
			ERROR_MSG("Could not lock the file: %s", strerror(errno));
			return false;
		}

		locked = true;

		return true;
	}

	bool unlock()
	{
		struct flock fl;
		fl.l_type = F_UNLCK;
		fl.l_whence = SEEK_SET;
		fl.l_start = 0;
		fl.l_len = 0;
		fl.l_pid = 0;

		if (!locked || !valid) return false;

		if (fcntl(fd, F_SETLK, &fl) != 0)
		{
			valid = false;

			ERROR_MSG("Could not unlock the file: %s", strerror(errno));
			return false;
		}

		locked = false;

		return valid;
	}

private:

	bool locked;

	bool valid;

	int fd;

	void *vbaddr;

	void *vaddr;

	int mapLength;

	int userLength;

};

#endif
