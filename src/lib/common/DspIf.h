/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*****************************************************************************/

#ifndef DSPIF_H
#define DSPIF_H


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ByteString.h>
#include <MapRegion.h>


//Commands for DSP shared memory
//Commands for struct  sec_if_area->header.command
#define SEC_COMAMND_GENERATE_RSA_KEYPAIR 1
#define SEC_COMAMND_RSA_METHOD 2
#define SEC_COMMAND_KEYSTORE_HASH 3
#define SEC_COMMAND_WRAP_RSA_KEY 4

//Sub commands for
//RSA_privkey_algo_params->header.RSA_method
#define RSA_METHOD_DECRYPT_INIT 1
#define RSA_METHOD_DECRYPT_UPDATE 2
#define RSA_METHOD_DECRYPT_FINAL 3
#define RSA_METHOD_SIGN_INIT 4
#define RSA_METHOD_SIGN_UPDATE 5
#define RSA_METHOD_SIGN_FINAL 6
#define RSA_METHOD_UNINSTALL_KEY 7

#define HMACSHA1_LEN 20

const uint32_t MAGIC = 0xBABE5EED;

//Status DSP <-> ARM
#define SEC_DSP_IDLE            0
#define SEC_DSP_PROCESSING      1
#define SEC_ARM_PENDING_RESP    0
#define SEC_ARM_FINISHED        1

//DSP errocodes
const uint32_t OK=0;
const uint32_t outOfMem=101;
const uint32_t headerCRC=102;
const uint32_t payloadCRC=103;
const uint32_t unkonwCommand=104;
const uint32_t unkonwSubCommand=105;


//Shared memory
typedef struct sec_if_area_status {
    uint32_t    armstatus;
    uint32_t    dspstatus;
}sec_if_area_status;

typedef struct sec_if_area_header {
    uint32_t    headerAdler; //adler, magic, command, inDataLen, outDataLen
    uint32_t    payloadAdler; //payload
    uint32_t    magic;
    uint32_t    command;
    uint32_t    dsperrorcode;
    uint32_t    inDataLen;
    uint32_t    outDataLen;
}sec_if_area_header;

typedef struct sec_if_area {
    sec_if_area_status status;
    sec_if_area_header header;
    uint8_t     inData[16384];
    uint8_t     outData[16384];
}sec_if_area;


// Parameters, results and return values for DSP
//algorithms
typedef enum algo_status{
    fail=0,
    pass,
    status_not_used
}algo_status;

typedef struct RSA_keypair_header {
    int errorcode;
    uint16_t dataSz;
}RSA_keypair_header;

typedef struct RSA_keypair {
    RSA_keypair_header header;
    uint8_t data[16382];
}RSA_keypair;

typedef struct RSA_keypair_params{
    int keySizeInOctects;
        uint16_t maxBufferSz;
}RSA_keypair_params;


typedef struct RSA_privkey_algo_params_header {
    uint32_t RSA_method;
    uint16_t dataSz;
}RSA_privkey_algo_params_header;

typedef struct RSA_privkey_algo_params {
    RSA_privkey_algo_params_header header;
    uint8_t data[16380];
}RSA_privkey_algo_params;

typedef struct RSA_privkey_algo_result_header {
    int errorcode;
    algo_status algoStatus;
    uint16_t dataSz;
}RSA_privkey_algo_result_header;

typedef struct RSA_privkey_algo_result {
    RSA_privkey_algo_result_header header;
    uint8_t data[8182];
}RSA_privkey_algo_result;

typedef struct keystoreHashParams {
    uint32_t keystoreSz;
    uint32_t datain;
}keystoreHashParams;
typedef struct keystoreHashResult {
    int errorcode;
    uint8_t hash[HMACSHA1_LEN];
}keystoreHashResult;

typedef struct RSA_importkey_params {

    uint8_t modulus[257];
    uint8_t privexp[257];
    uint8_t prime1[129];
    uint8_t prime2[129];
    uint8_t exp1[129];
    uint8_t exp2[129];
    uint8_t coef[129];
    uint8_t pubexp[4];

    uint16_t maxWrappedSz;
    uint16_t modulusSz;
    uint16_t privexpSz;
    uint16_t prime1Sz;
    uint16_t prime2Sz;
    uint16_t exp1Sz;
    uint16_t exp2Sz;
    uint16_t coefSz;
    uint16_t pubexpSz;
}RSA_importkey_params;

typedef struct RSA_importkey_result_header{
    int errorcode;
    uint16_t dataSz;
}RSA_importkey_result_header;

typedef struct RSA_importkey_result{
    RSA_importkey_result_header header;
    uint8_t data[16382];
}RSA_importkey_result;



//
typedef enum{
    NONE=0,
    INTEGER =0x02,
    BITSTRING=0x03,
    ASN_NULL = 0x05,
    OBJ_IDENT =0x06,
    SEQUENCE=0x30
}ASN_TYPE;



class SecureDSP
{
public:
    SecureDSP();
    ~SecureDSP();
    bool generateKey(ByteString& privateKey, ByteString& publicKey, ByteString& publicExp, uint32_t keylen);
    bool wrapKey(   ByteString& privateKey,
                                ByteString& pubexp,
                                ByteString& modulus,
                                ByteString& privexp,
                                ByteString& prime1,
                                ByteString& prime2,
                                ByteString& exp1,
                                ByteString& exp2,
                                ByteString& coef);

    int32_t sign(ByteString& privateKey, const ByteString& dataToSign, ByteString& signature, uint32_t const keysize, uint32_t padding);
    int32_t decrypt(ByteString& privateKey, const ByteString& encryptedData, ByteString& decryptedData, uint32_t const keysize);
    bool hashInit();
    bool hashUpdate(const ByteString& data);
    bool hashFinal(ByteString& hashedData);
private:
    void            *memBase;
    MapRegion       *m_dspmap;
    sec_if_area     *m_interfaceArea;
    bool             m_initdone;
};


#endif // DSPIF_H
