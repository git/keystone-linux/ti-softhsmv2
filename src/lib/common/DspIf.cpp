/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*****************************************************************************/

#include "DspIf.h"
#include <Configuration.h>
#include <log.h>
#include <zlib.h>

#ifdef __arm__
#define TARGETBUILD
#endif

typedef struct{
    uint8_t *ptr;
    uint32_t length;
}RSA_KEY_ELEMS;

const char DSPstatusStr[][20]={{"SEC_DSP_IDLE"},{"SEC_DSP_PROCESSING"}};

/*****************************************************************************/
/*****************************************************************************/
static inline bool dspStatusIdle(sec_if_area const * const ifarea)
{
    return ifarea->status.dspstatus == SEC_DSP_IDLE ? true:false;
}
/*****************************************************************************/
static inline bool armStatusIdle(sec_if_area const * const ifarea)
{
    return ifarea->status.armstatus==SEC_ARM_FINISHED ? true : false;
}
/*****************************************************************************/
static inline void setArmStatus(sec_if_area * const ifarea, uint32_t const status)
{
    ifarea->status.armstatus=status;
}
void waitDspDone(sec_if_area * const ifarea)
{
    uint32_t waitcount = 0;
    uint32_t prevStatus;
    volatile uint32_t* status= &(ifarea->status.dspstatus);
    volatile uint32_t* cmd= &(ifarea->header.command);
    prevStatus = *status;
    while(1){
        if((*status==SEC_DSP_IDLE) && (*cmd==0)){
            //In case we missed the DSP status change.
            break;
        }
        if(*status != prevStatus) {
            DEBUG_MSG("DSP status %s -> %s",DSPstatusStr[prevStatus],DSPstatusStr[*status]);
            prevStatus = *status;
            if(*status == SEC_DSP_IDLE )break;
        }
        usleep(10000); /*Give execution time to other processes*/
        waitcount++;
        if(waitcount > 6000) {
            /*Max time to wait ~60sec */
            //Another timeout defined in SoftHSM-client.cpp: SoftHSM::MsgSendRecv
            //We must beat the SoftHSM client timeout in order to give meaningfull
            //error messages / keep the state consistent.
            ERROR_MSG("DSP timeout!");
            setArmStatus(ifarea, SEC_ARM_FINISHED);
            break;
        }
    }
}

/*****************************************************************************/
int32_t initShm(sec_if_area * const ifarea, bool const forced)
{
    uint32_t i;

    if(ifarea->status.dspstatus == SEC_DSP_IDLE && ifarea->status.armstatus==SEC_ARM_FINISHED) {
        ifarea->header.inDataLen=0xffffffff;
        ifarea->header.outDataLen=0xffffffff;

        memset(ifarea->inData,0xff,sizeof(ifarea->inData));
        memset(ifarea->outData,0xff,sizeof(ifarea->outData));

    }else{
        ERROR_MSG("initSHM failed. DSP busy=%d arm busy = %d",ifarea->status.dspstatus,ifarea->status.armstatus);
        return -1;
    }
    if(forced){
        setArmStatus(ifarea, SEC_ARM_FINISHED);
    }
    DEBUG_MSG("shminit done");
    return 1;
}
/*****************************************************************************/
int32_t sendMsgWait(sec_if_area * const ifarea, uint32_t const cmd,uint8_t const * const payload, uint16_t const payloadSz)
{
    int32_t i,ret = -1;
    sec_if_area_header header;

    header.dsperrorcode = OK;
    header.payloadAdler = adler32(0,(uint8_t*)payload,payloadSz);
    header.magic = MAGIC;
    header.command = cmd;
    header.inDataLen = payloadSz;
    header.outDataLen = 0;
    header.headerAdler = 0;
    header.headerAdler = adler32(0,(uint8_t*)&header,sizeof(sec_if_area_header));

    //Sanity check
    if(ifarea->status.dspstatus == SEC_DSP_IDLE && ifarea->status.armstatus==SEC_ARM_PENDING_RESP){

        ifarea->header.headerAdler = header.headerAdler;
        ifarea->header.payloadAdler = header.payloadAdler;
        ifarea->header.magic = header.magic;
        ifarea->header.inDataLen = header.inDataLen;
        ifarea->header.outDataLen = header.outDataLen;

        for(i=0;i<payloadSz;i++){
            ifarea->inData[i] = payload[i];
        }


        DEBUG_MSG("write cmd to SHM");
        ifarea->header.command = header.command;
        DEBUG_MSG("Wait DSP");
        waitDspDone(ifarea);
        DEBUG_MSG("DSP done");
        //nuke the payload from shared mem.
        memset(ifarea->inData,0,payloadSz );
        ret = 0;
    }else {
        ERROR_MSG("ERROR: sendMsg wrong state");
    }
    return ret;
}
/*****************************************************************************/
int32_t receiveMsg(sec_if_area * const ifarea, uint32_t const cmd,uint8_t * const payload, uint16_t * const payloadSz)
{
    uint32_t i;
    int32_t ret = -1;
    sec_if_area_header header;
    uint32_t headerAdler;

    //Sanity check
    if(ifarea->status.dspstatus == SEC_DSP_IDLE && ifarea->status.armstatus==SEC_ARM_PENDING_RESP) {
        //copy the header from SHM to local header
        headerAdler = ifarea->header.headerAdler;
        header.headerAdler = 0;
        header.payloadAdler = ifarea->header.payloadAdler;
        header.magic = ifarea->header.magic;
        header.command = cmd; //header adler is calculated with the command, not '0'
        header.dsperrorcode = ifarea->header.dsperrorcode;
        header.inDataLen = ifarea->header.inDataLen;
        header.outDataLen = ifarea->header.outDataLen;

        //check the header CRC
        if(headerAdler != adler32(0,(uint8_t*)&header,sizeof(sec_if_area_header)) ){

            *payloadSz = 0;
            ERROR_MSG("ERROR receiveMsg header CRC error");
            return -1;
        }

        //Don't overflow the RX buffer
        if(*payloadSz<header.outDataLen){
            *payloadSz = header.outDataLen;
            return -2;
        }

        //Copy the payload & nuke the payload from shared mem.
        for(i=0;i<header.outDataLen;i++) {
            payload[i]=ifarea->outData[i];
            ifarea->outData[i] = 0;
        }
        *payloadSz = i;
        //check the payload CRC
        if(header.payloadAdler != adler32(0,payload,header.outDataLen)){
            ERROR_MSG("ERROR receiveMsg payload CRC error");
            *payloadSz = 0;
            return -3;
        }
        DEBUG_MSG("receiveMsg OK %d bytes",header.outDataLen);
        ret = 0;
    }else{
        ERROR_MSG("ERROR: receiveMsg wrong state");
    }

    return ret;
}
/*****************************************************************************/
int32_t keypairSize(uint32_t const keySize)
{
    //Assume worst case 2048bit key
    // bytes for wrapped private key asn.1
    int32_t keySz;
    uint16_t tmp;

    //asn.1 sequence 4bytes (common pub & priv)
    keySz=4;


    //wrapped private key
    //asn.1 sequence 4bytes (inside wrapped priv)
    keySz+=4;
    //algorithm version
    keySz+=1;
    //der encoding for algorithm version
    keySz+=2;

    //modulus = keySize+1
    keySz+=keySize+1;
    //der encoding for modulus(inside private wrapped key)
    keySz+=4;

    //pub exponent: 3bytes
    keySz+=3;
    //der encoding for pub exponent (inside wrapped private key)
    keySz+=2;

    //private exponent: keySize+1
    keySz+=keySize+1;
    //der encoding for private exponent (inside wrapped private key)
    keySz+=4;

    //prime1: keySize / 2 +1
    //prime2: keySize / 2 +1
    keySz+=keySize+2;
    //der encoding for prime1 (inside wrapped private key)
    keySz+=4;
    //der encoding for prime2 (inside wrapped private key)
    keySz+=4;


    //exponent1: keySize / 2
    //exponent2: keySize / 2 +1
    keySz+=keySize+1;
    //der encoding for exp1 (inside wrapped private key)
    keySz+=4;
    //der encoding for exp2 (inside wrapped private key)
    keySz+=4;

    //coefficient: keySize / 2 +1
    keySz+=(keySize/2) +1;
    //der encoding for coefficient (inside wrapped private key)
    keySz+=4;

    //keywrap private key: round up to keywrap block size.
    tmp=keySz&7;
    if(tmp) tmp=8-tmp;
    keySz+=tmp;

    //der encoding for wrapped private key
    keySz+=4;

    //public key
    //pub exponent: 3bytes
    keySz+=3;
    //der encoding for pub exponent (public key)
    keySz+=2;

    //modulus = keySize +1
    keySz+=keySize+1;
    //der encoding for modulus(public key)
    keySz+=4;

    return keySz;
}

/*****************************************************************************/
int32_t runPrivkeyAlgo(sec_if_area * const ifarea, uint32_t const command, bool const waitResp, uint8_t const * const dataIn,uint16_t const dataInSz,uint8_t * const dataOut, uint16_t * const dataOutSz)
{
    int32_t ret=-1;

    uint8_t* buffer;
    uint8_t* pBuffer;
    buffer = (uint8_t*)malloc(sizeof(RSA_privkey_algo_params_header)+dataInSz);

    RSA_privkey_algo_result_header result;
    RSA_privkey_algo_result_header* pResult=NULL;
    uint8_t* payload=NULL;
    uint16_t payloadSz;

    pBuffer = buffer;
    if(buffer==NULL){
        ERROR_MSG("runPrivkeyAlgo out of memory");
        return -1;
    }
    DEBUG_MSG("runPrivkeyAlgo reading from dsp, magic value: 0x%X", ifarea->header.magic);

    if(dspStatusIdle(ifarea) && armStatusIdle(ifarea)) {
        setArmStatus(ifarea, SEC_ARM_PENDING_RESP);

        RSA_privkey_algo_params_header params;
        params.RSA_method = command;
        params.dataSz = dataInSz;
        memcpy(pBuffer,&params,sizeof(RSA_privkey_algo_params_header));
        pBuffer = buffer + sizeof(RSA_privkey_algo_params_header);
        memcpy(pBuffer,dataIn,dataInSz);

        ret =  sendMsgWait(ifarea, SEC_COMAMND_RSA_METHOD,buffer, sizeof(RSA_privkey_algo_params_header)+dataInSz);
        free(buffer);
        buffer=NULL;
        pBuffer=NULL;
        if(ret==0){
                payloadSz = sizeof(RSA_privkey_algo_result_header);
                ret = receiveMsg(ifarea, SEC_COMAMND_RSA_METHOD,(uint8_t*)&result, &payloadSz);
                if(ret==0){
                    DEBUG_MSG("no payload, only RSA_privkey_algo_result_header");
                    DEBUG_MSG("errorcode=%d",result.errorcode );
                    DEBUG_MSG("algoStatus=%d",result.algoStatus );
                    DEBUG_MSG("dataSz=%d",result.dataSz );
                    //Check the algo return value
                    DEBUG_MSG("DSP return value=%d (",result.errorcode);

                    if(result.errorcode==OK){
                        //Success OK.
                        if(dataOutSz)
                            *dataOutSz = result.dataSz;
                        ret =0;
                    }else{
                        if(dataOutSz)
                            *dataOutSz=0;
                        ret = result.errorcode;
                    }
                }else if(ret == -2 && dataOut && dataOutSz){
                    DEBUG_MSG("payload size=%d",payloadSz);
                    payload = (uint8_t*)malloc(payloadSz);
                    if(!payload){
                        ERROR_MSG("runPrivkeyAlgo out of memory");
                        *dataOutSz=0;
                        ret = -1;
                    }else{
                        ret = receiveMsg(ifarea, SEC_COMAMND_RSA_METHOD,(uint8_t*)payload, &payloadSz);
                        if(ret==0){
                            pResult = (RSA_privkey_algo_result_header*)payload;
                            DEBUG_MSG("errorcode=%d",pResult->errorcode );
                            DEBUG_MSG("algoStatus=%d",pResult->algoStatus );
                            DEBUG_MSG("dataSz=%d",pResult->dataSz );
                            //Dont overflow RX buffer
                            if(*dataOutSz >= pResult->dataSz){
                                memcpy(dataOut,payload+sizeof(RSA_privkey_algo_result_header),pResult->dataSz);
                            }else{
                                ERROR_MSG("runPrivkeyAlgo RX buffer too small %d <-> %d",*dataOutSz, pResult->dataSz);
                                ret = -1;
                            }
                            *dataOutSz = pResult->dataSz;
                        }
                    }
                }else {
                    ERROR_MSG("runPrivkeyAlgo %d bytes received but ret=%d dataOut=%p dataOutSz=%p",payloadSz, ret,dataOut,dataOutSz);
                    if(dataOutSz)
                        *dataOutSz=0;
                }
        }
    }else{
        ERROR_MSG("runPrivkeyAlgo DSP busy");
    }
    if(ret || waitResp == false){
        setArmStatus(ifarea, SEC_ARM_FINISHED);
    }
    free(buffer);
    buffer=NULL;
    free(payload);
    return ret;
}
/*****************************************************************************/
void unInstallKey(sec_if_area * const ifarea)
{
    int32_t ret;
    ret = runPrivkeyAlgo(ifarea, RSA_METHOD_UNINSTALL_KEY, false,NULL,0,NULL,NULL);
    if(ret){
        ERROR_MSG("unInstallKey failed");
    }else {
        DEBUG_MSG("unInstallKey success");
    }
    return;
}
/*****************************************************************************/

int32_t runRsaKeypairAlgo(sec_if_area * const ifarea, int32_t const keySize,uint8_t * const keypairDer, uint16_t* const keypairDerSz)
{
    int32_t ret = -1;

    RSA_keypair_params rsaKeypairParams;
    uint8_t* payload;
    uint16_t payloadSz;
    RSA_keypair* keypair;
    payloadSz =sizeof(RSA_keypair_header) + keypairSize(keySize)+2;
    payload = (uint8_t*)malloc(payloadSz);
    if(payload){
        DEBUG_MSG("reading from dsp, magic value: 0x%X", ifarea->header.magic);

        if(dspStatusIdle(ifarea) && armStatusIdle(ifarea)) {
            setArmStatus(ifarea, SEC_ARM_PENDING_RESP);

            keypair = (RSA_keypair*)payload;

            rsaKeypairParams.keySizeInOctects=keySize;
            rsaKeypairParams.maxBufferSz=*keypairDerSz;

            DEBUG_MSG("Start RSA keypair generation");

            sendMsgWait(ifarea, SEC_COMAMND_GENERATE_RSA_KEYPAIR,(uint8_t* )&rsaKeypairParams,sizeof(RSA_keypair_params));

            ret = receiveMsg(ifarea, SEC_COMAMND_GENERATE_RSA_KEYPAIR,payload, &payloadSz);

            setArmStatus(ifarea, SEC_ARM_FINISHED);
            if(ret==0){

                DEBUG_MSG("keygen return value=%d (",keypair->header.errorcode);
                if(keypair->header.errorcode!=OK) return keypair->header.errorcode;
                //Dont overflow rx buffer
                if(keypair->header.dataSz > *keypairDerSz){
                    ERROR_MSG("ERROR: runRsaKeypairAlgo too small RX buffer %d <-> %d",keypair->header.dataSz, *keypairDerSz);
                    ret = outOfMem;
                }else{
                    DEBUG_MSG("runRsaKeypairAlgo keypairDer data size=%d",keypair->header.dataSz );
                    memcpy(keypairDer,keypair->data, keypair->header.dataSz );
                    *keypairDerSz = keypair->header.dataSz;
                }

            }

        } else DEBUG_MSG("DSP busy");
    }else ERROR_MSG("ERROR runRsaKeypairAlgo out of memory %d",payloadSz);

    free(payload);
    return ret;
}
/*****************************************************************************/
bool runKeystoreHashAlgo(sec_if_area * const ifarea, const uint8_t* datain, uint32_t const length,uint8_t * const hash)
{
    int32_t ret=-1;
    bool returnvalue = false;

    keystoreHashParams params;
    keystoreHashResult result;

    keystoreHashParams* pIf = (keystoreHashParams*) ifarea->inData;

    uint16_t payloadSz = sizeof(keystoreHashResult);

    if(dspStatusIdle(ifarea) && armStatusIdle(ifarea)) {
        setArmStatus(ifarea, SEC_ARM_PENDING_RESP);

        params.keystoreSz = length;
        params.datain = datain[0] | (datain[1]<<8) | (datain[2]<<16) | (datain[3] << 24);
        
        memcpy(&(pIf->datain),datain,length);


        sendMsgWait(ifarea, SEC_COMMAND_KEYSTORE_HASH,(uint8_t* )&params,sizeof(keystoreHashParams));
        ret = receiveMsg(ifarea, SEC_COMMAND_KEYSTORE_HASH,(uint8_t*)&result, &payloadSz);

        if(ret==0){
            if(result.errorcode==OK ){
                memcpy(hash,result.hash,sizeof(keystoreHashResult::hash));
                returnvalue = true;
            }
        }
        setArmStatus(ifarea, SEC_ARM_FINISHED);
    }
    return returnvalue;
}

static inline bool dsphashFinal(sec_if_area * const ifarea, char * const hashedData) {
    DEBUG_MSG("DSPHmacSha1::hashFinal()");
    setArmStatus(ifarea, SEC_ARM_FINISHED);
    return true;
}

/*****************************************************************************
 * Parse ASN.1 sequence.
 * Go through TAG length pairs, and store pointers and data lengths to resultsTable.
 * This function is recursive, each TAG is processes in it's own call to parseASN_internal.
 * params:
 * char *p: pointer to TAG
 * const uint32_t len: length of the TAG
 * const uint32_t maxlen: The remaining length of the whole ASN.1 sequence
 * uint32_t* iteration: Running number, starting from zero, is incremented by one each time this function is called.
 * RESULT* resultsTable: parsed data / length pairs are stored in resultsTable, param iteration is used as offset.
 */
int parseASN_internal( uint8_t * p, uint32_t const len, uint32_t const maxlen,uint32_t * const p_iteration,RSA_KEY_ELEMS* const elems )
{
    uint8_t *curpos=p;
    uint8_t c;
    uint32_t typeLen;
    uint32_t TAGlength;
    ASN_TYPE TAG;

    int i;
    uint32_t remainingLen;
    signed int iteration = *p_iteration;
    int ret;
    (void)len;

    TAG=(ASN_TYPE)curpos++[0];

    if(TAG==SEQUENCE || TAG==INTEGER || TAG==OBJ_IDENT || TAG==BITSTRING) {
        c=curpos++[0];
        if(c <= (char)127) {
            TAGlength=c&0x000000ff;
            typeLen=1;
        }
        else if( c >(char) 127 ) {
            typeLen=c-0x80;
            TAGlength=0;
            for(i=typeLen;i>0;i--) {
                TAGlength+=(((uint8_t)curpos++[0])<<((i-1)*8));
            }
            typeLen++;
        }

        if(TAGlength>maxlen) {
            return -1;
            }
        elems[iteration].ptr=curpos;
        elems[iteration].length=TAGlength;
        if(TAG==SEQUENCE ) {
            remainingLen= maxlen-(typeLen+1);//processed len + size of tag.
            }
        if(TAG==OBJ_IDENT) {
            iteration++;
            curpos+=TAGlength;
            remainingLen = maxlen-(TAGlength+typeLen+1);
        }
        if(TAG==INTEGER) {
            iteration++;
            curpos+=TAGlength;
            remainingLen = maxlen-(TAGlength+typeLen+1);//processed data len, size of data size and size of tag
        }
        if(TAG==BITSTRING) {
            remainingLen = maxlen-(typeLen+2);

        }
        if(remainingLen) {
            *p_iteration = iteration;
            ret = parseASN_internal(curpos, TAGlength, remainingLen,p_iteration,elems);
            if(ret!=0)return ret;
        }
    }
    else if(TAG==ASN_NULL) {
            remainingLen = maxlen-2;
            *p_iteration = iteration;
            curpos+=1;
            ret = parseASN_internal(curpos, 0, remainingLen,p_iteration,elems);
        }
    else {
        return -1;
    }
    return 0;
}
/*****************************************************************************
 * Wrapper function for parseASN_internal.
 * Sets rest of the parameters to default values.
 * char *p: pointer to buffer containing the ASN.1 sequence
 * const uint32_t len: Length of the ASN.1 buffer.
 * RESULT* resultsTable: pointer to struct where the parsed data/length pairs are stored.
 * return value: Numer of TAGs found, or zero if some errors was encountered.
 */
uint32_t parseASN( uint8_t* p, uint32_t const len,RSA_KEY_ELEMS * const elems)
{
    uint32_t iteration=0;
    int ret;
    ret = parseASN_internal(p, len, len,&iteration,elems );
    return ret ? 0: iteration+1;
}

/*****************************************************************************/
int32_t keygen(sec_if_area * const ifarea, uint32_t const keySize, uint8_t * const publicExponent, uint8_t * const modulus, uint16_t * const ModulusSz, uint8_t * const privkey, uint16_t * const privKeySz)
{
    int32_t ret = -1;
    uint8_t* keypairDer=NULL;
    uint16_t keypairDerSz = keypairSize(keySize);
    RSA_KEY_ELEMS elems[5];
    DEBUG_MSG("calculated key der sz=%d",keypairDerSz);
    keypairDer = (uint8_t*)malloc(keypairDerSz);
    if(keypairDer){

        ret = runRsaKeypairAlgo(ifarea, keySize, keypairDer, &keypairDerSz );
        if(ret){
            ERROR_MSG("keygen failed");
        }else {
            DEBUG_MSG("keygen keypairDerSz=%d ",keypairDerSz);
            ret = parseASN(keypairDer, keypairDerSz,elems);

            if(ret==3){
                if(elems[0].length != 3){
                    ERROR_MSG("wrong size public exponent 3 <-> %d",elems[0].length);
                    ret = -1;
                    return ret;
                }
                DEBUG_MSG("cpy publicExponent %d bytes",elems[0].length);
                memcpy(publicExponent,elems[0].ptr,elems[0].length);

                DEBUG_MSG("cpy modulus %d bytes",elems[1].length);
                memcpy(modulus,elems[1].ptr,elems[1].length);
                *ModulusSz = elems[1].length;

                DEBUG_MSG("cpy privkey %d bytes",elems[2].length);
                memcpy(privkey,elems[2].ptr,elems[2].length);
                DEBUG_MSG("cpy done");
                *privKeySz = elems[2].length;
                ret = 0;
                DEBUG_MSG("keygen success");
            }else{
                ERROR_MSG("parse ASN failed %d",ret);
                ret = -1;
            }
        }
    }
    DEBUG_MSG("free(keypairDer)");
    free(keypairDer);
    DEBUG_MSG("keygen return %d",ret);
    return ret;
}

/*****************************************************************************/
int32_t runRsaKeyImport(sec_if_area * const ifarea,
                        uint8_t * const wrapped,
                        uint16_t* wrappedSz,
                        uint8_t* pubexp,
                        uint16_t pubexpSz,
                        uint8_t* modulus,
                        uint16_t modulusSz,
                        uint8_t* privexp,
                        uint16_t privexpSz,
                        uint8_t* prime1,
                        uint16_t prime1Sz,
                        uint8_t* prime2,
                        uint16_t prime2Sz,
                        uint8_t* exp1,
                        uint16_t exp1Sz,
                        uint8_t* exp2,
                        uint16_t exp2Sz,
                        uint8_t* coef,
                        uint16_t coefSz)
{
    int32_t ret = -1;

    RSA_importkey_params* rsaKeyImportParams;
    uint8_t* payloadIn;
    uint16_t payloadInSz;

    uint8_t* payloadOut;
    uint16_t payloadOutSz;
    RSA_KEY_ELEMS keymaterial[5];


    RSA_importkey_result* importResult;
    payloadInSz =sizeof(RSA_importkey_params);
    payloadOutSz = sizeof(RSA_importkey_result_header) + keypairSize(modulusSz);
    DEBUG_MSG("payloadInSz=%d payloadOutSz=%d\n",payloadInSz,payloadOutSz);
    payloadIn = (uint8_t*)malloc(payloadInSz);
    payloadOut = (uint8_t*)malloc(payloadOutSz);

    if(payloadIn && payloadOut){
        DEBUG_MSG("reading from dsp, magic value: 0x%X", ifarea->header.magic);

        if(dspStatusIdle(ifarea) && armStatusIdle(ifarea)) {
            setArmStatus(ifarea,SEC_ARM_PENDING_RESP);

            importResult = (RSA_importkey_result*)payloadOut;
            rsaKeyImportParams = (RSA_importkey_params*)payloadIn;


            rsaKeyImportParams->pubexpSz = pubexpSz;
            rsaKeyImportParams->modulusSz = modulusSz;
            rsaKeyImportParams->privexpSz = privexpSz;
            rsaKeyImportParams->prime1Sz = prime1Sz;
            rsaKeyImportParams->prime2Sz = prime2Sz;
            rsaKeyImportParams->exp1Sz = exp1Sz;
            rsaKeyImportParams->exp2Sz = exp2Sz;
            rsaKeyImportParams->coefSz = coefSz;
            rsaKeyImportParams->maxWrappedSz=*wrappedSz;

            memcpy(rsaKeyImportParams->pubexp,pubexp,pubexpSz);
            memcpy(rsaKeyImportParams->modulus,modulus,modulusSz);
            memcpy(rsaKeyImportParams->privexp,privexp,privexpSz);
            memcpy(rsaKeyImportParams->prime1,prime1,prime1Sz);
            memcpy(rsaKeyImportParams->prime2,prime2,prime2Sz);
            memcpy(rsaKeyImportParams->exp1,exp1,exp1Sz);
            memcpy(rsaKeyImportParams->exp2,exp2,exp2Sz);
            memcpy(rsaKeyImportParams->coef,coef,coefSz);

            DEBUG_MSG("Start RSA key import");

            sendMsgWait(ifarea,SEC_COMMAND_WRAP_RSA_KEY,(uint8_t* )rsaKeyImportParams,sizeof(RSA_importkey_params));

            ret = receiveMsg(ifarea,SEC_COMMAND_WRAP_RSA_KEY,payloadOut, &payloadOutSz);

            setArmStatus(ifarea,SEC_ARM_FINISHED);
            if(ret==0){

                DEBUG_MSG("key import return value=%d",importResult->header.errorcode);
                if(importResult->header.errorcode!=OK) return importResult->header.errorcode;
                //Dont overflow rx buffer
                if(importResult->header.dataSz > *wrappedSz){
                    ERROR_MSG("ERROR: key import too small RX buffer %d <-> %d",importResult->header.dataSz, *wrappedSz);
                    ret = outOfMem;
                }else{
                    DEBUG_MSG("key import wrapped data size=%d",importResult->header.dataSz );
                    ret = parseASN(importResult->data, importResult->header.dataSz,keymaterial);
                    if(ret==3){
                        //we are not interrested of the public key material, only save the private key
                        DEBUG_MSG("cpy privkey %d bytes",keymaterial[2].length);
                        memcpy(wrapped,keymaterial[2].ptr,keymaterial[2].length);
                        DEBUG_MSG("cpy done");
                        *wrappedSz = keymaterial[2].length;
                        ret = 0;
                    }else{
                        ERROR_MSG("key import failed\n");
                        ret = -1;
                    }
                }
            }

        } else ERROR_MSG("DSP busy");
    }else ERROR_MSG("ERROR key import algo out of memory %d",payloadInSz);

    free(payloadIn);
    free(payloadOut);
    return ret;
}
/*****************************************************************************/
int decryptInit(sec_if_area * const ifarea, uint8_t* wrapped,uint16_t wrappedSz)
{
    int ret;
    DEBUG_MSG("decryptInit");

    ret = runPrivkeyAlgo(ifarea, RSA_METHOD_DECRYPT_INIT, false, wrapped, wrappedSz,NULL,NULL);
    if(ret){
        ERROR_MSG("decryptInit failed");
    }else {
        DEBUG_MSG("decryptInit success");
    }
    return ret;
}
/*****************************************************************************/
int decryptUpdate(sec_if_area * const ifarea, uint8_t const * dataIn, uint16_t dataInSz,uint8_t* dataOut, uint16_t* dataOutSz)
{
    int ret;
    DEBUG_MSG("decryptUpdate");
    ret = runPrivkeyAlgo(ifarea, RSA_METHOD_DECRYPT_UPDATE,true, dataIn, dataInSz,dataOut, dataOutSz);
    if(ret){
        ERROR_MSG("decryptUpdate failed");
    }else {
        DEBUG_MSG("decryptUpdate success");
    }
    return ret;
}
/*****************************************************************************/
int decryptFinal(sec_if_area * const ifarea)
{
    //int ret;
    DEBUG_MSG("decryptFinal");
    setArmStatus(ifarea, SEC_ARM_FINISHED);
    unInstallKey(ifarea);
    return 0;
}
/*****************************************************************************/
int signInit(sec_if_area * const ifarea, uint8_t* wrapped,uint16_t wrappedSz)
{
    int ret;
    DEBUG_MSG("signInit, key ptr=%p, wrappedKeySz=%d", wrapped, wrappedSz);

    ret = runPrivkeyAlgo(ifarea, RSA_METHOD_SIGN_INIT, false, wrapped, wrappedSz,NULL,NULL);
    if(ret){
        ERROR_MSG("signInit failed, status %d",ret);
    }else {
        DEBUG_MSG("signInit success");
    }
    return ret;
}
/*****************************************************************************/
int signUpdate(sec_if_area * const ifarea, uint8_t const * dataIn, uint16_t dataInSz,uint8_t* dataOut, uint16_t* dataOutSz)
{
    int ret;
    DEBUG_MSG("signUpdate, dataptr=%p, dataInSz=%d", dataIn, dataInSz);

    ret = runPrivkeyAlgo(ifarea, RSA_METHOD_SIGN_UPDATE,true, dataIn, dataInSz,dataOut, dataOutSz);
    if(ret){
        ERROR_MSG("signUpdate failed, status %d",ret);
    }else {
        DEBUG_MSG("signUpdate success, dataOutSz=%d", *dataOutSz);
    }
    return ret;

}
/*****************************************************************************/
int signFinal(sec_if_area * const ifarea)
{
    DEBUG_MSG("signFinal");
    setArmStatus(ifarea, SEC_ARM_FINISHED);
    unInstallKey(ifarea);
    return 1;
}
/*****************************************************************************/
static inline bool keystoreHashUpdate(sec_if_area * const ifarea, const uint8_t* datain,const uint32_t length,uint8_t* hash)
{
    bool ret;

    ret = runKeystoreHashAlgo(ifarea, datain,length,hash);
    if(ret==false){
        ERROR_MSG("Keystore hash update failed");
    }else {
        DEBUG_MSG("Keystore hash update success");
    }
    return ret;
}
/*****************************************************************************/
SecureDSP::SecureDSP() {
#ifdef TARGETBUILD
    memBase = (void*)Configuration::i()->getInt("secstore.sharedmem.addr");
    m_initdone      = false;
    m_interfaceArea = NULL;
    m_dspmap        = NULL;
    if (memBase != NULL) {
        DEBUG_MSG("SecureDSP:: constructed, mapping address %p", memBase);
        m_dspmap = new MapRegion((void *) memBase, 0x10000, readWriteMode);
        if (m_dspmap) {
            m_interfaceArea = (sec_if_area*)m_dspmap->getPtr();
            if (initShm(m_interfaceArea, false) < 0) {
                ERROR_MSG("DSP init failed!");
                delete m_dspmap;
                m_dspmap = NULL;
                m_interfaceArea = NULL;
            } else {
                DEBUG_MSG("DSP init success!");
                m_interfaceArea = (sec_if_area*)m_dspmap->getPtr();
                m_initdone = true;
            }
        }
    } else {
        ERROR_MSG("SecureDSP:: secstore.sharedmem.addr is not set!");
    }
#else
    m_interfaceArea = (sec_if_area*)malloc(sizeof(sec_if_area));
    m_initdone = true;
#endif
}
/*****************************************************************************/
SecureDSP::~SecureDSP() {
#ifdef TARGETBUILD
    delete m_dspmap;
#else
    free(m_interfaceArea);
#endif
}
/*****************************************************************************/
bool SecureDSP::generateKey(ByteString& privateKey, ByteString& publicKey, ByteString& publicExp, uint32_t keylen) {
    if (this->m_initdone) {
#ifdef TARGETBUILD
        uint32_t i;
        int32_t err;
        bool ret=true;
        uint8_t* privkey=NULL;
        uint8_t* modulus=NULL;
        uint8_t publicExponent[4];

        uint16_t privKeySz=1216;//Wrapped 2048 bit private key
        uint16_t modulusSz = 300;
        uint8_t* pubkey=NULL;
        uint16_t pubKeySz =300; //2048 bit public key in DER format.
        DEBUG_MSG("SecureDSP::generateKey");


        privkey = (uint8_t*) malloc(privKeySz);
        modulus = (uint8_t*) malloc(modulusSz);
        pubkey =  (uint8_t*) malloc(pubKeySz);

        if(privkey && modulus){

            err = keygen(m_interfaceArea, keylen/8, publicExponent, modulus, &modulusSz, privkey, &privKeySz);

            if(err==0){

                for(i=0; i<privKeySz;i++) {
                    privateKey += privkey[i];
                }
                for(i=0; i<modulusSz;i++) {
                    publicKey += modulus[i];
                }
                for(i=0; i<3;i++) { /*Fixed size & value: size=3 bytes, value=65537*/
                    publicExp += publicExponent[i];
                }
            } else {
                ERROR_MSG("keygen failed");
                ret = false;
            }
        } else {
            ERROR_MSG("malloc failure");
            ret = false;
        }

        if(privkey) {
            free(privkey);
        }
        if(modulus) {
            free(modulus);
        }
        if(pubkey) {
            free(pubkey);
        }
        return ret;
#else
        uint32_t i;
        for(i=0; i<800;i++) {
            privateKey += 'a';
        }
        for(i=0; i<keylen/8;i++) {
            publicKey += 'p';
        }
        return true;
#endif
    } else {
        ERROR_MSG("DSP not initialized, can't continue!");
        return false;
    }
}

/*****************************************************************************/
bool  SecureDSP::wrapKey(   ByteString& privateKey,
                            ByteString& pubexp,
                            ByteString& modulus,
                            ByteString& privexp,
                            ByteString& prime1,
                            ByteString& prime2,
                            ByteString& exp1,
                            ByteString& exp2,
                            ByteString& coef)  {

    if (this->m_initdone) {
#ifdef TARGETBUILD
    bool ret=true;
    uint32_t i;
    int32_t err;
    uint8_t* privkey=NULL;
    uint16_t privKeySz=1716;//Wrapped 2048 bit private key
    DEBUG_MSG("SecureDSP::wrapKey");

    privkey = (uint8_t*) malloc(privKeySz);

        if(privkey){

            err = runRsaKeyImport(m_interfaceArea,
                                  privkey,
                                  &privKeySz,
                                  pubexp.byte_str(),
                                  pubexp.size(),
                                  modulus.byte_str(),
                                  modulus.size(),
                                  privexp.byte_str(),
                                  privexp.size(),
                                  prime1.byte_str(),
                                  prime1.size(),
                                  prime2.byte_str(),
                                  prime2.size(),
                                  exp1.byte_str(),
                                  exp1.size(),
                                  exp2.byte_str(),
                                  exp2.size(),
                                  coef.byte_str(),
                                  coef.size() );

            if(err==0){

                for(i=0; i<privKeySz;i++) {
                    privateKey += privkey[i];
                }
            } else {
                ERROR_MSG("key wrap failed %d",err);
                ret = false;
            }
        } else {
            ERROR_MSG("malloc failure");
            ret = false;
        }

        if(privkey) {
            free(privkey);
        }
        return ret;
#else
        ERROR_MSG("wrapkey not implemented yet");
#endif
    } else {
        ERROR_MSG("DSP not initialized, can't continue!");
        return false;
    }
    return false;
}
/*****************************************************************************/
int32_t SecureDSP::sign(ByteString& privateKey, const ByteString& dataToSign, ByteString& signature, uint32_t const keysize, uint32_t padding){
    int32_t signlen = keysize;
#ifdef TARGETBUILD
    (void)padding;
    DEBUG_MSG("SecureDSP::sign (ARM) started");

    int32_t status = signInit(m_interfaceArea, privateKey.byte_str(),privateKey.size());
    if (status==0) {
        DEBUG_MSG(" SecureDSP::sign init success, continue to signUpdate");
        uint16_t dataOutSz = 1024; //Where to get?

        uint8_t * dataOut = (uint8_t*) malloc(dataOutSz);
        uint8_t const * data = dataToSign.const_byte_str();

        status = signUpdate(m_interfaceArea, data, dataToSign.size(), dataOut, &dataOutSz);
        DEBUG_MSG("Sign finished, status %d, len %d",status, dataOutSz);

        for(uint32_t i=0; i<dataOutSz; i++) {
            signature += dataOut[i];
        }
        signlen = dataOutSz;
        free(dataOut);
    } else {
        ERROR_MSG(" SecureDSP::sign init failed");
        return -1;
    }
    signFinal(m_interfaceArea);
    DEBUG_MSG(" SecureDSP::sign (ARM) finished, status %d, signature len %d",status, signlen);
#else
    for(uint32_t i=0; i<signlen; i++) {
        signature += 's';
    }
#endif
    return signlen;
}
/*****************************************************************************/
int32_t SecureDSP::decrypt(ByteString& privateKey, const ByteString& encryptedData, ByteString& decryptedData, uint32_t const keysize) {
    int32_t status = 0;
    DEBUG_MSG("DSP decrypt");
    if (this->m_initdone) {
#ifdef TARGETBUILD
        (void)keysize;
        status = decryptInit(m_interfaceArea, privateKey.byte_str(),privateKey.size());
        if (status==0) {
            uint16_t dataOutSz = 1024; //Where to get?

            uint8_t * dataOut = (uint8_t*) malloc(dataOutSz);
            uint8_t const * data = encryptedData.const_byte_str();
            if(dataOut) {
                status = decryptUpdate(m_interfaceArea, data, encryptedData.size(), dataOut, &dataOutSz);

                decryptFinal(m_interfaceArea);
                decryptedData.resize(0);
                for(uint32_t i=0; i<dataOutSz; i++) {
                    decryptedData += dataOut[i];
                }
                free(dataOut);
            }
        } else {
            ERROR_MSG(" SecureDSP::decrypt init failed");
            return -1;
        }
#else
        for (uint32_t i=0; i<encryptedData.size(); i++) {
            decryptedData += 's';
        }
#endif
    } else {
        ERROR_MSG("DSP not initialized!");
    }
    return status;
}

/*****************************************************************************/
bool SecureDSP::hashInit() {
    bool ret = false;
    DEBUG_MSG("DSP hashInit");
    if (this->m_initdone) {
        ret = true;
    } else {
        ERROR_MSG("DSP not initialized!");
    }
    return ret;
}

uint8_t hash[HMACSHA1_LEN];
/*****************************************************************************/
bool SecureDSP::hashUpdate(const ByteString& data) {
    bool ret = false;
    DEBUG_MSG("DSP hashUpdate");
    if (this->m_initdone) {
        const uint8_t* address = (uint8_t*)data.const_byte_str();
        ret = keystoreHashUpdate(m_interfaceArea, address, data.size(), hash);
    } else {
        ERROR_MSG("DSP not initialized!");
    }
    return ret;
}
/*****************************************************************************/
bool SecureDSP::hashFinal(ByteString& hashedData) {
    DEBUG_MSG("DSP hashFinal");

    for(uint32_t i=0; i<sizeof(keystoreHashResult::hash); i++) {
        hashedData += hash[i];
    }


    return true;
}

