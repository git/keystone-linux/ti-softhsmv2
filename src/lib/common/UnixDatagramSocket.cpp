/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <iostream>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include "UnixDatagramSocket.h"

UnixDatagramSocket::UnixDatagramSocket(const std::string socketName)
{
	bind = true;
	fd = -1;

	memset ((void *) &localAddr, 0, sizeof(struct sockaddr_un));
	memset(&readfds, 0, sizeof(fd_set));
	localAddr.sun_family = AF_UNIX;

	if (checkAndCreatePath(socketName) < 0) {
		return;
	}
	localAddr.sun_path[UNIX_PATH_MAX-1] = '\0'; /*Make sure that string is terminated*/
	strncpy (localAddr.sun_path, socketName.c_str(), UNIX_PATH_MAX-1);

	socketInit();
}

UnixDatagramSocket::UnixDatagramSocket(const struct sockaddr_un *addr)
{
	bind = true;
	memcpy ((void *) &localAddr, (void *)addr, sizeof(struct sockaddr_un));
	socketInit();
}

UnixDatagramSocket::UnixDatagramSocket()
{
	bind = false;
	memset ((void *) &localAddr, 0, sizeof(struct sockaddr_un));
	socketInit();
}

UnixDatagramSocket::~UnixDatagramSocket()
{
	if (fd > 0) {
		close(fd);
	}
	if (localAddr.sun_family == AF_UNIX) {
		unlink(localAddr.sun_path);
	}
}

void UnixDatagramSocket::socketInit()
{
	errnoVal = 0;

	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0) {
		errnoVal = errno;
		return;
	}

	if (bind) {
		unlink(localAddr.sun_path);

		if (::bind(fd, (struct sockaddr *) &localAddr, sizeof(struct sockaddr_un)) < 0) {
			errnoVal = errno;
			return;
		}

		FD_ZERO(&readfds);
		FD_SET(fd, &readfds);
	}
}

int UnixDatagramSocket::sendData (const char *data, int dataLen, const std::string toSocketName)
{
	struct sockaddr_un addr;

	addr.sun_family = AF_UNIX;
	addr.sun_path[UNIX_PATH_MAX-1] = '\0'; /*Make sure that string is terminated*/
	strncpy (addr.sun_path, toSocketName.c_str(), UNIX_PATH_MAX-1);

	return sendData(data, dataLen, &addr);

}

int UnixDatagramSocket::waitData (int &datagramSize, struct timeval *timeOut /*= NULL*/,
					int externalFD /*= -1*/)
{
	int retval, size;
	fd_set fds = readfds;

	errnoVal = 0;
	datagramSize = -1;

	if (!bind) return -1;

	if (externalFD != -1) {
		FD_SET(externalFD, &fds);
	}

	retval = select(FD_SETSIZE, &fds, NULL, NULL, timeOut);
	if (retval == -1) {
		errnoVal = errno;
		if (errno != EINTR) return -1;
	}

	if ((externalFD != -1) && (FD_ISSET(externalFD, &fds))) {
		return 1;
	}

	if (!FD_ISSET(fd, &fds)) {
		/* Wait timedout */
		return -2;
	}

	if (!retval) {
		return 0;
	}

	retval = ioctl(fd, FIONREAD, &datagramSize);
	if (retval == -1) {
		errnoVal = errno;
		return -1;
	}

	return retval;
}

int UnixDatagramSocket::sendData (const char *data, int dataLen, const struct sockaddr_un *to)
{
	errnoVal = 0;

	int retval = sendto (fd, data, dataLen, 0, (struct sockaddr *) to, sizeof(struct sockaddr_un));

	if (retval == -1) {
		errnoVal = errno;
		return -1;
	}

	return retval;
}

int UnixDatagramSocket::recvData (char *data, int dataLen, struct sockaddr_un *from /*= 0*/, bool peek /*= false*/)
{
	errnoVal = 0;

	if (!bind) return -1;

	int size;
	int flags = (peek) ? MSG_PEEK : 0;
	socklen_t fromLen = (from) ? sizeof(struct sockaddr_un) : 0;

	size = recvfrom(fd, data, dataLen, flags, (struct sockaddr *) from, &fromLen);
	if (size == -1) {
		errnoVal = errno;
		return -1;
	}

	return size;
}

int UnixDatagramSocket::checkAndCreatePath(const std::string path)
{
	int status;
	size_t pos = 0;
	std::string dir;

	while ((pos = path.find_first_of('/', pos)) != std::string::npos) {

		dir = path.substr(0, pos++);

		if (!dir.size()) continue;

		status = mkdir(dir.c_str(),
			       S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

		if (status && (errno != EEXIST)) {
			errnoVal = errno;
			return -1;
		}
	}
	return 0;
}
