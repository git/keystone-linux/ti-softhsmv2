//----------------------------------------------------------------
// Statically-allocated memory manager
//
// by Eli Bendersky (eliben@gmail.com)
//  
// This code is in the public domain.
//----------------------------------------------------------------
// NOTE: Modified by TI to wrap into C++ class


#include <stdio.h>
#include <stdlib.h>
#include "log.h"
#include "Configuration.h"
#include "MemMgr.h"

/* 
* uncomment the below define to map a file(for DEBUG) instead of /dev/mem
*/
//#define MEMMGR_FILE_MAP
#define MEMMGR_FILE_NAME "./memmgr.bin"

// Initialize the one-and-only instance
std::auto_ptr<MemMgr> MemMgr::instance(NULL);

// Return the one-and-only instance
MemMgr* MemMgr::i()
{
	if (!instance.get())
	{
		instance = std::auto_ptr<MemMgr>(new MemMgr());
	}

	return instance.get();
}

// Constructor
MemMgr::MemMgr()
{
    unsigned long secStoreBase, memBase;
    unsigned long maxSize = Configuration::i()->getInt("secstore.mem.size");
    secStoreBase = Configuration::i()->getInt("secstore.mem.addr");

    valid = false;
    base.s.next = 0;
    base.s.size = 0;
    freep = 0;
    pool_free_pos = 0;
    pool  = NULL;
    map   = NULL;
    mutex = NULL;

    if (maxSize <= MEMMGR_POOL_SIZE) {
        ERROR_MSG("Not enough memory for MemMgr memSize(0x%x)", maxSize);
        return; 
    }

    memBase = secStoreBase + maxSize - MEMMGR_POOL_SIZE; 

#ifndef MEMMGR_FILE_MAP
    INFO_MSG("Creating mapping at 0x%x length 0x%x", memBase, MEMMGR_POOL_SIZE);
    map = new MapRegion ((void *) memBase, MEMMGR_POOL_SIZE, readWriteMode);
#else
    INFO_MSG("Creating mapping from %s", MEMMGR_FILE_NAME);
    map = new MapRegion ((std::string) MEMMGR_FILE_NAME, readWriteMode);
#endif

    if (!map->isValid()) {
        ERROR_MSG("Can't map 0x%x length 0x%x for MemMgr", memBase, MEMMGR_POOL_SIZE);
        return;
    }

    pool = (byte*)map->getPtr();

    //pool = (byte *)malloc(MEMMGR_POOL_SIZE);
    if (pool == NULL) {
        delete map;
        ERROR_MSG("Couldn't allocate memory for MemMgr");
        return; 
    }

    // Get a mutex
    mutex = MutexFactory::i()->getMutex();

    if (mutex == NULL)
    {
        delete map;
        ERROR_MSG("Could not create Mutex");
        return;
    }

    valid = true;
    return;
}

// Destructor
MemMgr::~MemMgr()
{
    if (map) {
        delete map;
    }
    MutexFactory::i()->recycleMutex(mutex);
}

void MemMgr::printStats()
{
    mem_header_t* p;

    if (!valid) {
        ERROR_MSG("MemMgr invalid");
        return;
    }

    INFO_MSG("------ Memory manager stats ------\n\n");
    INFO_MSG(    "Pool: free_pos = %lu (%lu bytes left)\n\n", 
            pool_free_pos, MEMMGR_POOL_SIZE - pool_free_pos);

    p = (mem_header_t*) pool;

    while (p < (mem_header_t*) (pool + pool_free_pos))
    {
        INFO_MSG(    "  * Addr: 0x%8lu; Size: %8lu\n",
                p, p->s.size);

        p += p->s.size;
    }

    INFO_MSG("\nFree list:\n\n");

    if (freep)
    {
        p = freep;

        while (1)
        {
            INFO_MSG(    "  * Addr: 0x%8lu; Size: %8lu; Next: 0x%8lu\n", 
                    p, p->s.size, p->s.next);

            p = p->s.next;

            if (p == freep)
                break;
        }
    }
    else
    {
        INFO_MSG("Empty\n");
    }
    
    INFO_MSG("\n");
}


mem_header_t* MemMgr::getMemFromPool(ulong nquantas)
{
    ulong total_req_size;

    mem_header_t* h;

    if (nquantas < MIN_POOL_ALLOC_QUANTAS)
        nquantas = MIN_POOL_ALLOC_QUANTAS;

    total_req_size = nquantas * sizeof(mem_header_t);

    if (pool_free_pos + total_req_size <= MEMMGR_POOL_SIZE)
    {
        h = (mem_header_t*) (pool + pool_free_pos);
        h->s.size = nquantas;
        free((void*) (h + 1));
        pool_free_pos += total_req_size;
    }
    else
    {
        return 0;
    }

    return freep;
}


// Allocations are done in 'quantas' of header size.
// The search for a free block of adequate size begins at the point 'freep' 
// where the last block was found.
// If a too-big block is found, it is split and the tail is returned (this 
// way the header of the original needs only to have its size adjusted).
// The pointer returned to the user points to the free space within the block,
// which begins one quanta after the header.
//
void* MemMgr::alloc(ulong nbytes)
{
    mem_header_t* p;
    mem_header_t* prevp;

    if (!valid) {
        ERROR_MSG("MemMgr invalid");
        return NULL;
    }

    MutexLocker lock(mutex);

    // Calculate how many quantas are required: we need enough to house all
    // the requested bytes, plus the header. The -1 and +1 are there to make sure
    // that if nbytes is a multiple of nquantas, we don't allocate too much
    //
    ulong nquantas = (nbytes + sizeof(mem_header_t) - 1) / sizeof(mem_header_t) + 1;

    // First alloc call, and no free list yet ? Use 'base' for an initial
    // denegerate block of size 0, which points to itself
    // 
    if ((prevp = freep) == 0)
    {
        base.s.next = freep = prevp = &base;
        base.s.size = 0;
    }

    for (p = prevp->s.next; ; prevp = p, p = p->s.next)
    {
        // big enough ?
        if (p->s.size >= nquantas) 
        {
            // exactly ?
            if (p->s.size == nquantas)
            {
                // just eliminate this block from the free list by pointing
                // its prev's next to its next
                //
                prevp->s.next = p->s.next;
            }
            else // too big
            {
                p->s.size -= nquantas;
                p += p->s.size;
                p->s.size = nquantas;
            }

            freep = prevp;
            return (void*) (p + 1);
        }
        // Reached end of free list ?
        // Try to allocate the block from the pool. If that succeeds,
        // getMemFromPool adds the new block to the free list and
        // it will be found in the following iterations. If the call
        // to getMemFromPool doesn't succeed, we've run out of
        // memory
        //
        else if (p == freep)
        {
            if ((p = getMemFromPool(nquantas)) == 0)
            {
                DEBUG_MSG("!! Memory allocation failed !!\n");
                return 0;
            }
        }
    }
}


// Scans the free list, starting at freep, looking the the place to insert the 
// free block. This is either between two existing blocks or at the end of the
// list. In any case, if the block being freed is adjacent to either neighbor,
// the adjacent blocks are combined.
//
void MemMgr::free(void* ap)
{
    mem_header_t* block;
    mem_header_t* p;

    if (!valid) {
        ERROR_MSG("MemMgr invalid");
        return;
    }

    MutexLocker lock(mutex);

    // acquire pointer to block header
    block = ((mem_header_t*) ap) - 1;

    // Find the correct place to place the block in (the free list is sorted by
    // address, increasing order)
    //
    for (p = freep; !(block > p && block < p->s.next); p = p->s.next)
    {
        // Since the free list is circular, there is one link where a 
        // higher-addressed block points to a lower-addressed block. 
        // This condition checks if the block should be actually 
        // inserted between them
        //
        if (p >= p->s.next && (block > p || block < p->s.next))
            break;
    }

    // Try to combine with the higher neighbor
    //
    if (block + block->s.size == p->s.next)
    {
        block->s.size += p->s.next->s.size;
        block->s.next = p->s.next->s.next;
    }
    else
    {
        block->s.next = p->s.next;
    }

    // Try to combine with the lower neighbor
    //
    if (p + p->s.size == block)
    {
        p->s.size += block->s.size;
        p->s.next = block->s.next;
    }
    else
    {
        p->s.next = block;
    }

    freep = p;
}

