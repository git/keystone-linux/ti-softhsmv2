/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*****************************************************************************
  FileSystem.h

  Helper functions to access the filesystem.
 *****************************************************************************/

#ifndef _SOFTHSM_V2_FILESYSTEM_H
#define _SOFTHSM_V2_FILESYSTEM_H

#include "config.h"
#include <stdint.h>
#include <string>
#include <vector>
#include <memory>

//#define FILE_REGION_MAP
#include "CryptoFactory.h"
#include "MapRegion.h"
#include "FileStore.h"

#define READ_UINT32(addr)       (*(volatile uint32_t *)(addr))
#define WRITE_UINT32(addr, val) (*(volatile uint32_t *)(addr)) = (uint32_t)(val)

#define DBSTORE_MAX_NAME_LENGTH 256
//#define DBSTORE_MAX_NAME_LENGTH 64

/*
   FS structure

   HDR {magic, tag, size}
   Node {type, name_len, name, data_len, alloc_len}, [data], [pad] 
   ....
   Node {type, name_len, name, data_len, alloc_len}, [data], [pad]
*/

#define FSNodeTypeOffset	0
#define FSNodeNameLengthOffset	(FSNodeTypeOffset + 4)
#define FSNodeNameOffset	(FSNodeNameLengthOffset + 4)
#define FSNodeDataLengthOffset	(FSNodeNameOffset + DBSTORE_MAX_NAME_LENGTH)
#define FSNodeAllocLengthOffset	(FSNodeDataLengthOffset + 4)
#define FSNodeDataOffset	(FSNodeAllocLengthOffset + 4)

#define fsNodeHdrLength		(FSNodeDataOffset)

#define FSNodeTypeDirectory	1
#define FSNodeTypeFile		2

enum fsReturnCode {
	fsSuccess,
	fsEOF,
	fsNoNode,
	fsInvalid,
	fsErrorTag,
	fsErrorGen
};

enum fsLockType {
	fsLockTypeRead,
	fsLockTypeWrite
};

class FileSystem;

class FSNode
{

	private:

		uint32_t tag;

		char *rootPtr;

		uint32_t getAllocLength() const
		{
			return (rootPtr) ? READ_UINT32(&rootPtr[FSNodeAllocLengthOffset]) : 0;
		}

	public:
		// Constructor
		FSNode() { reset(); }

		// Destructor
		virtual ~FSNode() { }

		void reset()
		{
			tag = 0;
			rootPtr = 0;
		}

		uint32_t getNodeType() const
		{
			return (rootPtr) ? READ_UINT32(&rootPtr[FSNodeTypeOffset]) : 0;
		}

		uint32_t getNodeNameLength() const
		{
			return (rootPtr) ? READ_UINT32(&rootPtr[FSNodeNameLengthOffset]) : 0;
		}

		char* getNodeName() const
		{
			return (rootPtr) ? (rootPtr + FSNodeNameOffset) : 0;
		}

		uint32_t getNodeDataLength() const
		{
			return (rootPtr) ? READ_UINT32(&rootPtr[FSNodeDataLengthOffset]) : 0;
		}

		char *getData() const
		{
			if (rootPtr && getNodeDataLength()) {
				return (rootPtr + FSNodeDataOffset);
			} else {
				return 0;
			}
		}

		friend class FileSystem;
};

class FileSystem
{
	public:
		// Return the one-and-only instance
		static FileSystem* i();

		// This will destroy the one-and-only instance.
		static void reset();

		// Destructor
		virtual ~FileSystem();

		// Check if the FileSystem is valid
		bool isValid() {
			return valid;
		}

		// Return a list of all files in a FileSystem
		char *getRoot() {
			return rootPtr;
		}

		bool isReInitNeeded() {
			return reInitNeeded;
		}

		void clearReInit() {
			reInitNeeded = false;
		}

		// Return a size of all files in a FileSystem
		unsigned int getFSSize();

		// Return maximum allowed size of the FileSystem
		unsigned int getMaxSize()
		{
			return maxSize;
		}

		// Return the FileSystem tag
		uint32_t getFSTag();

		// Returns the next node of the FileSystem given current node
		fsReturnCode getNextNode(FSNode &node);

		// Add a node to filesystem
		fsReturnCode addNode(uint32_t type, std::string name, uint32_t dataLength = 0);

		// Add a node to filesystem
		fsReturnCode deleteNode(std::string name);

		// Find node by name
		fsReturnCode getNodeByName(FSNode &node, std::string name);

		// Increase size of a Node
		fsReturnCode expandNode(FSNode &node, uint32_t length);

		// Lock the filesystem
		bool lockFS(bool block = true, fsLockType type = fsLockTypeRead);

		// Unlock the filesystem
		bool unlockFS();

		// Flush the filesystem to background storage
		bool flush();

		void updateTag();

		// Check if node data is consistent
		bool fsNodeIntegrityCheck(const FSNode &node);

		// Checks filesystem integrity
		bool fsIntegrityCheck(const char *func = "", int line = 0);

	private:

		// Constructor
		FileSystem();
		FileSystem(bool version3);

		// The one-and-only instance
		static std::auto_ptr<FileSystem> instance;

		// The status
		bool valid;

		bool reInitNeeded;

		// All files in the FileSystem
		char *rootPtr;

		// Maximum allowed size of the FileSystem
		unsigned int maxSize;

		FileStore fileStore;

		int version;

		bool locked;

		MapRegion *map;

		char *getFileSystemKey(char *keyPtr, unsigned int length);

		bool decryptFS(ByteString &aesKeyData, ByteString &encryptedFS);

		bool getFSHash(ByteString &hashBuf);

		int getFSHashLength() const;

		bool checkHash();
};

#endif // !_SOFTHSM_V2_FILESYSTEM_H

