/* $Id: File.cpp 6799 2012-10-30 12:53:19Z rb $ */

/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * Copyright (c) 2010 SURFnet bv
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
  File.h

  This class wraps standard C file I/O in a convenient way for the object store
 *****************************************************************************/

#include <string>
#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <fcntl.h>
#include <errno.h>


#include "config.h"
#include "FileSystem.h"
#include "File.h"
#include "log.h"

#define filePtr (node.getData() + rwOffset)

#define CHECK_N_UPDATE_NODE(n, p) \
	do { \
		if (!FileSystem::i()->fsNodeIntegrityCheck(n)) { \
			if (FileSystem::i()->getNodeByName(n, p) != fsSuccess) { \
				ERROR_MSG("Fail to get node for file %s\n", p.c_str()); \
				return false; \
			} \
		} \
	} while(0)

#define FS_CHECK \
	do { \
		if (!FileSystem::i()->fsIntegrityCheck(__func__, __LINE__)) { \
			ERROR_MSG("File system integrity check failed with file %s\n", path.c_str()); \
			valid = false; \
			return; \
		} \
	} while (0)

#define FS_CHECK_BOOL \
	do { \
		if (!FileSystem::i()->fsIntegrityCheck(__func__, __LINE__)) { \
			ERROR_MSG("File system intagrity check failed with file %s\n", path.c_str()); \
			valid = false; \
			return false; \
		} \
	} while (0)

// Constructor
//
// N.B.: the create flag only has a function when a file is opened read/write
File::File(std::string path, bool forRead /* = true */, bool forWrite /* = false */, bool create /* = false */)
{
	fsReturnCode retval;

	node.reset();
	isReadable = forRead;
	isWritable = forWrite;
	valid = false;
	rwOffset = 0;

	FS_CHECK;

	this->path = path;

	if (!forRead && !forWrite)
		return;

	if (create == true) {
		// Try to create the file
		retval = FileSystem::i()->addNode(FSNodeTypeFile, path, 0);

		if (retval != fsSuccess) {
			valid = false;
			return;
		}
	}

	if (FileSystem::i()->getNodeByName(node, path)!= fsSuccess) {
		valid = false;
		return;
	}

	if (node.getNodeType() != FSNodeTypeFile) {
		valid = false;
		return;
	}

	FS_CHECK;

	valid = true;

}

// Destructor
File::~File()
{

}

// Check if the file is valid
bool File::isValid()
{
	return valid;
}

// Check if the file is readable
bool File::isRead()
{
	return isReadable;
}

// Check if the file is writable
bool File::isWrite()
{
	return isWritable;
}

// Check if the end-of-file was reached
bool File::isEOF()
{
	if (!valid)
		return false;

	if (rwOffset >= node.getNodeDataLength())
		return true;

	return false;
}

// Lock the file
bool File::lock(bool block /* = true */)
{
	return FileSystem::i()->lockFS(block, isWritable ? fsLockTypeWrite : fsLockTypeRead);
}

// Unlock the file
bool File::unlock()
{
	return FileSystem::i()->unlockFS();
}

// Read an unsigned long value; warning: not thread safe without locking!
bool File::readULong(unsigned long& value)
{
	FS_CHECK_BOOL;
	if (!valid || !isReadable) return false;

	CHECK_N_UPDATE_NODE(node, path);

	ByteString ulongVal;

	ulongVal.resize(8);

	if ( (rwOffset + 8) > node.getNodeDataLength())
		return false;

	memcpy (&ulongVal[0], filePtr, 8);

	rwOffset += 8;

	value = ulongVal.long_val();

	return true;
}

// Read a ByteString value; warning: not thread safe without locking!
bool File::readByteString(ByteString& value)
{
	if (!valid || !isReadable) return false;
	FS_CHECK_BOOL;
	CHECK_N_UPDATE_NODE(node, path);

	// Retrieve the length to read from the file
	unsigned long len;

	if (!readULong(len))
	{
		return false;
	}

	if ( (rwOffset + len) > node.getNodeDataLength())
		return false;

	// Read the byte string from the file
	value.resize(len);

	memcpy (&value[0], filePtr, len);

	rwOffset += len;
	FS_CHECK_BOOL;
	return true;
}

// Read a boolean value; warning: not thread safe without locking!
bool File::readBool(bool& value)
{
	if (!valid || !isReadable) return false;
	FS_CHECK_BOOL;
	CHECK_N_UPDATE_NODE(node, path);

	// Read the boolean from the file
	unsigned char boolValue;

	if ( (rwOffset + 1) > node.getNodeDataLength())
		return false;

	boolValue = filePtr[0];

	rwOffset += 1;

	value = boolValue ? true : false;
	FS_CHECK_BOOL;
	return true;
}

// Read a string value; warning: not thread safe without locking!
bool File::readString(std::string& value)
{
	if (!valid || !isReadable) return false;
	FS_CHECK_BOOL;
	CHECK_N_UPDATE_NODE(node, path);

	// Retrieve the length to read from the file
	unsigned long len;

	if (!readULong(len))
	{
		return false;
	}

	// Read the string from the file
	value.resize(len);

	if ( (rwOffset + len) > node.getNodeDataLength())
		return false;

	memcpy (&value[0], filePtr, len);

	rwOffset += len;
	FS_CHECK_BOOL;
	return true;
}

bool File::writeMem(ByteString &toWrite)
{
	fsReturnCode retval;
	if (!node.getData()) {
		ERROR_MSG("End of file for %s", path.c_str());
		return false;
	}
	FS_CHECK_BOOL;
	//It is a overwrite
	if (memcmp (toWrite.const_byte_str(), filePtr, toWrite.size())) {
		memcpy (filePtr, toWrite.const_byte_str(), toWrite.size());
		FileSystem::i()->updateTag();
		retval = FileSystem::i()->getNodeByName(node, path);
		if (retval != fsSuccess) {
			valid = false;
			return false;
		}
	}
	rwOffset += toWrite.size();
	FS_CHECK_BOOL;
	return true;
}

// Write an unsigned long value; warning: not thread safe without locking!
bool File::writeULong(const unsigned long value)
{
	fsReturnCode retval;
	FS_CHECK_BOOL;
	if (!valid || !isWritable) return false;

	CHECK_N_UPDATE_NODE(node, path);

	ByteString toWrite(value);

	if ( (rwOffset + toWrite.size()) > node.getNodeDataLength()) {
		// Increase file size
		if (FileSystem::i()->expandNode(node, (rwOffset + toWrite.size()) - node.getNodeDataLength()) != fsSuccess)
			return false;
	}
	FS_CHECK_BOOL;
	return writeMem(toWrite);
}

// Write a ByteString value; warning: not thread safe without locking!
bool File::writeByteString(const ByteString& value)
{
	fsReturnCode retval;
	if (!valid || !isWritable) return false;
	FS_CHECK_BOOL;
	CHECK_N_UPDATE_NODE(node, path);

	ByteString toWrite = value.serialise();

	if ( (rwOffset + toWrite.size()) > node.getNodeDataLength()) {
		// Increase file size
		if (FileSystem::i()->expandNode(node, (rwOffset + toWrite.size()) - node.getNodeDataLength()) != fsSuccess)
			return false;
	}
	FS_CHECK_BOOL;
	return writeMem(toWrite);
}

// Write a string value; warning: not thread safe without locking!
bool File::writeString(const std::string& value)
{
	if (!valid || !isWritable) return false;
	FS_CHECK_BOOL;
	CHECK_N_UPDATE_NODE(node, path);

	ByteString toWriteSize((const unsigned long) value.size());
	ByteString toWriteValue((const unsigned char*) &value[0], value.size());

	if ( !node.getNodeDataLength()
			|| (rwOffset + toWriteSize.size() + toWriteValue.size()) > node.getNodeDataLength()) {

		// Increase file size
		if (FileSystem::i()->expandNode(node, (rwOffset + toWriteSize.size() + toWriteValue.size()) - node.getNodeDataLength()) != fsSuccess)
			return false;
	}
	if (!writeMem(toWriteSize)) return false;
	if (!writeMem(toWriteValue)) return false;
	FS_CHECK_BOOL;
	return true;
}

// Write a boolean value; warning: not thread safe without locking!
bool File::writeBool(const bool value)
{
	fsReturnCode retval;
	FS_CHECK_BOOL;
	if (!valid || !isWritable) return false;

	char toWrite = value ? 0xFF : 0x00;

	CHECK_N_UPDATE_NODE(node, path);

	if ( (rwOffset + 1) > node.getNodeDataLength()) {

		// Increase file size
		retval = FileSystem::i()->expandNode(node, (rwOffset + 1) - node.getNodeDataLength());
		if (retval != fsSuccess) {
			return false;
		}
	}

	if (filePtr[0] != toWrite) {
		filePtr[0] = toWrite;
		FileSystem::i()->updateTag();
		retval = FileSystem::i()->getNodeByName(node, path);

		if (retval != fsSuccess) {
			valid = false;
			return false;
		}
	}
	rwOffset += 1;
	FS_CHECK_BOOL;
	return true;
}

// Rewind the file
bool File::rewind()
{
	if (!valid) return false;

	rwOffset = 0;

	return true;
}

// Seek to the specified position relative to the start of the file; if no
// argument is specified this operation seeks to the end of the file
bool File::seek(long offset /* = -1 */)
{

	if (!valid) return false;

	if (offset == -1)
	{
		rwOffset = node.getNodeDataLength();
		return true;
	}
	else
	{
		if (offset > node.getNodeDataLength())
			return false;
		rwOffset = offset;
		return true;
	}
}

// Flush the buffered stream to background storage
bool File::flush()
{

	return valid && !FileSystem::i()->flush();
}

