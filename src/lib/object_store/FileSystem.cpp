/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*****************************************************************************
  FileSystem.cpp

  Helper functions for accessing file system.
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fstream>

#include "config.h"
#include "OSPathSep.h"
#include "log.h"
#include "Configuration.h"
#include "AESKey.h"
#include "FileSystem.h"
#include "ByteStringPreAlloc.h"
#include "SymmetricKey.h"
#include "SymmetricAlgorithm.h"
#include "CryptoFactory.h"
#include "MemMgr.h"

//#define SAVE_SECDB_FILES

#define AEK_KEY_MAGIC "securedb"
#define AES_KEY_LENGTH (128/8)
#define AES_KEY_CHECKSUM_SIZE (4)

#define KEY_MAP_LENGTH (strlen(AEK_KEY_MAGIC) + AES_KEY_LENGTH + AES_KEY_CHECKSUM_SIZE)

#define DBSTORE_FILE_NAME "./dbstore.bin"
#define DBSTORE_MAGIC 0x5ECF5DB0
#define DBSTORE_MEMSET_VAL 254

#define DBSTORE_KEY_FILE_NAME "./dbstore.key.bin"

#define	fsHdrOffset32Magic	(0)
#define	fsHdrOffset32Tag	(1 + fsHdrOffset32Magic)
#define	fsHdrOffset32Size	(1 + fsHdrOffset32Tag)
#define	fsHdrOffset32Node	(1 + fsHdrOffset32Size)

#define FS_MAGIC	*(((volatile uint32_t *) FileSystem::rootPtr) + fsHdrOffset32Magic)
#define FS_TAG		*(((volatile uint32_t *) FileSystem::rootPtr) + fsHdrOffset32Tag)
#define FS_LEN		*(((volatile uint32_t *) FileSystem::rootPtr) + fsHdrOffset32Size)

#define DATA_ALIGN_4(x) 	((((uint32_t)x) + 3) & ~3)

#define MAX_ITER_COUNT 1000

#define GET_NEXT_NODE(node) (node.rootPtr + node.getAllocLength())

// Initialise the one-and-only instance
std::auto_ptr<FileSystem> FileSystem::instance(NULL);

// Return the one-and-only instance
FileSystem* FileSystem::i()
{
	int32_t const version = Configuration::i()->getInt("secstore.version", 2);
	if (!instance.get())
	{
		if (version < 3) {
			instance = std::auto_ptr<FileSystem>(new FileSystem());
		} else {
			instance = std::auto_ptr<FileSystem>(new FileSystem(version));
		}
	}
	return instance.get();
}

void FileSystem::reset()
{
	if (instance.get())
		instance.reset();
}


FileSystem::FileSystem()
{
	int i;
	int max_index = 0;
	uint32_t max_tag = 0;
	char *fp = NULL;
	struct stat stat;
	int32_t fl_handle;
	uint32_t *hdrp;
	std::vector<ByteString> vbuf;
	ByteString keyData;
	MapRegion *keyMap = 0;
	char *keyPtr = 0;
	int secstoreKeyBase = Configuration::i()->getInt("secstore.key.addr");
	int secstoreBase = Configuration::i()->getInt("secstore.mem.addr");
	maxSize = Configuration::i()->getInt("secstore.mem.size");
	reInitNeeded = false;
	rootPtr = 0;
	locked = false;
	valid = false;
	version = 2;
	map = NULL;

	/* If internal memory is being used by MemMgr then leave that space */
	std::string heapStr;
	heapStr = Configuration::i()->getString("secstore.osslheap.internal",
						DEFAULT_OSSL_HEAP_FLAG);
	if (!heapStr.compare("yes")) {
		maxSize -= MEMMGR_POOL_SIZE;
		INFO_MSG("restricting internal memory usage to length 0x%x",
				maxSize);
	}

#ifndef FILE_REGION_MAP
	INFO_MSG("Starting filesystem at 0x%x length 0x%x", secstoreBase, maxSize);

	map = new MapRegion ((void *) secstoreBase, maxSize, readWriteMode);
#else
	INFO_MSG("Starting filesystem from %s, length 0x%x", DBSTORE_FILE_NAME, maxSize);

	map = new MapRegion ((std::string) DBSTORE_FILE_NAME, readWriteMode);
#endif
	if (!map->isValid()) {
		ERROR_MSG("Can't map 0x%x length 0x%x for filesystem", secstoreBase, maxSize);
		goto close_n_exit;
	}

	maxSize = map->getLength();

	rootPtr = (char *) map->getPtr();
	memset (rootPtr, DBSTORE_MEMSET_VAL, maxSize);

	if (!fileStore.isValid()) {
		ERROR_MSG("Invalid filestore for FileSystem");
		goto close_n_exit;
	}

	if (!fileStore.getData(vbuf) && !vbuf.size() ) {
		ERROR_MSG("Could not get data for the FileStore");
	} else {

#ifndef FILE_REGION_MAP
		keyMap = new MapRegion((void *) secstoreKeyBase, KEY_MAP_LENGTH, readMode);
#else
		keyMap = new MapRegion ((std::string) DBSTORE_KEY_FILE_NAME, readMode);
#endif
		if (!keyMap->isValid()) {
			ERROR_MSG("Can't map 0x%x length 0x%x for securedb key",
				secstoreKeyBase, AES_KEY_LENGTH);
			goto close_n_exit;
		}

		keyPtr = getFileSystemKey((char *) keyMap->getPtr(), keyMap->getLength());
		if (!keyPtr) {
			ERROR_MSG("checkFileSystemKey returned false");
			goto close_n_exit;
		}

		keyData.resize(AES_KEY_LENGTH);
		memcpy (keyData.byte_str(), keyPtr, AES_KEY_LENGTH);

		for (i = 0; i < vbuf.size(); i++) {

			if (!decryptFS (keyData, vbuf[i])) continue;

			if (FS_MAGIC != DBSTORE_MAGIC) continue;

			if (!checkHash()) continue;

			if (max_tag <= FS_TAG) {
				max_tag = FS_TAG;
				max_index = i;
			}

			valid = true;
		}

		if (max_tag != FS_TAG) {
			if (!decryptFS (keyData, vbuf[max_index])) {
				ERROR_MSG ("Unexpected error while decrypting FS with index %d",
						max_index);
				goto close_n_exit;
			}
		}
		keyData.wipe();

	}

	// Process the filesystem
	hdrp = (uint32_t *) rootPtr;
	// Check filesystem, if not there, reset it
	if ((FS_MAGIC != DBSTORE_MAGIC)
			|| (FS_LEN > maxSize) || (!valid)) {

		DEBUG_MSG ("[Re]Creating DB store in %s", DBSTORE_FILE_NAME);

		memset (rootPtr, DBSTORE_MEMSET_VAL, maxSize);
		reInitNeeded = true;
		FS_MAGIC = DBSTORE_MAGIC;
		FS_LEN = fsHdrOffset32Node * 4;
		FS_TAG = 1;
	}

	if (FS_LEN > maxSize) {
		ERROR_MSG ("File system corrupted, fs size %d, file size %d", FS_LEN, maxSize);
		goto close_n_exit;
	}

	valid = true;
	locked = false;

	if (!fsIntegrityCheck(__func__, __LINE__)) {
		ERROR_MSG ("FileSystem validity check failed\n");
		valid = false;
		goto close_n_exit;
	}

	(void)flush();

close_n_exit:
	vbuf.clear();
	keyData.wipe();
	if(keyMap) {
		delete keyMap;
	}

}


// Constructor
FileSystem::FileSystem(bool version3)
{
	uint32_t i;
	char *fp = NULL;
	struct stat stat;
	int32_t fl_handle;
	uint32_t *hdrp;
	std::vector<ByteString> vbuf;
	maxSize = Configuration::i()->getInt("secstore.mem.size");
	rootPtr = 0;
	locked = false;
	valid = false;
	version = 3;
	reInitNeeded = false;
	map = NULL;

    //Store mem size can not be bigger than secstore sharedmem size - 28
    //The 28 is needed for hash header / command
    if( (0x10000-28) < maxSize){
        ERROR_MSG("secstore.mem.size 0x%x exeeds maximum value 0xfc00",maxSize);
        return;
    }

	if (fileStore.isValid()) {
		if (fileStore.getData(vbuf) && vbuf.size() ) {
			char * validRootPtr = NULL;
			uint8_t validCount = 0;
			for (i = 0; i < vbuf.size(); i++) {
				ByteString buf = vbuf.at(i);
				rootPtr = (char *) malloc(maxSize);
				if (rootPtr) {
					memcpy(rootPtr, buf.byte_str(), buf.size());
					DEBUG_MSG("Mapped storage at %p", rootPtr);

					if (FS_MAGIC == DBSTORE_MAGIC) {
						DEBUG_MSG("File storage has magic value in place");
						if (checkHash()) {
							DEBUG_MSG("File storage passed hash check! at %p", rootPtr);
							validCount++;
							valid = true;
							if(validRootPtr == NULL) {
								validRootPtr = rootPtr; /*Select first valid one as rootPtr*/
							} else {
								free(rootPtr);
							}
						} else {
							ERROR_MSG("File storage failed hash check! at %p", rootPtr);
							free(rootPtr);
						}
					} else {
						ERROR_MSG("File storage do not have magic value in place");
						free(rootPtr);
					}
				}
			}
			if(validCount > 0) {
				rootPtr = validRootPtr;
			} else {
				/* Special case: no valid storage found. Use first as rootPtr*/
				ByteString buf = vbuf.at(0);
				rootPtr = (char *) malloc(maxSize);
				if (rootPtr) {
					memcpy(rootPtr, buf.byte_str(), buf.size());
				} else {
					ERROR_MSG("Failed to allocate memory for storage root. Exiting");
					return;
				}
				ERROR_MSG("Did not find any valid storage, setting rootPtr %p", rootPtr);
			}

			// Process the filesystem
			hdrp = (uint32_t *) rootPtr;
			// Check filesystem, if not there, reset it
			if ((FS_MAGIC != DBSTORE_MAGIC)
					|| (FS_LEN > maxSize) || (!valid)) {

				DEBUG_MSG ("[Re]Creating DB store");
				memset (rootPtr, 0, maxSize);
				reInitNeeded = true;
				FS_MAGIC = DBSTORE_MAGIC;
				FS_LEN = fsHdrOffset32Node * 4;
				FS_TAG = 1;
				valid = true;
			}

			if (FS_LEN <= maxSize) {

				if (fsIntegrityCheck(__func__, __LINE__)) {
					DEBUG_MSG("Filesystem validity check passed");
					valid = true;
					(void)flush();
				} else {
					ERROR_MSG ("FileSystem validity check failed");
					valid = false;
				}
			} else {
				ERROR_MSG ("File system corrupted, fs size %d, file size %d", FS_LEN, maxSize);
			}
		} else {
			ERROR_MSG("Could not get data for the FileStore");
		}

	} else {
		ERROR_MSG("Invalid filestore for FileSystem");
	}
	vbuf.clear();
}

FileSystem::~FileSystem()
{
	DEBUG_MSG ("FileSystem desctructor called");

	if (locked) {
		unlockFS();
	}

	if (!isValid()) {
		return;
	}

	if(version < 3) {

		if (rootPtr > 0) {
			munmap (rootPtr, maxSize);
			rootPtr = 0;
			maxSize = 0;
		}
		if(map) {
			delete map;
		}
	} else {
		if(rootPtr) {
			free(rootPtr);
		}
	}
	valid = false;
}

unsigned int FileSystem::getFSSize()
{
	return FS_LEN;
}

// Lock the filesystem
bool FileSystem::lockFS(bool block /* = true */, fsLockType type /* = fsLockTypeRead */)
{
	if(version<3) {
		return map->lock(block, (type == fsLockTypeWrite) ? lockTypeWrite : lockTypeRead);
	} else {
		return true;
	}
}

// Unlock the filesystem
bool FileSystem::unlockFS()
{
	if(version<3) {
		return map->unlock();
	} else {
		return true;
	}
}

// Flush the filesystem to background storage
bool FileSystem::flush()
{
	ByteStringPreAlloc *fileSystemBuf = NULL;
	MapRegion *keyMap = 0;
	ByteString encryptedBuf, encryptedBufFinal;
	bool retval = false;

	int32_t const version = Configuration::i()->getInt("secstore.version", 2);
	if(version < 3) {
		ByteString keyData, hash;
		AESKey aesKey(AES_KEY_LENGTH * 8);
		char *keyPtr = 0;
		int secstoreKeyBase = Configuration::i()->getInt("secstore.key.addr");
		SymmetricAlgorithm *aes = CryptoFactory::i()->getSymmetricAlgorithm("aes");
		if (!aes) {
			ERROR_MSG("Can't get CryptoFactory algorithm for AES");
			goto close_n_exit;
		}

		if (!valid) {
			ERROR_MSG("Could not update background storage, file system is invalid");
			goto close_n_exit;
		}

		if (!fsIntegrityCheck(__func__, __LINE__)) {
			ERROR_MSG("Could not update background storage, file system corrputed");
			goto close_n_exit;
		}

		if (!fileStore.isValid()) {
			ERROR_MSG("Invalid filestore for FileSystem");
			goto close_n_exit;
		}

#ifndef FILE_REGION_MAP
		keyMap = new MapRegion((void *) secstoreKeyBase, KEY_MAP_LENGTH, readMode);
#else
		keyMap = new MapRegion ((std::string) DBSTORE_KEY_FILE_NAME, readWriteMode);
#endif
		if (!keyMap->isValid()) {
			ERROR_MSG("Can't map 0x%x length 0x%x for securedb key",
							secstoreKeyBase, AES_KEY_LENGTH);
			goto close_n_exit;
		}

		keyPtr = getFileSystemKey((char *) keyMap->getPtr(), keyMap->getLength());
		if (!keyPtr) {
			ERROR_MSG("checkFileSystemKey returned false");
			goto close_n_exit;
		}

		keyData.resize(AES_KEY_LENGTH);
		memcpy (keyData.byte_str(), keyPtr, AES_KEY_LENGTH);
		if (!aesKey.setKeyBits(keyData)) {
			ERROR_MSG("AES setKeyBits failed for AES decryption");
			goto close_n_exit;
		}

#ifdef SAVE_SECDB_FILES
		{
			std::ofstream ofs;
			INFO_MSG("Writing encrypteddbuf.bin of size %d", FS_LEN);
			ofs.open("./encryptedbuf.bin", std::ios::binary | std::ios::out | std::ios::trunc);
			ofs.write((char *) rootPtr, FS_LEN);
			ofs.close();
		}
#endif

		if (!getFSHash(hash)) {
			ERROR_MSG("can not calculate hash of the filesystem");
			goto close_n_exit;
		}

		if (FS_LEN + hash.size() > maxSize) {
			ERROR_MSG("not enough space to store hash data");
			goto close_n_exit;
		}

		memcpy (rootPtr + FS_LEN, hash.const_byte_str(), hash.size());

		fileSystemBuf = new ByteStringPreAlloc((unsigned char *) rootPtr, maxSize,
								FS_LEN + hash.size());

		if (!aes->encryptInit(&aesKey, "cbc")) {
			ERROR_MSG("encryptInit failed for AES encryption of secure DB");
			goto close_n_exit;
		}
		if (!aes->encryptUpdate(*(ByteString *)fileSystemBuf, encryptedBuf)) {
			ERROR_MSG("decryptUpdate failed for AES encryption of secure DB");
			goto close_n_exit;
		}
		if (!aes->encryptFinal(encryptedBufFinal)) {
			ERROR_MSG("encryptFinal failed for AES encryption");
			goto close_n_exit;
		}
		encryptedBuf += encryptedBufFinal;
		fileSystemBuf->wipe();
		keyData.wipe();
		hash.wipe();

		if (!fileStore.putData(encryptedBuf)) {
			ERROR_MSG("Could not get data for the FileSystem");
			goto close_n_exit;
		}

		retval = true;
	}
	else {
		ByteString hash;
		DEBUG_MSG ("FileSystem::flush called");

		if (!valid) {
			ERROR_MSG("Could not update background storage, file system is invalid");
			goto close_n_exit;
		}

		if (!fsIntegrityCheck(__func__, __LINE__)) {
			ERROR_MSG("Could not update background storage, file system corrupted");
			goto close_n_exit;
		}

		if (!fileStore.isValid()) {
			ERROR_MSG("Invalid filestore for FileSystem");
			goto close_n_exit;
		}

		if (!getFSHash(hash)) {
			ERROR_MSG("can not calculate hash of the filesystem");
			goto close_n_exit;
		}

		if (FS_LEN + hash.size() > maxSize) {
			ERROR_MSG("not enough space to store hash data");
			goto close_n_exit;
		}

		memcpy (rootPtr + FS_LEN, hash.const_byte_str(), hash.size());

		fileSystemBuf = new ByteStringPreAlloc((unsigned char *) rootPtr, maxSize,
								maxSize);

		hash.wipe();

		if (!fileStore.putData(*(ByteString *)fileSystemBuf)) {
			ERROR_MSG("Could not get data for the FileSystem");
			goto close_n_exit;
		}

		retval = true;
	}
close_n_exit:
	if(version < 3) {
		if (keyMap) {
			delete keyMap;
		}
		encryptedBuf.wipe();
		encryptedBufFinal.wipe();
	}
	if (fileSystemBuf) {
		delete fileSystemBuf;
	}
	return retval;
}

void FileSystem::updateTag()
{
	FS_TAG += 1;
}

bool FileSystem::fsNodeIntegrityCheck(const FSNode &node)
{
	if (node.tag != FS_TAG) {
		return false;
	}

	if ((node.getNodeType() != 1) && (node.getNodeType() != 2)){
		return false;
	}

	if ((node.getNodeNameLength() == 0) ||
			(node.getNodeNameLength() > DBSTORE_MAX_NAME_LENGTH)) {
		return false;
	}

	if (node.rootPtr + fsNodeHdrLength + node.getNodeDataLength() > rootPtr + FS_LEN) {
		return false;
	}

	if ((!node.getAllocLength()) || (node.getAllocLength() % 4)) {
		return false;
	}

	if (node.getAllocLength() < fsNodeHdrLength + node.getNodeDataLength()) {
		return false;
	}

	if (node.rootPtr + node.getAllocLength() > rootPtr + FS_LEN) {
		return false;
	}

	return true;
}

bool FileSystem::fsIntegrityCheck(const char *func /* = "" */, int line /*= 0*/)
{
	FSNode node;

	if (!valid || !rootPtr) {
		ERROR_MSG("%s:%d, File system not valid (v: %d, rp: %p)\n",
				func, line, valid, rootPtr);
		return false;
	}

	if ((FS_MAGIC != DBSTORE_MAGIC)
			|| (FS_LEN > maxSize)) {
		ERROR_MSG("%s:%d, Error in header: magic and/or size\n", func, line);
		return false;
	}

	node.tag = FS_TAG;

	node.rootPtr = rootPtr + (fsHdrOffset32Node * 4);

	while (node.rootPtr - rootPtr < FS_LEN) {

		if (!fsNodeIntegrityCheck(node)) {
			ERROR_MSG("Error in fsIntegrityCheck for offset 0x%x(%p)\n",
					node.rootPtr - rootPtr, node.rootPtr);
			return false;
		}

		node.rootPtr = GET_NEXT_NODE(node);
	}

	return true;
}

fsReturnCode FileSystem::getNextNode(FSNode &node)
{
	if (valid == false) {
		return fsInvalid;
	}

	if (!node.rootPtr) {

		if (FS_LEN <= fsHdrOffset32Node * 4) {
			return fsEOF;
		}

		node.tag = FS_TAG;

		node.rootPtr = rootPtr + (fsHdrOffset32Node * 4); 

		return fsSuccess;
	}

	if (node.tag != FS_TAG) {
		return fsErrorTag;
	}

	if (node.rootPtr + node.getAllocLength() - rootPtr >= FS_LEN) {
		return fsEOF;
	}

	node.rootPtr = GET_NEXT_NODE(node);

	return fsSuccess;
}

fsReturnCode FileSystem::addNode(uint32_t type, std::string name, uint32_t dataLength)
{
	FSNode node;
	fsReturnCode retval;
	char *nodePtr;
	uint32_t nodeAllocLength;

	if (valid == false) {
		return fsInvalid;
	}

	if ((type > 2) || (name.size() == 0) || (name.size() > DBSTORE_MAX_NAME_LENGTH)) {
		ERROR_MSG("Node name %s exceeds maximum size %d", name.c_str(), DBSTORE_MAX_NAME_LENGTH);
		return fsInvalid;
	}

	if (type == FSNodeTypeDirectory) {
		dataLength = 0;
	}

	retval = getNodeByName (node, name);

	if (retval == fsSuccess) {
		// The node already present we are done
		if (node.getNodeType() == type)
			return fsSuccess;

		ERROR_MSG("The incorrect node type %d for %s",
				type, name.c_str());
		return fsInvalid;
	}

	if (retval != fsNoNode) {
		ERROR_MSG("The getNodeByName returned incorrect value %d for %s",
				retval, name.c_str());
		return fsErrorGen;
	}

	nodeAllocLength = DATA_ALIGN_4(fsNodeHdrLength + dataLength); 

	if (FS_LEN + nodeAllocLength > maxSize  - FileSystem::getFSHashLength()) {
		ERROR_MSG("No more space in filesystem current %d, max size %d for %s",
				FS_LEN, maxSize, name.c_str());
		return fsInvalid;
	}

	nodePtr = rootPtr + FS_LEN;

	WRITE_UINT32(&nodePtr[FSNodeTypeOffset], type);
	WRITE_UINT32(&nodePtr[FSNodeNameLengthOffset], name.size());
	memset(&nodePtr[FSNodeNameOffset], 0, DBSTORE_MAX_NAME_LENGTH);
	strncpy(&nodePtr[FSNodeNameOffset], name.c_str(), DBSTORE_MAX_NAME_LENGTH);
	WRITE_UINT32(&nodePtr[FSNodeDataLengthOffset], dataLength);
	WRITE_UINT32(&nodePtr[FSNodeAllocLengthOffset], nodeAllocLength);

	FS_LEN += nodeAllocLength;

	updateTag();

	return fsSuccess;
}

fsReturnCode FileSystem::deleteNode(std::string name)
{
	FSNode node;
	fsReturnCode retval;
	bool found = false;
	uint32_t blockSize;
	char *nextNodePtr;
	int iter = 0;

	if (valid == false) {
		return fsInvalid;
	}

	if ((name.size() == 0) || (name.size() > DBSTORE_MAX_NAME_LENGTH)) {
		ERROR_MSG("Node name %s exceeds maximum size %d", name.c_str(), DBSTORE_MAX_NAME_LENGTH);
		return fsInvalid;
	}

	while ( true ) {
		retval = getNextNode(node);

		if (iter++ >= MAX_ITER_COUNT) {
			ERROR_MSG("deleteNode, Max iteration exceeded for delete directory %s",
					name.c_str());
			return fsErrorGen;
		}

		if (retval == fsEOF) {
			break;
		}

		if (retval == fsErrorTag) {
			node.reset();
			found = false;
			continue;
		}

		if (retval != fsSuccess) {
			ERROR_MSG("deleteNode, Filesystem returned error (%d) for directory %s",
					retval, name.c_str());
			return fsErrorGen;
		}

		if (!node.getNodeNameLength()) {
			ERROR_MSG("Filesystem returned 0 name length for directory %s",
					retval, name.c_str());
			return fsErrorGen;
		}

		if (name.size() > node.getNodeNameLength()) {
			continue;
		}

		if (name.compare(0, name.size(), node.getNodeName(), name.size())) {
			continue;
		}

		found = true;

		nextNodePtr = GET_NEXT_NODE(node);
		blockSize = node.getAllocLength();
		memcpy (node.rootPtr, nextNodePtr,
				FS_LEN - (nextNodePtr - FileSystem::rootPtr));

		FS_LEN -= blockSize;

		memset (rootPtr + FS_LEN, DBSTORE_MEMSET_VAL, maxSize - FS_LEN);

		node.reset();
	}

	if (!found)
		return fsNoNode;

	updateTag();

	return fsSuccess;
}

fsReturnCode FileSystem::getNodeByName(FSNode &node, std::string name)
{
	fsReturnCode retval;
	bool found = false;

	if (valid == false) {
		return fsInvalid;
	}

	if ((name.size() == 0) || (name.size() > DBSTORE_MAX_NAME_LENGTH)) {
		ERROR_MSG("Node name %s exceeds maximum size %d", name.c_str(), DBSTORE_MAX_NAME_LENGTH);
		return fsInvalid;
	}

	node.reset();

	while ( true ) {
		retval = getNextNode(node);

		if (retval == fsEOF) {
			break;
		}

		if (retval == fsErrorTag) {
			node.reset();
			found = false;
			continue;
		}

		if (retval != fsSuccess) {
			ERROR_MSG("deleteNode, Filesystem returned error (%d) for directory %s",
					retval, name.c_str());
			return fsErrorGen;
		}

		if (!node.getNodeNameLength()) {
			ERROR_MSG("Filesystem returned %d name length for directory %s",
					retval, name.c_str());
			return fsErrorGen;
		}

		if (name.size() != node.getNodeNameLength()) {
			continue;
		}

		if (name.compare(0, name.size(), node.getNodeName(), name.size())) {
			continue;
		}

		found = true;

		break;

	}

	if (!found)
		return fsNoNode;

	return fsSuccess;

}

fsReturnCode FileSystem::expandNode(FSNode &node, uint32_t length)
{
	uint32_t nodeOffset = node.rootPtr - rootPtr;
	uint32_t nodeSize = node.getAllocLength();

	if (nodeOffset + nodeSize > FS_LEN) {
		ERROR_MSG("expandNode, Filesystem/node corrupted node size %d fs size %d",
				nodeOffset + fsNodeHdrLength + node.getNodeDataLength(), FS_LEN);
		return fsErrorGen;
	}

	if (fsNodeHdrLength + node.getNodeDataLength() + length <= nodeSize) {
		WRITE_UINT32(&node.rootPtr[FSNodeDataLengthOffset], node.getNodeDataLength() + length);
		return fsSuccess;
	}

	uint32_t expandLength = DATA_ALIGN_4(fsNodeHdrLength + node.getNodeDataLength() + length - nodeSize);

	if (FS_LEN + expandLength > maxSize - FileSystem::getFSHashLength()) {
		ERROR_MSG("expandNode, Can't expand file system for %d, maxSize %d",
				expandLength, maxSize);
		return fsErrorGen;
	}

	if (nodeOffset + nodeSize < FS_LEN) {
		// Move the node to end of FS

		char *tmpBuffer = (char *) malloc(nodeSize);
		char *nextNodePtr = GET_NEXT_NODE(node);

		if (!tmpBuffer) {
			ERROR_MSG("expandNode, Can't allocate memory for size %d",
					fsNodeHdrLength + node.getNodeDataLength());
			return fsErrorGen;
		}

		// Copy the node to a temporary buffer
		memcpy (tmpBuffer, node.rootPtr, nodeSize);

		// Move the next node to end of FS to current node
		memcpy (node.rootPtr, nextNodePtr, FS_LEN - (nextNodePtr - rootPtr));

		// Copy the temporary buffer to end of FS
		memcpy (rootPtr + FS_LEN - nodeSize, tmpBuffer, nodeSize);

		free (tmpBuffer);

		node.rootPtr = rootPtr + FS_LEN - nodeSize;
	}

	WRITE_UINT32(&node.rootPtr[FSNodeDataLengthOffset], node.getNodeDataLength() + length);
	WRITE_UINT32(&node.rootPtr[FSNodeAllocLengthOffset], nodeSize + expandLength);

	FS_LEN += expandLength;

	updateTag();

	node.tag = FS_TAG;

	return fsSuccess;
}

char *FileSystem::getFileSystemKey(char *keyPtr, unsigned int length)
{
	uint32_t checksum = 0;
	volatile uint32_t *keyPtr32;
	int i;

	if (length < KEY_MAP_LENGTH) {
			ERROR_MSG("insufficient length for key data %d, needed %d",
						length, KEY_MAP_LENGTH);
			return 0;
	}

	if (memcmp (keyPtr, AEK_KEY_MAGIC, strlen(AEK_KEY_MAGIC))) {
			ERROR_MSG("invalid key data magic header");
			return 0;
	}

	keyPtr += strlen(AEK_KEY_MAGIC);

	keyPtr32 = (volatile uint32_t *) keyPtr;

	for (i = 0; i < AES_KEY_LENGTH/sizeof(uint32_t); i++) {
		checksum ^= keyPtr32[i];
	}

	if (checksum != keyPtr32[i]) {
			ERROR_MSG("checksum failed for keydata");
			return 0;
	}

	return keyPtr;
}

bool FileSystem::decryptFS(ByteString &aesKeyData, ByteString &encryptedFS)
{
	bool retval = false;
	AESKey aesKey(AES_KEY_LENGTH * 8);
	ByteStringPreAlloc *decryptedBuf = 0;
	ByteStringPreAlloc *decryptedBufFinal = 0;

	SymmetricAlgorithm *aes = CryptoFactory::i()->getSymmetricAlgorithm("aes");
	if (!aes) {
		ERROR_MSG("Can't get CryptoFactory algorithm for AES");
		goto close_n_exit;
	}

	if (!aesKey.setKeyBits(aesKeyData)) {
		ERROR_MSG("AES setKeyBits failed for AES decryption");
		goto close_n_exit;
	}

	if (!aes->decryptInit(&aesKey, "cbc")) {
		ERROR_MSG("decryptInit failed for AES decryption of secure DB");
		goto close_n_exit;
	}

	decryptedBuf = new ByteStringPreAlloc((unsigned char *) rootPtr, maxSize);

	if (!aes->decryptUpdate(encryptedFS, *decryptedBuf)) {
		ERROR_MSG("decryptUpdate failed for AES decryption of secure DB");
		goto close_n_exit;
	}

	decryptedBufFinal = new ByteStringPreAlloc((unsigned char *) rootPtr + decryptedBuf->size(),
							maxSize - decryptedBuf->size());
	if (!aes->decryptFinal(*(ByteString *)decryptedBufFinal)) {
		ERROR_MSG("decryptFinal failed for AES decryption");
		goto close_n_exit;
	}

#ifdef SAVE_SECDB_FILES
	{
		std::ofstream ofs;
		INFO_MSG("Writing decryptedbuf.bin of size %d",
			decryptedBuf->size() + decryptedBufFinal->size());
		ofs.open("./decryptedbuf.bin", std::ios::binary | std::ios::out | std::ios::trunc);
		ofs.write((char *) rootPtr, decryptedBuf->size() + decryptedBufFinal->size());
		ofs.close();
	}
#endif

	retval = true;

close_n_exit:
	if (aes) CryptoFactory::i()->recycleSymmetricAlgorithm(aes);
	if (decryptedBuf) delete decryptedBuf;
	if (decryptedBufFinal) delete decryptedBufFinal;

	return retval;
}

bool FileSystem::getFSHash(ByteString &hashBuf)
{
	bool retval = false;
	ByteStringPreAlloc fileSystemBuf((unsigned char *) rootPtr,
							maxSize, FS_LEN);

	HashAlgorithm * hash;
	int32_t const version = Configuration::i()->getInt("secstore.version", 2);
	if(version < 3) {
		hash = CryptoFactory::i()->getHashAlgorithm("md5");
	} else {
		hash = CryptoFactory::i()->getHashAlgorithm("hmacsha1");
	}
	if (!hash) {
		ERROR_MSG("Can't get CryptoFactory algorithm!");
		goto close_n_exit;
	}

	if (!hash->hashInit()) {
		ERROR_MSG("Hash init failed");
		goto close_n_exit;
	}


	if (!hash->hashUpdate(fileSystemBuf)) {
		ERROR_MSG("Hash update failed");
		goto close_n_exit;
	}

	if (!hash->hashFinal(hashBuf)) {
		ERROR_MSG("Hash final failed");
		goto close_n_exit;
	}

	retval = true;

close_n_exit:

	if (hash) CryptoFactory::i()->recycleHashAlgorithm(hash);

	return retval;
}

bool FileSystem::checkHash()
{
	ByteString hashBuf;
	char *hashPtr = rootPtr + FS_LEN;

	if (!getFSHash(hashBuf)) {
		ERROR_MSG("hash calculation failed");
		return false;
	}

	if (memcmp((const void *) hashPtr, hashBuf.const_byte_str(),
				hashBuf.size())) {
		ERROR_MSG("hash check failed");
		return false;
	}

	return true;
}


int FileSystem::getFSHashLength() const
{
	int32_t const version = Configuration::i()->getInt("secstore.version", 2);
	int len;
	HashAlgorithm * hash;
	if(version < 3) {
		hash = CryptoFactory::i()->getHashAlgorithm("md5");
	} else {
		hash = CryptoFactory::i()->getHashAlgorithm("hmacsha1");
	}
	if (hash){
		len = hash->getHashSize();
		CryptoFactory::i()->recycleHashAlgorithm(hash);
		return len;
	}
	return -1;
}


