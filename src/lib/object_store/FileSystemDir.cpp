/* $Id: Directory.cpp 6786 2012-10-26 11:58:40Z rb $ */

/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * Copyright (c) 2010 SURFnet bv
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
  Directory.cpp

  Helper functions for accessing directories.
 *****************************************************************************/

#include <stdio.h>
#include <string>
#include <vector>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"
#include "FileSystem.h"
#include "Directory.h"
#include "OSPathSep.h"
#include "log.h"

#define FS_CHECK \
	do { \
		if (!FileSystem::i()->fsIntegrityCheck(__func__, __LINE__)) { \
			ERROR_MSG("File system intagrity check failed with file %s\n", path.c_str()); \
			valid = false; \
			return; \
		} \
	} while (0)

#define FS_CHECK_BOOL \
	do { \
		if (!FileSystem::i()->fsIntegrityCheck(__func__, __LINE__)) { \
			ERROR_MSG("File system intagrity check failed with file %s\n", path.c_str()); \
			valid = false; \
			return false; \
		} \
	} while (0)

// Constructor
Directory::Directory(std::string path, bool create /* = false */)
{
	fsReturnCode retval;

	if (create) {
		FileSystem::i()->lockFS();
		retval = FileSystem::i()->addNode(FSNodeTypeDirectory, path);

		if (retval != fsSuccess) {
			ERROR_MSG("Add node returned error %d for directory %s",
				  retval, path.c_str());
			FileSystem::i()->unlockFS();
			FS_CHECK;
			valid = false;
			return;
		}
		FileSystem::i()->unlockFS();

	}

	this->path = path;
	FS_CHECK;
	valid = refresh();
}

// Return a list of all files in a directory
std::vector<std::string> Directory::getFiles()
{

	return files;
}

// Return a list of all subdirectories in a directory
std::vector<std::string> Directory::getSubDirs()
{
	return subDirs;
}

// Refresh the directory listing
bool Directory::refresh()
{
	FSNode node;
	fsReturnCode retval;
	bool found = false;

	subDirs.clear();
	files.clear();

	while ( true ) {
		retval = FileSystem::i()->getNextNode(node);
		if (retval == fsEOF) {
			break;
		}

		if (retval == fsErrorTag) {
			node.reset();
			subDirs.clear();
			files.clear();
			found = false;
			continue;
		}

		if (retval != fsSuccess) {
			DEBUG_MSG("Filesystem returned error (%d) for directory %s",
				  retval, path.c_str());
			return false;
		}

		if (!node.getNodeNameLength()) {
			DEBUG_MSG("Filesystem returned 0 name length for directory %s",
				  retval, path.c_str());
			return false;
		}

		if (path.size() > node.getNodeNameLength()) {
			continue;
		}

		if (path.compare(0, path.size(), node.getNodeName(), path.size())) {
			continue;
		}

		found = true;

		if (path.size() == node.getNodeNameLength()) {
			continue;
		}

		char * name = node.getNodeName();
		name += path.size();


		if (name[0] != OS_PATHSEP[0]) {
			continue;
		}
		name += 1;

		std::string nodeName(name);

		if (node.getNodeType() == FSNodeTypeDirectory) {
			subDirs.push_back(nodeName);
		} else if (node.getNodeType() == FSNodeTypeFile) {
			files.push_back(nodeName);
		} else {
			DEBUG_MSG("Invalid node type %d for directory %s",
				  node.getNodeType(), path.c_str());
			return false;
		}
	}

	valid = true;
	FS_CHECK_BOOL;
	return found;
}

bool Directory::isValid()
{
	return valid;
}

// Create a new subdirectory
bool Directory::mkdir(std::string name)
{
	fsReturnCode retval;

	std::string fullPath = path + OS_PATHSEP + name;
	if (fullPath.size() > DBSTORE_MAX_NAME_LENGTH) {
		return false;
	}

	FileSystem::i()->lockFS();
	retval = FileSystem::i()->addNode(FSNodeTypeDirectory, fullPath);

	if (retval != fsSuccess) {
		ERROR_MSG("Add node returned error %d for directory %s",
			  retval, path.c_str());
		FileSystem::i()->unlockFS();
		return false;
	}
	FileSystem::i()->unlockFS();
	FS_CHECK_BOOL;
	return refresh();
}

// Delete a file or subdirectory in the directory
bool Directory::remove(std::string name)
{
	fsReturnCode retval;
	std::string fullPath;
	FS_CHECK_BOOL;
	if (path.compare(name))
		fullPath = path + OS_PATHSEP + name;
	else {
		fullPath = path;
		valid = false;
	}
	if (fullPath.size() > DBSTORE_MAX_NAME_LENGTH) {
		return false;
	}

	FileSystem::i()->lockFS();
	retval = FileSystem::i()->deleteNode(fullPath);
	if (retval != fsSuccess) {
		ERROR_MSG("Delete node returned error %d for directory %s",
			  retval, path.c_str());
		FileSystem::i()->unlockFS();
		return false;
	}
	FileSystem::i()->unlockFS();
	if (!valid) return true;
	FS_CHECK_BOOL;
	return refresh();
}

