/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "log.h"
#include "FileStore.h"

FileStore::FileStore()
{

	redundancy = FILESTORE_REDUNDANCY_LEVEL;

	// Assumption: Redundancy level == 2
	fileName[0] = Configuration::i()->getString("secstore.file0");
	fileName[1] = Configuration::i()->getString("secstore.file1");

	valid = true;
	locked = false;
}

bool FileStore::getData (std::vector<ByteString> &vbuf)
{
	bool retval = false;
	struct stat results;
	std::ifstream fs;
	int index;

	if (!valid) {
		return false;
	}

	if (locked) {
		return false;
	}

	vbuf.clear();

	for (index = 0; index < redundancy; index++) {

		ByteString buf;

		if (stat(fileName[index].c_str(), &results) == -1) {
			ERROR_MSG ("File does not exist: %s\n", fileName[index].c_str());
			continue;
		}
		if (!results.st_size) {
			continue;
		}
		buf.resize(results.st_size);

		if (buf.size() != results.st_size) {
			ERROR_MSG ("Can't allocate memory for store data\n");
			return false;
		}

		fs.open (fileName[index].c_str(), std::ios::binary | std::ios::in);

		if (!fs.is_open()) {
			ERROR_MSG ("File open failed for %s\n", fileName[index].c_str());
			continue;
		}

		fs.read ((char *) buf.byte_str(), buf.size());

		fs.close();

		vbuf.push_back(buf);

		retval = true;
	}


	return retval;
}

bool FileStore::putData (ByteString &buf)
{
	FILE *fp;

	//lock();
	if (!redundancy) return false;

	for (int i = 0; i < redundancy; i++) {
		fp = fopen (fileName[i].c_str(), "wb");

		if (!fp) {
			ERROR_MSG ("File open failed for %s\n", fileName[i].c_str());
			continue;
		}

		// Data length == 0 truncate the file size to 0
		if (buf.size())
			fwrite ((char *) buf.byte_str(), buf.size(), 1, fp);

		fflush(fp);
		fsync(fileno(fp));
		fclose(fp);
	}

	return true;
}
