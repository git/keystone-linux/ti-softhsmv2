/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 * Copyright (c) 2010 SURFnet bv
 * Copyright (c) 2010 .SE (The Internet Infrastructure Foundation)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
 SoftHSM.cpp

 The implementation of the SoftHSM's main class
 *****************************************************************************/

/*****************************************************************************
 ChangeLog (01/03/2013): Changed all PKCS11 API's to be a wrapper to serialize
 the API and send to daemon.
 *****************************************************************************/

#include "config.h"
#include "SoftHSM-client.h"
#include "log.h"
#include "Configuration.h"
#include "SimpleConfigLoader.h"
#include "MutexFactory.h"
#include "osmutex.h"

#include "format_req_msg.h"
#include "parse_rsp_msg.h"
#include "msg_deserialize.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <sstream>

/*****************************************************************************
 Implementation of SoftHSM class specific functions
 *****************************************************************************/

// Initialise the one-and-only instance
std::auto_ptr<SoftHSM> SoftHSM::instance(NULL);

// Return the one-and-only instance
SoftHSM* SoftHSM::i()
{
    if (!instance.get())
    {
    	instance = std::auto_ptr<SoftHSM>(new SoftHSM());
    }

    return instance.get();
}

void SoftHSM::reset()
{
    if (instance.get())
    	instance.reset();
}

// Constructor
SoftHSM::SoftHSM()
{
    memset(&ServerSockAddr, 0, sizeof(sockaddr_un));
    SockMutex = NULL;
    isInitialised = false;
}

// Destructor
SoftHSM::~SoftHSM()
{
}

void SoftHSM::DeInitializeSocket()
{
    MutexFactory::i()->recycleMutex(SockMutex);
    ClientSock.reset();
}

int SoftHSM::InitializeSocket()
{
    std::string ClientSockName;
    std::string ServerSockName;
    std::ostringstream pid;

    // (Re)load the configuration
    if (!Configuration::i()->reload(SimpleConfigLoader::i()))
    {
    	ERROR_MSG("Could not load configuration");
    	return -1;
    }

    // Get the daemon/client socket names 
    ServerSockName = Configuration::i()->getString("sockets.serversock", DEFAULT_SERVERSOCK);
    if (ServerSockName.empty())
    {
    	ERROR_MSG("Could not get server socket");
    	return -1;
    }

    ServerSockAddr.sun_family = AF_UNIX;
    strncpy(ServerSockAddr.sun_path, ServerSockName.c_str(), UNIX_PATH_MAX-1);

    ClientSockName = Configuration::i()->getString("sockets.clientsock", DEFAULT_CLIENTSOCK);
    if (ClientSockName.empty())
    {
    	ERROR_MSG("Could not get client socket");
    	return -1;
    }

    /* Get PID and append to name */
    pid << getpid();
    ClientSockName += ".";
    ClientSockName += pid.str();

    ClientSock = std::auto_ptr<UnixDatagramSocket>(new UnixDatagramSocket(ClientSockName));

    if (!ClientSock.get())
    {
    	ERROR_MSG("Could not create client socket");
    	return -1;
    }

    // Get a mutex
    SockMutex = MutexFactory::i()->getMutex();

    if (SockMutex == NULL)
    {
    	ERROR_MSG("Could not create socket Mutex");
    	return -1;
    }

    return 0;
}

int SoftHSM::MsgSendRecv(msg_t *msg, enum msg_id id, msg_t **rmsg) 
{

    int len;
    char *data = NULL;
    struct timeval tm = {65, 0}; //DSPif has 60 second timeout.
    msg_t *pmsg;
    struct sockaddr_un *sock_addr = NULL;

    *rmsg = NULL;
    len = get_serialized_msg_len(&msg->hdr);
    DEBUG_MSG("msg_id(%d)\n", id);

    MutexLocker lock(SockMutex);

    do 
    {
        if (ClientSock.get()->sendData((char *)msg, len, &ServerSockAddr) == -1)
        {
    	    ERROR_MSG("sendData failed");
            break;
        }

        if (ClientSock.get()->waitData(len, &tm))
        {
    	    ERROR_MSG("waitData failed");
            break;
        }

        data = (char *)malloc(len);
        if (data == NULL)
    	    break;

        if (id == MSG_ID_C_Initialize_Rsp)
        {
            sock_addr = &ServerSockAddr;
        }

        if (ClientSock.get()->recvData(data, len, sock_addr) == -1)
        {
    	    ERROR_MSG("recvData failed");
    	    break;
        }

        pmsg = (msg_t *)data;

        msg_hdr_deserialize(&pmsg->hdr);
    
        if (id != get_deserialized_msg_id(&pmsg->hdr))
        {
    	    ERROR_MSG("unexpected msg(%d)", get_deserialized_msg_id(&pmsg->hdr));
    	    break;
        }

        *rmsg = pmsg;
    } while(0);

    if (*rmsg)
        return 0;

    if (data)
    	free(data);
    return -1;
}

/*****************************************************************************
 Implementation of PKCS #11 functions
 *****************************************************************************/

// PKCS #11 initialisation function
CK_RV SoftHSM::C_Initialize(CK_VOID_PTR pInitArgs)
{
    CK_C_INITIALIZE_ARGS_PTR args;
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    // Check if PKCS #11 is already initialised
    if (isInitialised)
    {
    	return CKR_CRYPTOKI_ALREADY_INITIALIZED;
    }

    // Do we have any arguments?
    if (pInitArgs != NULL_PTR)
    {
    	args = (CK_C_INITIALIZE_ARGS_PTR)pInitArgs;

    	// Must be set to NULL_PTR in this version of PKCS#11
    	if (args->pReserved != NULL_PTR)
    	{
    		DEBUG_MSG("pReserved must be set to NULL_PTR");
    		return CKR_ARGUMENTS_BAD;
    	}

    	// Are we not supplied with mutex functions?
    	if
    	(
    		args->CreateMutex == NULL_PTR &&
    		args->DestroyMutex == NULL_PTR &&
    		args->LockMutex == NULL_PTR &&
    		args->UnlockMutex == NULL_PTR
    	)
    	{
    		// Can we use our own mutex functions?
    		if (args->flags & CKF_OS_LOCKING_OK)
    		{
    			// Use our own mutex functions.
    			MutexFactory::i()->setCreateMutex(OSCreateMutex);
    			MutexFactory::i()->setDestroyMutex(OSDestroyMutex);
    			MutexFactory::i()->setLockMutex(OSLockMutex);
    			MutexFactory::i()->setUnlockMutex(OSUnlockMutex);
    			MutexFactory::i()->enable();
    		}
    		else
    		{
    			// The external application is not using threading
    			MutexFactory::i()->disable();
    		}
    	}
    	else
    	{
    		// We must have all mutex functions
    		if
    		(
    			args->CreateMutex == NULL_PTR ||
    			args->DestroyMutex == NULL_PTR ||
    			args->LockMutex == NULL_PTR ||
    			args->UnlockMutex == NULL_PTR
    		)
    		{
    			DEBUG_MSG("Not all mutex functions are supplied");
    			return CKR_ARGUMENTS_BAD;
    		}

    		// We could use our own mutex functions if the flag is set,
    		// but we use the external functions in both cases.

    		// Load the external mutex functions
    		MutexFactory::i()->setCreateMutex(args->CreateMutex);
    		MutexFactory::i()->setDestroyMutex(args->DestroyMutex);
    		MutexFactory::i()->setLockMutex(args->LockMutex);
    		MutexFactory::i()->setUnlockMutex(args->UnlockMutex);
    		MutexFactory::i()->enable();
    	}
    }
    else
    {
    	// No concurrent access by multiple threads
    	MutexFactory::i()->disable();
    }

    if (InitializeSocket())
    {
    	ERROR_MSG("Failed to initialize sockets");
    	return CKR_GENERAL_ERROR;
    }

    do {
        if (format_C_Initialize_Req_msg(0, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Initialize_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Initialize_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }

        // Set the state to initialised
        isInitialised = true;
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }

    return rv;
}

// PKCS #11 finalisation function
CK_RV SoftHSM::C_Finalize(CK_VOID_PTR pReserved)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    // Must be set to NULL_PTR in this version of PKCS#11
    if (pReserved != NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_Finalize_Req_msg(0, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Finalize_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Finalize_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }

        isInitialised = false;
        DeInitializeSocket();
        SoftHSM::reset();
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Return information about the PKCS #11 module
CK_RV SoftHSM::C_GetInfo(CK_INFO_PTR pInfo)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pInfo == NULL_PTR) return CKR_ARGUMENTS_BAD;

    pInfo->cryptokiVersion.major = CRYPTOKI_VERSION_MAJOR;
    pInfo->cryptokiVersion.minor = CRYPTOKI_VERSION_MINOR;
    memset(pInfo->manufacturerID, ' ', 32);
    memcpy(pInfo->manufacturerID, "SoftHSM", 7);
    pInfo->flags = 0;
    memset(pInfo->libraryDescription, ' ', 32);
    memcpy(pInfo->libraryDescription, "Implementation of PKCS11", 24);
    pInfo->libraryVersion.major = VERSION_MAJOR;
    pInfo->libraryVersion.minor = VERSION_MINOR;

    return CKR_OK;
}

// Return a list of available slots
CK_RV SoftHSM::C_GetSlotList(CK_BBOOL tokenPresent, CK_SLOT_ID_PTR pSlotList, CK_ULONG_PTR pulCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    if (pSlotList == NULL) {
        *pulCount = 0;
    }

    do {
        if (format_C_GetSlotList_Req_msg(tokenPresent, (uint32_t)*pulCount, &msg))
        {
    	    ERROR_MSG("Format Req failed");
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetSlotList_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetSlotList_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulCount, (uint32_t*)pSlotList, rmsg))
        {
    	    ERROR_MSG("Parse Rsp failed");
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Return information about a slot
CK_RV SoftHSM::C_GetSlotInfo(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_GetSlotInfo_Req_msg(slotID, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetSlotInfo_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetSlotInfo_Rsp_msg((uint32_t*)&rv, pInfo, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Return information about a token in a slot
CK_RV SoftHSM::C_GetTokenInfo(CK_SLOT_ID slotID, CK_TOKEN_INFO_PTR pInfo)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_GetTokenInfo_Req_msg(slotID, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetTokenInfo_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetTokenInfo_Rsp_msg((uint32_t*)&rv, pInfo, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Return the list of supported mechanisms for a given slot
CK_RV SoftHSM::C_GetMechanismList(CK_SLOT_ID slotID, CK_MECHANISM_TYPE_PTR pMechanismList, CK_ULONG_PTR pulCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pulCount == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pMechanismList == NULL_PTR) *pulCount = 0;

    do {
        if (format_C_GetMechanismList_Req_msg(slotID, *pulCount, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetMechanismList_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetMechanismList_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulCount, (uint32_t*)pMechanismList, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Return more information about a mechanism for a given slot
CK_RV SoftHSM::C_GetMechanismInfo(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pInfo == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_GetMechanismInfo_Req_msg(slotID, (uint32_t)type, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetMechanismInfo_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetMechanismInfo_Rsp_msg((uint32_t*)&rv, pInfo, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise the token in the specified slot
CK_RV SoftHSM::C_InitToken(CK_SLOT_ID slotID, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen, CK_UTF8CHAR_PTR pLabel)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    // Check the PIN
    if (pPin == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (ulPinLen < MIN_PIN_LEN || ulPinLen > MAX_PIN_LEN) return CKR_PIN_INCORRECT;

    do {
        if (format_C_InitToken_Req_msg(slotID, ulPinLen, pPin, pLabel, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_InitToken_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_InitToken_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise the user PIN
CK_RV SoftHSM::C_InitPIN(CK_SESSION_HANDLE hSession, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    // Check the PIN
    if (pPin == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (ulPinLen < MIN_PIN_LEN || ulPinLen > MAX_PIN_LEN) return CKR_PIN_LEN_RANGE;

    do {
        if (format_C_InitPIN_Req_msg(hSession, ulPinLen, pPin, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_InitPIN_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_InitPIN_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Change the PIN
CK_RV SoftHSM::C_SetPIN(CK_SESSION_HANDLE hSession, CK_UTF8CHAR_PTR pOldPin, CK_ULONG ulOldLen, CK_UTF8CHAR_PTR pNewPin, CK_ULONG ulNewLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    // Check the new PINs
    if (pOldPin == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pNewPin == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (ulNewLen < MIN_PIN_LEN || ulNewLen > MAX_PIN_LEN) return CKR_PIN_LEN_RANGE;

    do {
        if (format_C_SetPIN_Req_msg(hSession, ulOldLen, pOldPin, ulNewLen, pNewPin, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_SetPIN_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_SetPIN_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Open a new session to the specified slot
CK_RV SoftHSM::C_OpenSession(CK_SLOT_ID slotID, CK_FLAGS flags, CK_VOID_PTR pApplication, CK_NOTIFY notify, CK_SESSION_HANDLE_PTR phSession)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_OpenSession_Req_msg(slotID, flags, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_OpenSession_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_OpenSession_Rsp_msg((uint32_t*)&rv, (uint32_t*)phSession, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Close the given session
CK_RV SoftHSM::C_CloseSession(CK_SESSION_HANDLE hSession)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_CloseSession_Req_msg(hSession, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_CloseSession_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_CloseSession_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Close all open sessions
CK_RV SoftHSM::C_CloseAllSessions(CK_SLOT_ID slotID)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_CloseAllSessions_Req_msg(slotID, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_CloseAllSessions_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_CloseAllSessions_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Retrieve information about the specified session
CK_RV SoftHSM::C_GetSessionInfo(CK_SESSION_HANDLE hSession, CK_SESSION_INFO_PTR pInfo)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_GetSessionInfo_Req_msg(hSession, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetSessionInfo_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetSessionInfo_Rsp_msg((uint32_t*)&rv, pInfo, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Determine the state of a running operation in a session
CK_RV SoftHSM::C_GetOperationState(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pOperationState, CK_ULONG_PTR pulOperationStateLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Set the operation sate in a session
CK_RV SoftHSM::C_SetOperationState(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pOperationState, CK_ULONG ulOperationStateLen, CK_OBJECT_HANDLE hEncryptionKey, CK_OBJECT_HANDLE hAuthenticationKey)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Login on the token in the specified session
CK_RV SoftHSM::C_Login(CK_SESSION_HANDLE hSession, CK_USER_TYPE userType, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    // Get the PIN
    if (pPin == NULL_PTR) return CKR_ARGUMENTS_BAD;

    switch (userType)
    {
    	case CKU_SO:
    	case CKU_USER:
            break;

    	case CKU_CONTEXT_SPECIFIC:
    		// TODO: When do we want to use this user type?
    		return CKR_OPERATION_NOT_INITIALIZED;
    		break;
    	default:
    		return CKR_USER_TYPE_INVALID;
    }

    do {
        if (format_C_Login_Req_msg(hSession, userType, ulPinLen, pPin, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Login_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Login_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Log out of the token in the specified session
CK_RV SoftHSM::C_Logout(CK_SESSION_HANDLE hSession)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_Logout_Req_msg(hSession, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Logout_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Logout_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Create a new object on the token in the specified session using the given attribute template
CK_RV SoftHSM::C_CreateObject(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, CK_OBJECT_HANDLE_PTR phObject)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_CreateObject_Req_msg(hSession, ulCount, pTemplate, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_CreateObject_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_CreateObject_Rsp_msg((uint32_t*)&rv, (uint32_t*)phObject, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Create a copy of the object with the specified handle
CK_RV SoftHSM::C_CopyObject(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, CK_OBJECT_HANDLE_PTR phNewObject)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Destroy the specified object
CK_RV SoftHSM::C_DestroyObject(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_DestroyObject_Req_msg(hSession, hObject, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_DestroyObject_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_DestroyObject_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Determine the size of the specified object
CK_RV SoftHSM::C_GetObjectSize(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ULONG_PTR pulSize)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Retrieve the specified attributes for the given object
CK_RV SoftHSM::C_GetAttributeValue(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pTemplate == NULL) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_GetAttributeValue_Req_msg(hSession, hObject, ulCount, pTemplate, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_GetAttributeValue_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_GetAttributeValue_Rsp_msg((uint32_t*)&rv, (uint32_t*)&ulCount, pTemplate, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }

    return rv;
}

// Change or set the value of the specified attributes on the specified object
CK_RV SoftHSM::C_SetAttributeValue(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pTemplate == NULL) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_SetAttributeValue_Req_msg(hSession, hObject, ulCount, pTemplate, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_SetAttributeValue_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_SetAttributeValue_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise object search in the specified session using the specified attribute template as search parameters
CK_RV SoftHSM::C_FindObjectsInit(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_FindObjectsInit_Req_msg(hSession, ulCount, pTemplate, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_FindObjectsInit_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_FindObjectsInit_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Continue the search for objects in the specified session
CK_RV SoftHSM::C_FindObjects(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE_PTR phObject, CK_ULONG ulMaxObjectCount, CK_ULONG_PTR pulObjectCount)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (phObject == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pulObjectCount == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_FindObjects_Req_msg(hSession, ulMaxObjectCount, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_FindObjects_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_FindObjects_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulObjectCount, (uint32_t*)phObject, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Finish searching for objects
CK_RV SoftHSM::C_FindObjectsFinal(CK_SESSION_HANDLE hSession)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;

    do {
        if (format_C_FindObjectsFinal_Req_msg(hSession, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_FindObjectsFinal_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_FindObjectsFinal_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise encryption using the specified object and mechanism
CK_RV SoftHSM::C_EncryptInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pMechanism == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_EncryptInit_Req_msg(hSession, pMechanism, hKey, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_EncryptInit_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_EncryptInit_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Perform a single operation encryption operation in the specified session
CK_RV SoftHSM::C_Encrypt(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pEncryptedData, CK_ULONG_PTR pulEncryptedDataLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pData == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pulEncryptedDataLen == NULL_PTR) return CKR_ARGUMENTS_BAD;

    if (pEncryptedData == NULL_PTR) *pulEncryptedDataLen = 0;

    do {
        if (format_C_Encrypt_Req_msg(hSession, *pulEncryptedDataLen, ulDataLen, pData, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Encrypt_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Encrypt_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulEncryptedDataLen, pEncryptedData, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Feed data to the running encryption operation in a session
CK_RV SoftHSM::C_EncryptUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pEncryptedData, CK_ULONG_PTR pulEncryptedDataLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Finalise the encryption operation
CK_RV SoftHSM::C_EncryptFinal(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedData, CK_ULONG_PTR pulEncryptedDataLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Initialise decryption using the specified object
CK_RV SoftHSM::C_DecryptInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pMechanism == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_DecryptInit_Req_msg(hSession, pMechanism, hKey, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_DecryptInit_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_DecryptInit_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Perform a single operation decryption in the given session
CK_RV SoftHSM::C_Decrypt(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedData, CK_ULONG ulEncryptedDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulDataLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pEncryptedData == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pulDataLen == NULL_PTR) return CKR_ARGUMENTS_BAD;

    if (pData == NULL_PTR) *pulDataLen = 0;
    do {
        if (format_C_Decrypt_Req_msg(hSession, *pulDataLen, ulEncryptedDataLen, pEncryptedData, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Decrypt_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Decrypt_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulDataLen, pData, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Feed data to the running decryption operation in a session
CK_RV SoftHSM::C_DecryptUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedData, CK_ULONG ulEncryptedDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pDataLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Finalise the decryption operation
CK_RV SoftHSM::C_DecryptFinal(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG_PTR pDataLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Initialise digesting using the specified mechanism in the specified session
CK_RV SoftHSM::C_DigestInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Digest the specified data in a one-pass operation and return the resulting digest
CK_RV SoftHSM::C_Digest(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pDigest, CK_ULONG_PTR pulDigestLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running digest operation
CK_RV SoftHSM::C_DigestUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running digest operation by digesting a secret key with the specified handle
CK_RV SoftHSM::C_DigestKey(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Finalise the digest operation in the specified session and return the digest
CK_RV SoftHSM::C_DigestFinal(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pDigest, CK_ULONG_PTR pulDigestLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Initialise a signing operation using the specified key and mechanism
CK_RV SoftHSM::C_SignInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pMechanism == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_SignInit_Req_msg(hSession, pMechanism, hKey, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_SignInit_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_SignInit_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Sign the data in a single pass operation
CK_RV SoftHSM::C_Sign(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pData == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pulSignatureLen == NULL_PTR) return CKR_ARGUMENTS_BAD;

    if (pSignature == NULL_PTR) *pulSignatureLen = 0;

    do {
        if (format_C_Sign_Req_msg(hSession, *pulSignatureLen, ulDataLen, pData, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Sign_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Sign_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulSignatureLen, pSignature, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Update a running signing operation with additional data
CK_RV SoftHSM::C_SignUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pPart == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_SignUpdate_Req_msg(hSession, ulPartLen, pPart, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_SignUpdate_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_SignUpdate_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Finalise a running signing operation and return the signature
CK_RV SoftHSM::C_SignFinal(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pulSignatureLen == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_SignFinal_Req_msg(hSession, *pulSignatureLen, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_SignFinal_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_SignFinal_Rsp_msg((uint32_t*)&rv, (uint32_t*)pulSignatureLen, pSignature, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise a signing operation that allows recovery of the signed data
CK_RV SoftHSM::C_SignRecoverInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Perform a single part signing operation that allows recovery of the signed data
CK_RV SoftHSM::C_SignRecover(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Initialise a verification operation using the specified key and mechanism
CK_RV SoftHSM::C_VerifyInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pMechanism == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_VerifyInit_Req_msg(hSession, pMechanism, hKey, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_VerifyInit_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_VerifyInit_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Perform a single pass verification operation
CK_RV SoftHSM::C_Verify(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG ulSignatureLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pData == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pSignature == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_Verify_Req_msg(hSession, ulSignatureLen, pSignature, ulDataLen, pData, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_Verify_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_Verify_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Update a running verification operation with additional data
CK_RV SoftHSM::C_VerifyUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pPart == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_VerifyUpdate_Req_msg(hSession, ulPartLen, pPart, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_VerifyUpdate_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_VerifyUpdate_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Finalise the verification operation and check the signature
CK_RV SoftHSM::C_VerifyFinal(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pSignature, CK_ULONG ulSignatureLen)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pSignature == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_VerifyFinal_Req_msg(hSession, ulSignatureLen, pSignature, &msg))
        {
            break;
        }
    
        if (MsgSendRecv(msg, MSG_ID_C_VerifyFinal_Rsp, &rmsg))
        {
            break;
        }
    
        if (parse_C_VerifyFinal_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Initialise a verification operation the allows recovery of the signed data from the signature
CK_RV SoftHSM::C_VerifyRecoverInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Perform a single part verification operation and recover the signed data
CK_RV SoftHSM::C_VerifyRecover(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pSignature, CK_ULONG ulSignatureLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulDataLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running multi-part encryption and digesting operation
CK_RV SoftHSM::C_DigestEncryptUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen, CK_BYTE_PTR pEncryptedPart, CK_ULONG_PTR pulEncryptedPartLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running multi-part decryption and digesting operation
CK_RV SoftHSM::C_DecryptDigestUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen, CK_BYTE_PTR pDecryptedPart, CK_ULONG_PTR pulDecryptedPartLen)
{	if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running multi-part signing and encryption operation
CK_RV SoftHSM::C_SignEncryptUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen, CK_BYTE_PTR pEncryptedPart, CK_ULONG_PTR pulEncryptedPartLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Update a running multi-part decryption and verification operation
CK_RV SoftHSM::C_DecryptVerifyUpdate(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedPart, CK_ULONG ulEncryptedPartLen, CK_BYTE_PTR pPart, CK_ULONG_PTR pulPartLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Generate a secret key using the specified mechanism
CK_RV SoftHSM::C_GenerateKey(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, CK_OBJECT_HANDLE_PTR phKey)
{
    msg_t *msg = NULL, *rmsg = NULL;
    CK_RV rv = CKR_GENERAL_ERROR;

    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    if (pMechanism == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (pTemplate == NULL_PTR) return CKR_ARGUMENTS_BAD;
    if (phKey == NULL_PTR) return CKR_ARGUMENTS_BAD;

    do {
        if (format_C_GenerateKey_Req_msg(hSession, ulCount, pTemplate, &msg))
        {
            break;
        }

        if (MsgSendRecv(msg, MSG_ID_C_GenerateKey_Rsp, &rmsg))
        {
            break;
        }

        if (parse_C_GenerateKey_Rsp_msg((uint32_t*)&rv, rmsg))
        {
            break;
        }
    } while(0);

    if (msg) {
        free(msg);
    }
    if (rmsg) {
        free(rmsg);
    }
    return rv;
}

// Generate a key-pair using the specified mechanism
CK_RV SoftHSM::C_GenerateKeyPair
(
    CK_SESSION_HANDLE hSession,
    CK_MECHANISM_PTR pMechanism,
    CK_ATTRIBUTE_PTR pPublicKeyTemplate,
    CK_ULONG ulPublicKeyAttributeCount,
    CK_ATTRIBUTE_PTR pPrivateKeyTemplate,
    CK_ULONG ulPrivateKeyAttributeCount,
    CK_OBJECT_HANDLE_PTR phPublicKey,
    CK_OBJECT_HANDLE_PTR phPrivateKey
)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Wrap the specified key using the specified wrapping key and mechanism
CK_RV SoftHSM::C_WrapKey
(
    CK_SESSION_HANDLE hSession,
    CK_MECHANISM_PTR pMechanism,
    CK_OBJECT_HANDLE hWrappingKey,
    CK_OBJECT_HANDLE hKey,
    CK_BYTE_PTR pWrappedKey,
    CK_ULONG_PTR pulWrappedKeyLen
)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Unwrap the specified key using the specified unwrapping key
CK_RV SoftHSM::C_UnwrapKey
(
    CK_SESSION_HANDLE hSession,
    CK_MECHANISM_PTR pMechanism,
    CK_OBJECT_HANDLE hUnwrappingKey,
    CK_BYTE_PTR pWrappedKey,
    CK_ULONG ulWrappedKeyLen,
    CK_ATTRIBUTE_PTR pTemplate,
    CK_ULONG ulCount,
    CK_OBJECT_HANDLE_PTR hKey
)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Derive a key from the specified base key
CK_RV SoftHSM::C_DeriveKey
(
    CK_SESSION_HANDLE hSession,
    CK_MECHANISM_PTR pMechanism,
    CK_OBJECT_HANDLE hBaseKey,
    CK_ATTRIBUTE_PTR pTemplate,
    CK_ULONG ulCount,
    CK_OBJECT_HANDLE_PTR phKey
)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Seed the random number generator with new data
CK_RV SoftHSM::C_SeedRandom(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pSeed, CK_ULONG ulSeedLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Generate the specified amount of random data
CK_RV SoftHSM::C_GenerateRandom(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pRandomData, CK_ULONG ulRandomLen)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_SUPPORTED;
}

// Legacy function
CK_RV SoftHSM::C_GetFunctionStatus(CK_SESSION_HANDLE hSession)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_PARALLEL;
}

// Legacy function
CK_RV SoftHSM::C_CancelFunction(CK_SESSION_HANDLE hSession)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    return CKR_FUNCTION_NOT_PARALLEL;
}

// Wait or poll for a slot even on the specified slot
CK_RV SoftHSM::C_WaitForSlotEvent(CK_FLAGS flags, CK_SLOT_ID_PTR pSlot, CK_VOID_PTR pReserved)
{
    if (!isInitialised) return CKR_CRYPTOKI_NOT_INITIALIZED;
    static CK_SLOT_ID event = 1;
    if (event) {
        *pSlot = 0;
        event = 0;
    } else return CKR_NO_EVENT;
    return CKR_OK;
}

