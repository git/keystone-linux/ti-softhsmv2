/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <string.h>

#include "parse_msg_types.h"

int parse_ck_attribute_msg
(
    struct ck_attribute_msg *msg,
    CK_ATTRIBUTE  *templ
)
{
    templ->type = msg->type;
    templ->ulValueLen = msg->value_len;
    if ((templ->pValue) && (msg->value_len) && (msg->value_len != (uint32_t)-1)) {
        memcpy(templ->pValue, msg->value, msg->value_len);
    }
    return 0;
}

int parse_get_ck_attribute_msg_malloc
(
    struct ck_attribute_msg *msg,
    CK_ATTRIBUTE  *templ
)
{
    memset(templ, 0, sizeof(*templ));
    templ->type = msg->type;
    templ->ulValueLen = msg->value_len;
    if ((msg->value_len) && (msg->value_len != (uint32_t)-1)) {
        templ->pValue = malloc(msg->value_len);
        if (templ->pValue == NULL) {
            return -1;
        }
    }
    return 0;
}

int parse_ck_attribute_msg_malloc
(
    struct ck_attribute_msg *msg,
    CK_ATTRIBUTE  *templ
)
{
    memset(templ, 0, sizeof(*templ));
    templ->type = msg->type;
    templ->ulValueLen = msg->value_len;
    if ((msg->value_len) && (msg->value_len != (uint32_t)-1)) {
        templ->pValue = malloc(msg->value_len);
        if (templ->pValue == NULL) {
            return -1;
        }
        memcpy(templ->pValue, msg->value, msg->value_len);
    }
    return 0;
}

int parse_ck_mechanism_msg
(
    struct ck_mechanism_msg *msg,
    CK_MECHANISM     *mech
)
{
    mech->mechanism = msg->mechanism;
    mech->ulParameterLen = msg->parameter_len;
    if ((msg->parameter_len) && (msg->parameter_len != (uint32_t)-1)) { 
        mech->pParameter = malloc(msg->parameter_len);
        if (mech->pParameter == NULL) {
            return -1;
        }
        memcpy(mech->pParameter, msg->parameter, msg->parameter_len);
    }
    return 0;
}

void parse_ck_version_msg
(
    struct ck_version_msg   *msg,
    CK_VERSION       *ver
)
{
    ver->major = msg->major;
    ver->minor = msg->minor;
    return;
}

void parse_ck_info_msg
(
    struct ck_info_msg  *msg,
    CK_INFO      *info
)
{
    parse_ck_version_msg(&msg->cryptoki_version, &info->cryptokiVersion);
    memcpy(info->manufacturerID, msg->manufacturer_id, 32);
    info->flags = msg->flags;
    memcpy(info->libraryDescription, msg->library_description, 32);
    parse_ck_version_msg(&msg->library_version, &info->libraryVersion);
    return;
}

void parse_ck_slot_info_msg
(
    struct ck_slot_info_msg  *msg,
    CK_SLOT_INFO      *info
)
{
    memcpy(info->slotDescription, msg->slot_description, 64);
    memcpy(info->manufacturerID, msg->manufacturer_id, 32);
    info->flags = msg->flags;
    parse_ck_version_msg(&msg->hardware_version, &info->hardwareVersion);
    parse_ck_version_msg(&msg->firmware_version, &info->firmwareVersion);
    return;
}

void parse_ck_token_info_msg
(
    struct ck_token_info_msg  *msg,
    CK_TOKEN_INFO      *info
)
{
    memcpy(info->label, msg->label, 32);
    memcpy(info->manufacturerID, msg->manufacturer_id, 32);
    memcpy(info->model, msg->model, 16);
    memcpy(info->serialNumber, msg->serial_number, 16);
    info->flags = msg->flags;
    info->ulMaxSessionCount = msg->max_session_count;
    info->ulSessionCount = msg->session_count;
    info->ulMaxRwSessionCount = msg->max_rw_session_count;
    info->ulRwSessionCount = msg->rw_session_count;
    info->ulMaxPinLen = msg->max_pin_len;
    info->ulMinPinLen = msg->min_pin_len;
    info->ulTotalPublicMemory = msg->total_public_memory;
    info->ulFreePublicMemory = msg->free_public_memory;
    info->ulTotalPrivateMemory = msg->total_private_memory;
    info->ulFreePrivateMemory = msg->free_private_memory;
    parse_ck_version_msg(&msg->hardware_version, &info->hardwareVersion);
    parse_ck_version_msg(&msg->firmware_version, &info->firmwareVersion);
    return;
}

void parse_ck_mechanism_info_msg
(
    struct ck_mechanism_info_msg *msg,
    CK_MECHANISM_INFO     *mech
)
{
    mech->ulMinKeySize = msg->min_key_size;
    mech->ulMaxKeySize = msg->max_key_size;
    mech->flags = msg->flags;
    return;
}

void parse_ck_session_info_msg
(
    struct ck_session_info_msg *msg,
    CK_SESSION_INFO     *info
)
{
    info->slotID = msg->slot_id;
    info->state = msg->state;
    info->flags = msg->flags;
    info->ulDeviceError = msg->device_error;
    return;
}

void free_ck_attribute(CK_ATTRIBUTE *attr)
{
    free(attr->pValue);
    return;
}


