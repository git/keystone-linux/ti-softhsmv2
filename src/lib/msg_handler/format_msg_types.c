/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <string.h>

#include "format_msg_types.h"

void format_ck_attribute_msg
(
    struct ck_attribute_msg *msg,
    CK_ATTRIBUTE  *templ
)
{
    msg->type = templ->type;
    msg->value_len = templ->ulValueLen;
    if ((templ->pValue) && (msg->value_len) && (msg->value_len != (uint32_t)-1))
        memcpy(msg->value, templ->pValue, templ->ulValueLen);
    return;
}

void format_get_ck_attribute_msg
(
    struct ck_attribute_msg *msg,
    CK_ATTRIBUTE  *templ
)
{
    msg->type = templ->type;
    msg->value_len = templ->ulValueLen;
    return;
}

void format_ck_mechanism_msg
(
    struct ck_mechanism_msg *msg,
    CK_MECHANISM     *mech
)
{
    msg->mechanism = mech->mechanism;
    msg->parameter_len = mech->ulParameterLen;
    if (mech->ulParameterLen)
    	memcpy(msg->parameter, mech->pParameter, mech->ulParameterLen);
    return;
}

void format_ck_version_msg
(
    struct ck_version_msg   *msg,
    CK_VERSION       *ver
)
{
    msg->major = ver->major;
    msg->minor = ver->minor;
    return;
}

void format_ck_info_msg
(
    struct ck_info_msg  *msg,
    CK_INFO      *info
)
{
    format_ck_version_msg(&msg->cryptoki_version, &info->cryptokiVersion);
    memcpy(msg->manufacturer_id, info->manufacturerID, 32);
    msg->flags = info->flags;
    memcpy(msg->library_description, info->libraryDescription, 32);
    format_ck_version_msg(&msg->library_version, &info->libraryVersion);
    return;
}

void format_ck_slot_info_msg
(
    struct ck_slot_info_msg  *msg,
    CK_SLOT_INFO      *info
)
{
    memcpy(msg->slot_description, info->slotDescription, 64);
    memcpy(msg->manufacturer_id, info->manufacturerID, 32);
    msg->flags = info->flags;
    format_ck_version_msg(&msg->hardware_version, &info->hardwareVersion);
    format_ck_version_msg(&msg->firmware_version, &info->firmwareVersion);
    return;
}

void format_ck_token_info_msg
(
    struct ck_token_info_msg  *msg,
    CK_TOKEN_INFO      *info
)
{
    memcpy(msg->label, info->label, 32);
    memcpy(msg->manufacturer_id, info->manufacturerID, 32);
    memcpy(msg->model, info->model, 16);
    memcpy(msg->serial_number, info->serialNumber, 16);
    msg->flags = info->flags;
    msg->max_session_count = info->ulMaxSessionCount;
    msg->session_count = info->ulSessionCount;
    msg->max_rw_session_count = info->ulMaxRwSessionCount;
    msg->rw_session_count = info->ulRwSessionCount;
    msg->max_pin_len = info->ulMaxPinLen;
    msg->min_pin_len = info->ulMinPinLen;
    msg->total_public_memory = info->ulTotalPublicMemory;
    msg->free_public_memory = info->ulFreePublicMemory;
    msg->total_private_memory = info->ulTotalPrivateMemory;
    msg->free_private_memory = info->ulFreePrivateMemory;
    format_ck_version_msg(&msg->hardware_version, &info->hardwareVersion);
    format_ck_version_msg(&msg->firmware_version, &info->firmwareVersion);
    return;
}

void format_ck_mechanism_info_msg
(
    struct ck_mechanism_info_msg *msg,
    CK_MECHANISM_INFO     *mech
)
{
    msg->min_key_size = mech->ulMinKeySize;
    msg->max_key_size = mech->ulMaxKeySize;
    msg->flags = mech->flags;
    return;
}

void format_ck_session_info_msg
(
    struct ck_session_info_msg *msg,
    CK_SESSION_INFO     *info
)
{
    msg->slot_id = info->slotID;
    msg->state = info->state;
    msg->flags = info->flags;
    msg->device_error = info->ulDeviceError;
    return;
}

