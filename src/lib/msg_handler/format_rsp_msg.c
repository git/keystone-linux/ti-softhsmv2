/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "format_rsp_msg.h"
#include "format_msg_types.h"
#include "msg_serialize.h"
#include <stdlib.h>
#include <string.h>

int format_C_Initialize_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Initialize_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Initialize_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Initialize_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Initialize_Rsp_msg_serialize(&(pmsg->body.C_Initialize_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Finalize_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Finalize_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Finalize_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Finalize_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Finalize_Rsp_msg_serialize(&(pmsg->body.C_Finalize_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetInfo_Rsp_msg
(
	uint32_t	retval,
	CK_INFO	*info,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetInfo_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetInfo_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetInfo_Rsp.retval = retval;
	format_ck_info_msg(&(pmsg->body.C_GetInfo_Rsp.info), info);
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetInfo_Rsp_msg_serialize(&(pmsg->body.C_GetInfo_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetSlotList_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	uint32_t	*slot_list,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_GetSlotList_Rsp_msg);

	if (slot_list) {
		len = len + (sizeof(*slot_list) * (count - 1));
	}

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSlotList_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSlotList_Rsp.retval = retval;
	pmsg->body.C_GetSlotList_Rsp.count = count;
	if (slot_list) {
		for (i=0; i<count; i++)
		{
			pmsg->body.C_GetSlotList_Rsp.slot_list[i] = slot_list[i];
		}
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSlotList_Rsp_msg_serialize(&(pmsg->body.C_GetSlotList_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetSlotInfo_Rsp_msg
(
	uint32_t	retval,
	CK_SLOT_INFO	*info,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetSlotInfo_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSlotInfo_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSlotInfo_Rsp.retval = retval;
	format_ck_slot_info_msg(&(pmsg->body.C_GetSlotInfo_Rsp.info), info);
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSlotInfo_Rsp_msg_serialize(&(pmsg->body.C_GetSlotInfo_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetTokenInfo_Rsp_msg
(
	uint32_t	retval,
	CK_TOKEN_INFO	*info,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetTokenInfo_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetTokenInfo_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetTokenInfo_Rsp.retval = retval;
	format_ck_token_info_msg(&(pmsg->body.C_GetTokenInfo_Rsp.info), info);
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetTokenInfo_Rsp_msg_serialize(&(pmsg->body.C_GetTokenInfo_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetMechanismList_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	uint32_t	*mechanism_list,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_GetMechanismList_Rsp_msg);
	if (mechanism_list) {
		len = len + (sizeof(*mechanism_list) * (count - 1));
	}

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetMechanismList_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetMechanismList_Rsp.retval = retval;
	pmsg->body.C_GetMechanismList_Rsp.count = count;
	
	if (mechanism_list) {
	for (i=0; i<count; i++)
		{
			pmsg->body.C_GetMechanismList_Rsp.mechanism_list[i] = mechanism_list[i];
		}
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetMechanismList_Rsp_msg_serialize(&(pmsg->body.C_GetMechanismList_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetMechanismInfo_Rsp_msg
(
	uint32_t	retval,
	CK_MECHANISM_INFO	*info,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetMechanismInfo_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetMechanismInfo_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetMechanismInfo_Rsp.retval = retval;
	format_ck_mechanism_info_msg(&(pmsg->body.C_GetMechanismInfo_Rsp.info), info);
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetMechanismInfo_Rsp_msg_serialize(&(pmsg->body.C_GetMechanismInfo_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_InitToken_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_InitToken_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_InitToken_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_InitToken_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_InitToken_Rsp_msg_serialize(&(pmsg->body.C_InitToken_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_InitPIN_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_InitPIN_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_InitPIN_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_InitPIN_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_InitPIN_Rsp_msg_serialize(&(pmsg->body.C_InitPIN_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_SetPIN_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SetPIN_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SetPIN_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_SetPIN_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SetPIN_Rsp_msg_serialize(&(pmsg->body.C_SetPIN_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_OpenSession_Rsp_msg
(
	uint32_t	retval,
	uint32_t	session,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_OpenSession_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_OpenSession_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_OpenSession_Rsp.retval = retval;
	pmsg->body.C_OpenSession_Rsp.session = session;
	msg_hdr_serialize(&(pmsg->hdr));
	C_OpenSession_Rsp_msg_serialize(&(pmsg->body.C_OpenSession_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_CloseSession_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_CloseSession_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CloseSession_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_CloseSession_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_CloseSession_Rsp_msg_serialize(&(pmsg->body.C_CloseSession_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_CloseAllSessions_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_CloseAllSessions_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CloseAllSessions_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_CloseAllSessions_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_CloseAllSessions_Rsp_msg_serialize(&(pmsg->body.C_CloseAllSessions_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetSessionInfo_Rsp_msg
(
	uint32_t	retval,
	CK_SESSION_INFO	*info,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetSessionInfo_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSessionInfo_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSessionInfo_Rsp.retval = retval;
	format_ck_session_info_msg(&(pmsg->body.C_GetSessionInfo_Rsp.info), info);
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSessionInfo_Rsp_msg_serialize(&(pmsg->body.C_GetSessionInfo_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Login_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Login_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Login_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Login_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Login_Rsp_msg_serialize(&(pmsg->body.C_Login_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Logout_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Logout_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Logout_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Logout_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Logout_Rsp_msg_serialize(&(pmsg->body.C_Logout_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_CreateObject_Rsp_msg
(
	uint32_t	retval,
	uint32_t	object,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_CreateObject_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CreateObject_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_CreateObject_Rsp.retval = retval;
	pmsg->body.C_CreateObject_Rsp.object = object;
	msg_hdr_serialize(&(pmsg->hdr));
	C_CreateObject_Rsp_msg_serialize(&(pmsg->body.C_CreateObject_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_DestroyObject_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_DestroyObject_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DestroyObject_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_DestroyObject_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_DestroyObject_Rsp_msg_serialize(&(pmsg->body.C_DestroyObject_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GetAttributeValue_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint8_t *attr;
	uint32_t i;
	int len = sizeof(struct C_GetAttributeValue_Rsp_msg);
	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

    	for (i=0; i<count; i++) {
		if ((templ[i].ulValueLen) && (templ[i].ulValueLen != (uint32_t)-1)) {
        		len += ((templ[i].ulValueLen -1) & ~3);
		}
    	}

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetAttributeValue_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GetAttributeValue_Rsp.retval = retval;
	pmsg->body.C_GetAttributeValue_Rsp.count = count;

    	attr = (uint8_t *)&(pmsg->body.C_GetAttributeValue_Rsp.templ[0]);
	for (i=0; i<count; i++)
	{
		format_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if ((templ[i].ulValueLen) && (templ[i].ulValueLen != (uint32_t)-1)) {
			attr = (attr + (sizeof(struct ck_attribute_msg)) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetAttributeValue_Rsp_msg_serialize(&(pmsg->body.C_GetAttributeValue_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_SetAttributeValue_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SetAttributeValue_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SetAttributeValue_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_SetAttributeValue_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SetAttributeValue_Rsp_msg_serialize(&(pmsg->body.C_SetAttributeValue_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjectsInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_FindObjectsInit_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjectsInit_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjectsInit_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjectsInit_Rsp_msg_serialize(&(pmsg->body.C_FindObjectsInit_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjects_Rsp_msg
(
	uint32_t	retval,
	uint32_t	object_count,
	uint32_t	*object,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_FindObjects_Rsp_msg);

	if (object_count)
		len = len + (sizeof(*object) * (object_count - 1));

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjects_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjects_Rsp.retval = retval;
	pmsg->body.C_FindObjects_Rsp.object_count = object_count;
	for (i=0; i<object_count; i++)
	{
		pmsg->body.C_FindObjects_Rsp.object[i] = object[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjects_Rsp_msg_serialize(&(pmsg->body.C_FindObjects_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjectsFinal_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_FindObjectsFinal_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjectsFinal_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjectsFinal_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjectsFinal_Rsp_msg_serialize(&(pmsg->body.C_FindObjectsFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_EncryptInit_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptInit_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptInit_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptInit_Rsp_msg_serialize(&(pmsg->body.C_EncryptInit_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Encrypt_Rsp_msg
(
	uint32_t	retval,
	uint32_t	encrypted_data_len,
	uint8_t	*encrypted_data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Encrypt_Rsp_msg);
	len = len + (encrypted_data_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Encrypt_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Encrypt_Rsp.retval = retval;
	pmsg->body.C_Encrypt_Rsp.encrypted_data_len = encrypted_data_len;
	if (encrypted_data) {
		memcpy(pmsg->body.C_Encrypt_Rsp.encrypted_data, encrypted_data, encrypted_data_len);
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Encrypt_Rsp_msg_serialize(&(pmsg->body.C_Encrypt_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptUpdate_Rsp_msg
(
	uint32_t	retval,
	uint32_t	encrypted_part_len,
	uint8_t	*encrypted_part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_EncryptUpdate_Rsp_msg);
	len = len + (encrypted_part_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptUpdate_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptUpdate_Rsp.retval = retval;
	pmsg->body.C_EncryptUpdate_Rsp.encrypted_part_len = encrypted_part_len;
	for (i=0; i<encrypted_part_len; i++)
	{
		pmsg->body.C_EncryptUpdate_Rsp.encrypted_part[i] = encrypted_part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptUpdate_Rsp_msg_serialize(&(pmsg->body.C_EncryptUpdate_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	last_encrypted_part_len,
	uint8_t	*last_encrypted_part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_EncryptFinal_Rsp_msg);
	len = len + (last_encrypted_part_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptFinal_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptFinal_Rsp.retval = retval;
	pmsg->body.C_EncryptFinal_Rsp.last_encrypted_part_len = last_encrypted_part_len;
	for (i=0; i<last_encrypted_part_len; i++)
	{
		pmsg->body.C_EncryptFinal_Rsp.last_encrypted_part[i] = last_encrypted_part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptFinal_Rsp_msg_serialize(&(pmsg->body.C_EncryptFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_DecryptInit_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptInit_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptInit_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptInit_Rsp_msg_serialize(&(pmsg->body.C_DecryptInit_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Decrypt_Rsp_msg
(
	uint32_t	retval,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Decrypt_Rsp_msg);
	len = len + (data_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Decrypt_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Decrypt_Rsp.retval = retval;
	pmsg->body.C_Decrypt_Rsp.data_len = data_len;
	if (data) {
		memcpy(pmsg->body.C_Decrypt_Rsp.data, data, data_len);
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Decrypt_Rsp_msg_serialize(&(pmsg->body.C_Decrypt_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptUpdate_Rsp_msg
(
	uint32_t	retval,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_DecryptUpdate_Rsp_msg);
	len = len + (part_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptUpdate_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptUpdate_Rsp.retval = retval;
	pmsg->body.C_DecryptUpdate_Rsp.part_len = part_len;
	for (i=0; i<part_len; i++)
	{
		pmsg->body.C_DecryptUpdate_Rsp.part[i] = part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptUpdate_Rsp_msg_serialize(&(pmsg->body.C_DecryptUpdate_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	last_part_len,
	uint8_t	*last_part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_DecryptFinal_Rsp_msg);
	len = len + (last_part_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptFinal_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptFinal_Rsp.retval = retval;
	pmsg->body.C_DecryptFinal_Rsp.last_part_len = last_part_len;
	for (i=0; i<last_part_len; i++)
	{
		pmsg->body.C_DecryptFinal_Rsp.last_part[i] = last_part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptFinal_Rsp_msg_serialize(&(pmsg->body.C_DecryptFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_SignInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SignInit_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignInit_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_SignInit_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignInit_Rsp_msg_serialize(&(pmsg->body.C_SignInit_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Sign_Rsp_msg
(
	uint32_t	retval,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Sign_Rsp_msg);
	len = len + (signature_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Sign_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Sign_Rsp.retval = retval;
	pmsg->body.C_Sign_Rsp.signature_len = signature_len;
	if (signature) {
		memcpy(pmsg->body.C_Sign_Rsp.signature, signature, signature_len);
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Sign_Rsp_msg_serialize(&(pmsg->body.C_Sign_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_SignUpdate_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SignUpdate_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignUpdate_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_SignUpdate_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignUpdate_Rsp_msg_serialize(&(pmsg->body.C_SignUpdate_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_SignFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_SignFinal_Rsp_msg);
	len = len + (signature_len - 1);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignFinal_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_SignFinal_Rsp.retval = retval;
	pmsg->body.C_SignFinal_Rsp.signature_len = signature_len;
	for (i=0; i<signature_len; i++)
	{
		pmsg->body.C_SignFinal_Rsp.signature[i] = signature[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignFinal_Rsp_msg_serialize(&(pmsg->body.C_SignFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_VerifyInit_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyInit_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyInit_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyInit_Rsp_msg_serialize(&(pmsg->body.C_VerifyInit_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Verify_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Verify_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Verify_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_Verify_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Verify_Rsp_msg_serialize(&(pmsg->body.C_Verify_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyUpdate_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_VerifyUpdate_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyUpdate_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyUpdate_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyUpdate_Rsp_msg_serialize(&(pmsg->body.C_VerifyUpdate_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyFinal_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_VerifyFinal_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyFinal_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyFinal_Rsp.retval = retval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyFinal_Rsp_msg_serialize(&(pmsg->body.C_VerifyFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_Error_Rsp_msg
(
	uint32_t	errorval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GenError_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GenError_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyFinal_Rsp.retval = errorval;
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyFinal_Rsp_msg_serialize(&(pmsg->body.C_VerifyFinal_Rsp));
	*msg = pmsg;
	return 0;
}

int format_C_GenerateKey_Rsp_msg
(
	uint32_t	errorval,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GenerateKey_Rsp_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL) {
		return -1;
	}
	pmsg->hdr.id = MSG_ID_C_GenerateKey_Rsp;
	pmsg->hdr.len = len;
	pmsg->body.C_GenerateKey_Rsp.retval = errorval;

	msg_hdr_serialize(&(pmsg->hdr));
	C_GenerateKey_Rsp_msg_serialize(&(pmsg->body.C_GenerateKey_Rsp));
	*msg = pmsg;
	return 0;
}
