/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __FORMAT_REQ_MSG_H__
#define __FORMAT_REQ_MSG_H__

#include <stdint.h>

#include "cryptoki.h"
#include "pkcs11_msg.h"

#if defined(__cplusplus)
extern "C" {
#endif

int format_C_Initialize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
);

int format_C_Finalize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
);

int format_C_GetInfo_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
);

int format_C_GetSlotList_Req_msg
(
	uint8_t	token_present,
	uint32_t	count,
	msg_t	**msg
);

int format_C_GetSlotInfo_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
);

int format_C_GetTokenInfo_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
);

int format_C_GetMechanismList_Req_msg
(
	uint32_t	slot_id,
	uint32_t	count,
	msg_t	**msg
);

int format_C_GetMechanismInfo_Req_msg
(
	uint32_t	slot_id,
	uint32_t	type,
	msg_t	**msg
);

int format_C_InitToken_Req_msg
(
	uint32_t	slot_id,
	uint32_t	pin_len,
	uint8_t	*pin,
	uint8_t	*label,
	msg_t	**msg
);

int format_C_InitPIN_Req_msg
(
	uint32_t	session,
	uint32_t	pin_len,
	uint8_t	*pin,
	msg_t	**msg
);

int format_C_SetPIN_Req_msg
(
	uint32_t	session,
	uint32_t	old_len,
	uint8_t	*old_pin,
	uint32_t	new_len,
	uint8_t	*new_pin,
	msg_t	**msg
);

int format_C_OpenSession_Req_msg
(
	uint32_t	slot_id,
	uint32_t	flags,
	msg_t	**msg
);

int format_C_CloseSession_Req_msg
(
	uint32_t	session,
	msg_t	**msg
);

int format_C_CloseAllSessions_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
);

int format_C_GetSessionInfo_Req_msg
(
	uint32_t	session,
	msg_t	**msg
);

int format_C_Login_Req_msg
(
	uint32_t	session,
	uint32_t	user_type,
	uint32_t	pin_len,
	uint8_t	*pin,
	msg_t	**msg
);

int format_C_Logout_Req_msg
(
	uint32_t	session,
	msg_t	**msg
);

int format_C_CreateObject_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);

int format_C_DestroyObject_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	msg_t	**msg
);

int format_C_GetAttributeValue_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);

int format_C_SetAttributeValue_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);

int format_C_FindObjectsInit_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);

int format_C_FindObjects_Req_msg
(
	uint32_t	session,
	uint32_t	max_object_count,
	msg_t	**msg
);

int format_C_FindObjectsFinal_Req_msg
(
	uint32_t	session,
	msg_t	**msg
);

int format_C_EncryptInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
);

int format_C_Encrypt_Req_msg
(
	uint32_t	session,
	uint32_t	encrypted_data_len,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
);

int format_C_EncryptUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	encrypted_part_len,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
);

int format_C_EncryptFinal_Req_msg
(
	uint32_t	session,
	uint32_t	last_encrypted_part_len,
	msg_t	**msg
);

int format_C_DecryptInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
);

int format_C_Decrypt_Req_msg
(
	uint32_t	session,
	uint32_t	data_len,
	uint32_t	encrypted_data_len,
	uint8_t	*encrypted_data,
	msg_t	**msg
);

int format_C_DecryptUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint32_t	encrypted_part_len,
	uint8_t	*encrypted_part,
	msg_t	**msg
);

int format_C_DecryptFinal_Req_msg
(
	uint32_t	session,
	uint32_t	last_part_len,
	msg_t	**msg
);

int format_C_SignInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
);

int format_C_Sign_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
);

int format_C_SignUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
);

int format_C_SignFinal_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	msg_t	**msg
);

int format_C_VerifyInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
);

int format_C_Verify_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint8_t	*signature,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
);

int format_C_VerifyUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
);

int format_C_VerifyFinal_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
);

int format_C_GenerateKey_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);
#if defined(__cplusplus)
}
#endif

#endif

