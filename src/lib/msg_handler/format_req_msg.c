/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdlib.h>
#include <string.h>

#include "msg_serialize.h"
#include "format_msg_types.h"
#include "format_req_msg.h"

int format_C_Initialize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Initialize_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Initialize_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Initialize_Req.reserved = reserved;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Initialize_Req_msg_serialize(&(pmsg->body.C_Initialize_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Finalize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Finalize_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Finalize_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Finalize_Req.reserved = reserved;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Finalize_Req_msg_serialize(&(pmsg->body.C_Finalize_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetInfo_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetInfo_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetInfo_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetInfo_Req.reserved = reserved;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetInfo_Req_msg_serialize(&(pmsg->body.C_GetInfo_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetSlotList_Req_msg
(
	uint8_t	token_present,
	uint32_t	count,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetSlotList_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSlotList_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSlotList_Req.token_present = token_present;
	pmsg->body.C_GetSlotList_Req.count = count;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSlotList_Req_msg_serialize(&(pmsg->body.C_GetSlotList_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetSlotInfo_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetSlotInfo_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSlotInfo_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSlotInfo_Req.slot_id = slot_id;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSlotInfo_Req_msg_serialize(&(pmsg->body.C_GetSlotInfo_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetTokenInfo_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetTokenInfo_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetTokenInfo_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetTokenInfo_Req.slot_id = slot_id;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetTokenInfo_Req_msg_serialize(&(pmsg->body.C_GetTokenInfo_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetMechanismList_Req_msg
(
	uint32_t	slot_id,
	uint32_t	count,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetMechanismList_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetMechanismList_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetMechanismList_Req.slot_id = slot_id;
	pmsg->body.C_GetMechanismList_Req.count = count;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetMechanismList_Req_msg_serialize(&(pmsg->body.C_GetMechanismList_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetMechanismInfo_Req_msg
(
	uint32_t	slot_id,
	uint32_t	type,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetMechanismInfo_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetMechanismInfo_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetMechanismInfo_Req.slot_id = slot_id;
	pmsg->body.C_GetMechanismInfo_Req.type = type;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetMechanismInfo_Req_msg_serialize(&(pmsg->body.C_GetMechanismInfo_Req));
	*msg = pmsg;
	return 0;
}

int format_C_InitToken_Req_msg
(
	uint32_t	slot_id,
	uint32_t	pin_len,
	uint8_t	*pin,
	uint8_t	*label,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_InitToken_Req_msg);
	len = len + (pin_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_InitToken_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_InitToken_Req.slot_id = slot_id;
	pmsg->body.C_InitToken_Req.pin_len = pin_len;
	for (i=0; i<pin_len; i++)
	{
		pmsg->body.C_InitToken_Req.pin[i] = pin[i];
	}
	memcpy(pmsg->body.C_InitToken_Req.label, label, 32);
	msg_hdr_serialize(&(pmsg->hdr));
	C_InitToken_Req_msg_serialize(&(pmsg->body.C_InitToken_Req));
	*msg = pmsg;
	return 0;
}

int format_C_InitPIN_Req_msg
(
	uint32_t	session,
	uint32_t	pin_len,
	uint8_t	*pin,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_InitPIN_Req_msg);
	len = len + (pin_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_InitPIN_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_InitPIN_Req.session = session;
	pmsg->body.C_InitPIN_Req.pin_len = pin_len;
	for (i=0; i<pin_len; i++)
	{
		pmsg->body.C_InitPIN_Req.pin[i] = pin[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_InitPIN_Req_msg_serialize(&(pmsg->body.C_InitPIN_Req));
	*msg = pmsg;
	return 0;
}

int format_C_SetPIN_Req_msg
(
	uint32_t	session,
	uint32_t	old_len,
	uint8_t	*old_pin,
	uint32_t	new_len,
	uint8_t	*new_pin,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_SetPIN_Req_msg);
	len = len + (old_len - 1) + (new_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SetPIN_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_SetPIN_Req.session = session;
	pmsg->body.C_SetPIN_Req.old_len = old_len;
	pmsg->body.C_SetPIN_Req.new_len = new_len;
	memcpy(pmsg->body.C_SetPIN_Req.old_pin, old_pin, old_len);
	memcpy((pmsg->body.C_SetPIN_Req.old_pin + old_len), new_pin, new_len);

	msg_hdr_serialize(&(pmsg->hdr));
	C_SetPIN_Req_msg_serialize(&(pmsg->body.C_SetPIN_Req));
	*msg = pmsg;
	return 0;
}

int format_C_OpenSession_Req_msg
(
	uint32_t	slot_id,
	uint32_t	flags,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_OpenSession_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_OpenSession_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_OpenSession_Req.slot_id = slot_id;
	pmsg->body.C_OpenSession_Req.flags = flags;
	msg_hdr_serialize(&(pmsg->hdr));
	C_OpenSession_Req_msg_serialize(&(pmsg->body.C_OpenSession_Req));
	*msg = pmsg;
	return 0;
}

int format_C_CloseSession_Req_msg
(
	uint32_t	session,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_CloseSession_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CloseSession_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_CloseSession_Req.session = session;
	msg_hdr_serialize(&(pmsg->hdr));
	C_CloseSession_Req_msg_serialize(&(pmsg->body.C_CloseSession_Req));
	*msg = pmsg;
	return 0;
}

int format_C_CloseAllSessions_Req_msg
(
	uint32_t	slot_id,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_CloseAllSessions_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CloseAllSessions_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_CloseAllSessions_Req.slot_id = slot_id;
	msg_hdr_serialize(&(pmsg->hdr));
	C_CloseAllSessions_Req_msg_serialize(&(pmsg->body.C_CloseAllSessions_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetSessionInfo_Req_msg
(
	uint32_t	session,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_GetSessionInfo_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetSessionInfo_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetSessionInfo_Req.session = session;
	msg_hdr_serialize(&(pmsg->hdr));
	C_GetSessionInfo_Req_msg_serialize(&(pmsg->body.C_GetSessionInfo_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Login_Req_msg
(
	uint32_t	session,
	uint32_t	user_type,
	uint32_t	pin_len,
	uint8_t	*pin,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_Login_Req_msg);
	len = len + (pin_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Login_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Login_Req.session = session;
	pmsg->body.C_Login_Req.user_type = user_type;
	pmsg->body.C_Login_Req.pin_len = pin_len;
	for (i=0; i<pin_len; i++)
	{
		pmsg->body.C_Login_Req.pin[i] = pin[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Login_Req_msg_serialize(&(pmsg->body.C_Login_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Logout_Req_msg
(
	uint32_t	session,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Logout_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Logout_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Logout_Req.session = session;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Logout_Req_msg_serialize(&(pmsg->body.C_Logout_Req));
	*msg = pmsg;
	return 0;
}

int format_C_CreateObject_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	uint8_t *attr;
	int len = sizeof(struct C_CreateObject_Req_msg);
	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

    	for (i=0; i<count; i++) {
        	len += ((templ[i].ulValueLen -1) & ~3);
    	}

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_CreateObject_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_CreateObject_Req.session = session;
	pmsg->body.C_CreateObject_Req.count = count;

    	attr = (uint8_t *)&(pmsg->body.C_CreateObject_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		format_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen) {
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}

	msg_hdr_serialize(&(pmsg->hdr));
	C_CreateObject_Req_msg_serialize(&(pmsg->body.C_CreateObject_Req));
	*msg = pmsg;
	return 0;
}

int format_C_DestroyObject_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_DestroyObject_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DestroyObject_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_DestroyObject_Req.session = session;
	pmsg->body.C_DestroyObject_Req.object = object;
	msg_hdr_serialize(&(pmsg->hdr));
	C_DestroyObject_Req_msg_serialize(&(pmsg->body.C_DestroyObject_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GetAttributeValue_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	struct ck_attribute_msg *attr;
	int len = sizeof(struct C_GetAttributeValue_Req_msg);
	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GetAttributeValue_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GetAttributeValue_Req.session = session;
	pmsg->body.C_GetAttributeValue_Req.object = object;
	pmsg->body.C_GetAttributeValue_Req.count = count;

    	attr = &(pmsg->body.C_GetAttributeValue_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		format_get_ck_attribute_msg(&attr[i], &templ[i]);
	}

	msg_hdr_serialize(&(pmsg->hdr));
	C_GetAttributeValue_Req_msg_serialize(&(pmsg->body.C_GetAttributeValue_Req));
	*msg = pmsg;
	return 0;
}

int format_C_SetAttributeValue_Req_msg
(
	uint32_t	session,
	uint32_t	object,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	uint8_t *attr;
	int len = sizeof(struct C_SetAttributeValue_Req_msg);
	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

	for (i=0; i<count; i++) {
		len += ((templ[i].ulValueLen -1) & ~3);
	}

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SetAttributeValue_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_SetAttributeValue_Req.session = session;
	pmsg->body.C_SetAttributeValue_Req.object = object;
	pmsg->body.C_SetAttributeValue_Req.count = count;

	attr = (uint8_t *)&(pmsg->body.C_SetAttributeValue_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		format_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen)
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		else
			attr = (attr + sizeof(struct ck_attribute_msg));
	}

	msg_hdr_serialize(&(pmsg->hdr));
	C_SetAttributeValue_Req_msg_serialize(&(pmsg->body.C_SetAttributeValue_Req));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjectsInit_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	uint8_t *attr;
	int len = sizeof(struct C_FindObjectsInit_Req_msg);
	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

	for (i=0; i<count; i++) {
        	len += ((templ[i].ulValueLen -1) & ~3);
	}

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjectsInit_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjectsInit_Req.session = session;
	pmsg->body.C_FindObjectsInit_Req.count = count;

	attr = (uint8_t *)&(pmsg->body.C_FindObjectsInit_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		format_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen)
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		else
			attr = (attr + sizeof(struct ck_attribute_msg));
	}

	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjectsInit_Req_msg_serialize(&(pmsg->body.C_FindObjectsInit_Req));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjects_Req_msg
(
	uint32_t	session,
	uint32_t	max_object_count,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_FindObjects_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjects_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjects_Req.session = session;
	pmsg->body.C_FindObjects_Req.max_object_count = max_object_count;
	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjects_Req_msg_serialize(&(pmsg->body.C_FindObjects_Req));
	*msg = pmsg;
	return 0;
}

int format_C_FindObjectsFinal_Req_msg
(
	uint32_t	session,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_FindObjectsFinal_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_FindObjectsFinal_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_FindObjectsFinal_Req.session = session;
	msg_hdr_serialize(&(pmsg->hdr));
	C_FindObjectsFinal_Req_msg_serialize(&(pmsg->body.C_FindObjectsFinal_Req));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_EncryptInit_Req_msg);
	if (mechanism->ulParameterLen)
		len += (mechanism->ulParameterLen -1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptInit_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptInit_Req.session = session;
	format_ck_mechanism_msg(&(pmsg->body.C_EncryptInit_Req.mechanism), mechanism);
	pmsg->body.C_EncryptInit_Req.key = key;
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptInit_Req_msg_serialize(&(pmsg->body.C_EncryptInit_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Encrypt_Req_msg
(
	uint32_t	session,
	uint32_t	encrypted_data_len,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_Encrypt_Req_msg);
	len = len + (data_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Encrypt_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Encrypt_Req.session = session;
	pmsg->body.C_Encrypt_Req.encrypted_data_len = encrypted_data_len;
	pmsg->body.C_Encrypt_Req.data_len = data_len;
	for (i=0; i<data_len; i++)
	{
		pmsg->body.C_Encrypt_Req.data[i] = data[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Encrypt_Req_msg_serialize(&(pmsg->body.C_Encrypt_Req));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	encrypted_part_len,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_EncryptUpdate_Req_msg);
	len = len + (part_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptUpdate_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptUpdate_Req.session = session;
	pmsg->body.C_EncryptUpdate_Req.encrypted_part_len = encrypted_part_len;
	pmsg->body.C_EncryptUpdate_Req.part_len = part_len;
	for (i=0; i<part_len; i++)
	{
		pmsg->body.C_EncryptUpdate_Req.part[i] = part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptUpdate_Req_msg_serialize(&(pmsg->body.C_EncryptUpdate_Req));
	*msg = pmsg;
	return 0;
}

int format_C_EncryptFinal_Req_msg
(
	uint32_t	session,
	uint32_t	last_encrypted_part_len,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_EncryptFinal_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_EncryptFinal_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_EncryptFinal_Req.session = session;
	pmsg->body.C_EncryptFinal_Req.last_encrypted_part_len = last_encrypted_part_len;
	msg_hdr_serialize(&(pmsg->hdr));
	C_EncryptFinal_Req_msg_serialize(&(pmsg->body.C_EncryptFinal_Req));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_DecryptInit_Req_msg);
	if (mechanism->ulParameterLen)
		len += (mechanism->ulParameterLen -1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptInit_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptInit_Req.session = session;
	format_ck_mechanism_msg(&(pmsg->body.C_DecryptInit_Req.mechanism), mechanism);
	pmsg->body.C_DecryptInit_Req.key = key;
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptInit_Req_msg_serialize(&(pmsg->body.C_DecryptInit_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Decrypt_Req_msg
(
	uint32_t	session,
	uint32_t	data_len,
	uint32_t	encrypted_data_len,
	uint8_t	*encrypted_data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_Decrypt_Req_msg);
	len = len + (encrypted_data_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Decrypt_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Decrypt_Req.session = session;
	pmsg->body.C_Decrypt_Req.data_len = data_len;
	pmsg->body.C_Decrypt_Req.encrypted_data_len = encrypted_data_len;
	for (i=0; i<encrypted_data_len; i++)
	{
		pmsg->body.C_Decrypt_Req.encrypted_data[i] = encrypted_data[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Decrypt_Req_msg_serialize(&(pmsg->body.C_Decrypt_Req));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint32_t	encrypted_part_len,
	uint8_t	*encrypted_part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_DecryptUpdate_Req_msg);
	len = len + (encrypted_part_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptUpdate_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptUpdate_Req.session = session;
	pmsg->body.C_DecryptUpdate_Req.part_len = part_len;
	pmsg->body.C_DecryptUpdate_Req.encrypted_part_len = encrypted_part_len;
	for (i=0; i<encrypted_part_len; i++)
	{
		pmsg->body.C_DecryptUpdate_Req.encrypted_part[i] = encrypted_part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptUpdate_Req_msg_serialize(&(pmsg->body.C_DecryptUpdate_Req));
	*msg = pmsg;
	return 0;
}

int format_C_DecryptFinal_Req_msg
(
	uint32_t	session,
	uint32_t	last_part_len,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_DecryptFinal_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_DecryptFinal_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_DecryptFinal_Req.session = session;
	pmsg->body.C_DecryptFinal_Req.last_part_len = last_part_len;
	msg_hdr_serialize(&(pmsg->hdr));
	C_DecryptFinal_Req_msg_serialize(&(pmsg->body.C_DecryptFinal_Req));
	*msg = pmsg;
	return 0;
}

int format_C_SignInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SignInit_Req_msg);
	if (mechanism->ulParameterLen)
		len += (mechanism->ulParameterLen -1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignInit_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_SignInit_Req.session = session;
	format_ck_mechanism_msg(&(pmsg->body.C_SignInit_Req.mechanism), mechanism);
	pmsg->body.C_SignInit_Req.key = key;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignInit_Req_msg_serialize(&(pmsg->body.C_SignInit_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Sign_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_Sign_Req_msg);
	len = len + (data_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Sign_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Sign_Req.session = session;
	pmsg->body.C_Sign_Req.signature_len = signature_len;
	pmsg->body.C_Sign_Req.data_len = data_len;
	for (i=0; i<data_len; i++)
	{
		pmsg->body.C_Sign_Req.data[i] = data[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_Sign_Req_msg_serialize(&(pmsg->body.C_Sign_Req));
	*msg = pmsg;
	return 0;
}

int format_C_SignUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_SignUpdate_Req_msg);
	len = len + (part_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignUpdate_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_SignUpdate_Req.session = session;
	pmsg->body.C_SignUpdate_Req.part_len = part_len;
	for (i=0; i<part_len; i++)
	{
		pmsg->body.C_SignUpdate_Req.part[i] = part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignUpdate_Req_msg_serialize(&(pmsg->body.C_SignUpdate_Req));
	*msg = pmsg;
	return 0;
}

int format_C_SignFinal_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_SignFinal_Req_msg);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_SignFinal_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_SignFinal_Req.session = session;
	pmsg->body.C_SignFinal_Req.signature_len = signature_len;
	msg_hdr_serialize(&(pmsg->hdr));
	C_SignFinal_Req_msg_serialize(&(pmsg->body.C_SignFinal_Req));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyInit_Req_msg
(
	uint32_t	session,
	CK_MECHANISM	*mechanism,
	uint32_t	key,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_VerifyInit_Req_msg);
	if (mechanism->ulParameterLen)
		len += (mechanism->ulParameterLen -1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyInit_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyInit_Req.session = session;
	format_ck_mechanism_msg(&(pmsg->body.C_VerifyInit_Req.mechanism), mechanism);
	pmsg->body.C_VerifyInit_Req.key = key;
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyInit_Req_msg_serialize(&(pmsg->body.C_VerifyInit_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Verify_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint8_t	*signature,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_Verify_Req_msg);
	len = len + (signature_len - 1) + (data_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Verify_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Verify_Req.session = session;

	pmsg->body.C_Verify_Req.signature_len = signature_len;
	memcpy(pmsg->body.C_Verify_Req.signature, signature, signature_len);

	pmsg->body.C_Verify_Req.data_len = data_len;
	memcpy((pmsg->body.C_Verify_Req.signature + signature_len), data, data_len);

	msg_hdr_serialize(&(pmsg->hdr));
	C_Verify_Req_msg_serialize(&(pmsg->body.C_Verify_Req));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyUpdate_Req_msg
(
	uint32_t	session,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_VerifyUpdate_Req_msg);
	len = len + (part_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyUpdate_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyUpdate_Req.session = session;
	pmsg->body.C_VerifyUpdate_Req.part_len = part_len;
	for (i=0; i<part_len; i++)
	{
		pmsg->body.C_VerifyUpdate_Req.part[i] = part[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyUpdate_Req_msg_serialize(&(pmsg->body.C_VerifyUpdate_Req));
	*msg = pmsg;
	return 0;
}

int format_C_VerifyFinal_Req_msg
(
	uint32_t	session,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	int len = sizeof(struct C_VerifyFinal_Req_msg);
	len = len + (signature_len - 1);

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_VerifyFinal_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_VerifyFinal_Req.session = session;
	pmsg->body.C_VerifyFinal_Req.signature_len = signature_len;
	for (i=0; i<signature_len; i++)
	{
		pmsg->body.C_VerifyFinal_Req.signature[i] = signature[i];
	}
	msg_hdr_serialize(&(pmsg->hdr));
	C_VerifyFinal_Req_msg_serialize(&(pmsg->body.C_VerifyFinal_Req));
	*msg = pmsg;
	return 0;
}

int format_C_GenerateKey_Req_msg
(
	uint32_t	session,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
)
{
	msg_t *pmsg;
	uint32_t i;
	uint8_t *attr;
	int len = sizeof(struct C_GenerateKey_Req_msg);

	len = len + (sizeof(struct ck_attribute_msg) * (count - 1));

	for (i=0; i<count; i++) {
		len += ((templ[i].ulValueLen -1) & ~3);
	}

	pmsg = malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_GenerateKey_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_GenerateKey_Req.session = session;
	pmsg->body.C_GenerateKey_Req.count = count;

	attr = (uint8_t *)&(pmsg->body.C_GenerateKey_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		format_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen) {
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}


	msg_hdr_serialize(&(pmsg->hdr));
	C_GenerateKey_Req_msg_serialize(&(pmsg->body.C_GenerateKey_Req));
	*msg = pmsg;
	return 0;
}

