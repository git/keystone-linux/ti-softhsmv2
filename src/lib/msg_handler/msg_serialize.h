/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __MSG_SERIALIZE_H__
#define __MSG_SERIALIZE_H__

#include "pkcs11_msg.h"

#if defined(__cplusplus)
extern "C" {
#endif

void msg_hdr_serialize(msg_hdr_t *hdr);
void ck_version_msg_serialize (struct ck_version_msg *msg);
void ck_info_msg_serialize (struct ck_info_msg *msg);
void ck_slot_info_msg_serialize (struct ck_slot_info_msg *msg);
void ck_token_info_msg_serialize (struct ck_token_info_msg *msg);
void ck_session_info_msg_serialize (struct ck_session_info_msg *msg);
void ck_attribute_msg_serialize (struct ck_attribute_msg *msg);
void ck_date_msg_serialize (struct ck_date_msg *msg);
void ck_mechanism_msg_serialize (struct ck_mechanism_msg *msg);
void ck_mechanism_info_msg_serialize (struct ck_mechanism_info_msg *msg);
void C_Initialize_Req_msg_serialize (struct C_Initialize_Req_msg *msg);
void C_Initialize_Rsp_msg_serialize (struct C_Initialize_Rsp_msg *msg);
void C_Finalize_Req_msg_serialize (struct C_Finalize_Req_msg *msg);
void C_Finalize_Rsp_msg_serialize (struct C_Finalize_Rsp_msg *msg);
void C_GetInfo_Req_msg_serialize (struct C_GetInfo_Req_msg *msg);
void C_GetInfo_Rsp_msg_serialize (struct C_GetInfo_Rsp_msg *msg);
void C_GetSlotList_Req_msg_serialize (struct C_GetSlotList_Req_msg *msg);
void C_GetSlotList_Rsp_msg_serialize (struct C_GetSlotList_Rsp_msg *msg);
void C_GetSlotInfo_Req_msg_serialize (struct C_GetSlotInfo_Req_msg *msg);
void C_GetSlotInfo_Rsp_msg_serialize (struct C_GetSlotInfo_Rsp_msg *msg);
void C_GetTokenInfo_Req_msg_serialize (struct C_GetTokenInfo_Req_msg *msg);
void C_GetTokenInfo_Rsp_msg_serialize (struct C_GetTokenInfo_Rsp_msg *msg);
void C_GetMechanismList_Req_msg_serialize (struct C_GetMechanismList_Req_msg *msg);
void C_GetMechanismList_Rsp_msg_serialize (struct C_GetMechanismList_Rsp_msg *msg);
void C_GetMechanismInfo_Req_msg_serialize (struct C_GetMechanismInfo_Req_msg *msg);
void C_GetMechanismInfo_Rsp_msg_serialize (struct C_GetMechanismInfo_Rsp_msg *msg);
void C_InitToken_Req_msg_serialize (struct C_InitToken_Req_msg *msg);
void C_InitToken_Rsp_msg_serialize (struct C_InitToken_Rsp_msg *msg);
void C_InitPIN_Req_msg_serialize (struct C_InitPIN_Req_msg *msg);
void C_InitPIN_Rsp_msg_serialize (struct C_InitPIN_Rsp_msg *msg);
void C_SetPIN_Req_msg_serialize (struct C_SetPIN_Req_msg *msg);
void C_SetPIN_Rsp_msg_serialize (struct C_SetPIN_Rsp_msg *msg);
void C_OpenSession_Req_msg_serialize (struct C_OpenSession_Req_msg *msg);
void C_OpenSession_Rsp_msg_serialize (struct C_OpenSession_Rsp_msg *msg);
void C_CloseSession_Req_msg_serialize (struct C_CloseSession_Req_msg *msg);
void C_CloseSession_Rsp_msg_serialize (struct C_CloseSession_Rsp_msg *msg);
void C_CloseAllSessions_Req_msg_serialize (struct C_CloseAllSessions_Req_msg *msg);
void C_CloseAllSessions_Rsp_msg_serialize (struct C_CloseAllSessions_Rsp_msg *msg);
void C_GetSessionInfo_Req_msg_serialize (struct C_GetSessionInfo_Req_msg *msg);
void C_GetSessionInfo_Rsp_msg_serialize (struct C_GetSessionInfo_Rsp_msg *msg);
void C_Login_Req_msg_serialize (struct C_Login_Req_msg *msg);
void C_Login_Rsp_msg_serialize (struct C_Login_Rsp_msg *msg);
void C_Logout_Req_msg_serialize (struct C_Logout_Req_msg *msg);
void C_Logout_Rsp_msg_serialize (struct C_Logout_Rsp_msg *msg);
void C_CreateObject_Req_msg_serialize (struct C_CreateObject_Req_msg *msg);
void C_CreateObject_Rsp_msg_serialize (struct C_CreateObject_Rsp_msg *msg);
void C_DestroyObject_Req_msg_serialize (struct C_DestroyObject_Req_msg *msg);
void C_DestroyObject_Rsp_msg_serialize (struct C_DestroyObject_Rsp_msg *msg);
void C_GetAttributeValue_Req_msg_serialize (struct C_GetAttributeValue_Req_msg *msg);
void C_GetAttributeValue_Rsp_msg_serialize (struct C_GetAttributeValue_Rsp_msg *msg);
void C_SetAttributeValue_Req_msg_serialize (struct C_SetAttributeValue_Req_msg *msg);
void C_SetAttributeValue_Rsp_msg_serialize (struct C_SetAttributeValue_Rsp_msg *msg);
void C_FindObjectsInit_Req_msg_serialize (struct C_FindObjectsInit_Req_msg *msg);
void C_FindObjectsInit_Rsp_msg_serialize (struct C_FindObjectsInit_Rsp_msg *msg);
void C_FindObjects_Req_msg_serialize (struct C_FindObjects_Req_msg *msg);
void C_FindObjects_Rsp_msg_serialize (struct C_FindObjects_Rsp_msg *msg);
void C_FindObjectsFinal_Req_msg_serialize (struct C_FindObjectsFinal_Req_msg *msg);
void C_FindObjectsFinal_Rsp_msg_serialize (struct C_FindObjectsFinal_Rsp_msg *msg);
void C_EncryptInit_Req_msg_serialize (struct C_EncryptInit_Req_msg *msg);
void C_EncryptInit_Rsp_msg_serialize (struct C_EncryptInit_Rsp_msg *msg);
void C_Encrypt_Req_msg_serialize (struct C_Encrypt_Req_msg *msg);
void C_Encrypt_Rsp_msg_serialize (struct C_Encrypt_Rsp_msg *msg);
void C_EncryptUpdate_Req_msg_serialize (struct C_EncryptUpdate_Req_msg *msg);
void C_EncryptUpdate_Rsp_msg_serialize (struct C_EncryptUpdate_Rsp_msg *msg);
void C_EncryptFinal_Req_msg_serialize (struct C_EncryptFinal_Req_msg *msg);
void C_EncryptFinal_Rsp_msg_serialize (struct C_EncryptFinal_Rsp_msg *msg);
void C_DecryptInit_Req_msg_serialize (struct C_DecryptInit_Req_msg *msg);
void C_DecryptInit_Rsp_msg_serialize (struct C_DecryptInit_Rsp_msg *msg);
void C_Decrypt_Req_msg_serialize (struct C_Decrypt_Req_msg *msg);
void C_Decrypt_Rsp_msg_serialize (struct C_Decrypt_Rsp_msg *msg);
void C_DecryptUpdate_Req_msg_serialize (struct C_DecryptUpdate_Req_msg *msg);
void C_DecryptUpdate_Rsp_msg_serialize (struct C_DecryptUpdate_Rsp_msg *msg);
void C_DecryptFinal_Req_msg_serialize (struct C_DecryptFinal_Req_msg *msg);
void C_DecryptFinal_Rsp_msg_serialize (struct C_DecryptFinal_Rsp_msg *msg);
void C_SignInit_Req_msg_serialize (struct C_SignInit_Req_msg *msg);
void C_SignInit_Rsp_msg_serialize (struct C_SignInit_Rsp_msg *msg);
void C_Sign_Req_msg_serialize (struct C_Sign_Req_msg *msg);
void C_Sign_Rsp_msg_serialize (struct C_Sign_Rsp_msg *msg);
void C_SignUpdate_Req_msg_serialize (struct C_SignUpdate_Req_msg *msg);
void C_SignUpdate_Rsp_msg_serialize (struct C_SignUpdate_Rsp_msg *msg);
void C_SignFinal_Req_msg_serialize (struct C_SignFinal_Req_msg *msg);
void C_SignFinal_Rsp_msg_serialize (struct C_SignFinal_Rsp_msg *msg);
void C_VerifyInit_Req_msg_serialize (struct C_VerifyInit_Req_msg *msg);
void C_VerifyInit_Rsp_msg_serialize (struct C_VerifyInit_Rsp_msg *msg);
void C_Verify_Req_msg_serialize (struct C_Verify_Req_msg *msg);
void C_Verify_Rsp_msg_serialize (struct C_Verify_Rsp_msg *msg);
void C_VerifyUpdate_Req_msg_serialize (struct C_VerifyUpdate_Req_msg *msg);
void C_VerifyUpdate_Rsp_msg_serialize (struct C_VerifyUpdate_Rsp_msg *msg);
void C_VerifyFinal_Req_msg_serialize (struct C_VerifyFinal_Req_msg *msg);
void C_VerifyFinal_Rsp_msg_serialize (struct C_VerifyFinal_Rsp_msg *msg);
void C_GenerateKey_Req_msg_serialize (struct C_GenerateKey_Req_msg *msg);
void C_GenerateKey_Rsp_msg_serialize (struct C_GenerateKey_Rsp_msg *msg);

#if defined(__cplusplus)
}
#endif

#endif

