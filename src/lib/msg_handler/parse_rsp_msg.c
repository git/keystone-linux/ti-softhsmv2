/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdint.h>

#include "msg_deserialize.h"
#include "parse_msg_types.h"
#include "parse_rsp_msg.h"

int parse_C_Initialize_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Initialize_Rsp_msg_deserialize(&(msg->body.C_Initialize_Rsp));
	*retval = msg->body.C_Initialize_Rsp.retval;
	return 0;
}

int parse_C_Finalize_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Finalize_Rsp_msg_deserialize(&(msg->body.C_Finalize_Rsp));
	*retval = msg->body.C_Finalize_Rsp.retval;
	return 0;
}

int parse_C_GetInfo_Rsp_msg
(
	uint32_t	*retval,
	CK_INFO	*info,
	msg_t	*msg
)
{
	C_GetInfo_Rsp_msg_deserialize(&(msg->body.C_GetInfo_Rsp));
	*retval = msg->body.C_GetInfo_Rsp.retval;
	parse_ck_info_msg(&(msg->body.C_GetInfo_Rsp.info), info);
	return 0;
}

int parse_C_GetSlotList_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*count,
	uint32_t	*slot_list,
	msg_t	*msg
)
{
	uint32_t i;

	C_GetSlotList_Rsp_msg_deserialize(&(msg->body.C_GetSlotList_Rsp));
	*retval = msg->body.C_GetSlotList_Rsp.retval;
	*count = msg->body.C_GetSlotList_Rsp.count;

	if (!slot_list ) {
		return 0;
	}

	for (i=0; i<*count; i++)
	{
		slot_list[i] = msg->body.C_GetSlotList_Rsp.slot_list[i];
	}
	return 0;
}

int parse_C_GetSlotInfo_Rsp_msg
(
	uint32_t	*retval,
	CK_SLOT_INFO	*info,
	msg_t	*msg
)
{
	C_GetSlotInfo_Rsp_msg_deserialize(&(msg->body.C_GetSlotInfo_Rsp));
	*retval = msg->body.C_GetSlotInfo_Rsp.retval;
	parse_ck_slot_info_msg(&(msg->body.C_GetSlotInfo_Rsp.info), info);
	return 0;
}

int parse_C_GetTokenInfo_Rsp_msg
(
	uint32_t	*retval,
	CK_TOKEN_INFO	*info,
	msg_t	*msg
)
{
	C_GetTokenInfo_Rsp_msg_deserialize(&(msg->body.C_GetTokenInfo_Rsp));
	*retval = msg->body.C_GetTokenInfo_Rsp.retval;
	parse_ck_token_info_msg(&(msg->body.C_GetTokenInfo_Rsp.info), info);
	return 0;
}

int parse_C_GetMechanismList_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*count,
	uint32_t	*mechanism_list,
	msg_t	*msg
)
{
	uint32_t i;

	C_GetMechanismList_Rsp_msg_deserialize(&(msg->body.C_GetMechanismList_Rsp));
	*retval = msg->body.C_GetMechanismList_Rsp.retval;
	*count = msg->body.C_GetMechanismList_Rsp.count;

	if (!mechanism_list) {
		return 0;
	}

	for (i=0; i<*count; i++)
	{
		mechanism_list[i] = msg->body.C_GetMechanismList_Rsp.mechanism_list[i];
	}
	return 0;
}

int parse_C_GetMechanismInfo_Rsp_msg
(
	uint32_t	*retval,
	CK_MECHANISM_INFO	*info,
	msg_t	*msg
)
{
	C_GetMechanismInfo_Rsp_msg_deserialize(&(msg->body.C_GetMechanismInfo_Rsp));
	*retval = msg->body.C_GetMechanismInfo_Rsp.retval;
	parse_ck_mechanism_info_msg(&(msg->body.C_GetMechanismInfo_Rsp.info), info);
	return 0;
}

int parse_C_InitToken_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_InitToken_Rsp_msg_deserialize(&(msg->body.C_InitToken_Rsp));
	*retval = msg->body.C_InitToken_Rsp.retval;
	return 0;
}

int parse_C_InitPIN_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_InitPIN_Rsp_msg_deserialize(&(msg->body.C_InitPIN_Rsp));
	*retval = msg->body.C_InitPIN_Rsp.retval;
	return 0;
}

int parse_C_SetPIN_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_SetPIN_Rsp_msg_deserialize(&(msg->body.C_SetPIN_Rsp));
	*retval = msg->body.C_SetPIN_Rsp.retval;
	return 0;
}

int parse_C_OpenSession_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*session,
	msg_t	*msg
)
{
	C_OpenSession_Rsp_msg_deserialize(&(msg->body.C_OpenSession_Rsp));
	*retval = msg->body.C_OpenSession_Rsp.retval;
	*session = msg->body.C_OpenSession_Rsp.session;
	return 0;
}

int parse_C_CloseSession_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_CloseSession_Rsp_msg_deserialize(&(msg->body.C_CloseSession_Rsp));
	*retval = msg->body.C_CloseSession_Rsp.retval;
	return 0;
}

int parse_C_CloseAllSessions_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_CloseAllSessions_Rsp_msg_deserialize(&(msg->body.C_CloseAllSessions_Rsp));
	*retval = msg->body.C_CloseAllSessions_Rsp.retval;
	return 0;
}

int parse_C_GetSessionInfo_Rsp_msg
(
	uint32_t	*retval,
	CK_SESSION_INFO	*info,
	msg_t	*msg
)
{
	C_GetSessionInfo_Rsp_msg_deserialize(&(msg->body.C_GetSessionInfo_Rsp));
	*retval = msg->body.C_GetSessionInfo_Rsp.retval;
	parse_ck_session_info_msg(&(msg->body.C_GetSessionInfo_Rsp.info), info);
	return 0;
}

int parse_C_Login_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Login_Rsp_msg_deserialize(&(msg->body.C_Login_Rsp));
	*retval = msg->body.C_Login_Rsp.retval;
	return 0;
}

int parse_C_Logout_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Logout_Rsp_msg_deserialize(&(msg->body.C_Logout_Rsp));
	*retval = msg->body.C_Logout_Rsp.retval;
	return 0;
}

int parse_C_CreateObject_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*object,
	msg_t	*msg
)
{
	C_CreateObject_Rsp_msg_deserialize(&(msg->body.C_CreateObject_Rsp));
	*retval = msg->body.C_CreateObject_Rsp.retval;
	*object = msg->body.C_CreateObject_Rsp.object;
	return 0;
}

int parse_C_DestroyObject_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_DestroyObject_Rsp_msg_deserialize(&(msg->body.C_DestroyObject_Rsp));
	*retval = msg->body.C_DestroyObject_Rsp.retval;
	return 0;
}

int parse_C_GetAttributeValue_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*count,
	CK_ATTRIBUTE	*templ,
	msg_t	*msg
)
{
	uint32_t i;
	uint8_t *attr;

	C_GetAttributeValue_Rsp_msg_deserialize(&(msg->body.C_GetAttributeValue_Rsp));
	*retval = msg->body.C_GetAttributeValue_Rsp.retval;
	*count = msg->body.C_GetAttributeValue_Rsp.count;

	attr = (uint8_t *)&(msg->body.C_GetAttributeValue_Rsp.templ[0]);
	for (i=0; i<*count; i++)
	{
		parse_ck_attribute_msg((struct ck_attribute_msg *)attr, &templ[i]);
		if ((templ[i].ulValueLen) && (templ[i].ulValueLen != (uint32_t)-1)) {
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}
	return 0;
}

int parse_C_SetAttributeValue_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_SetAttributeValue_Rsp_msg_deserialize(&(msg->body.C_SetAttributeValue_Rsp));
	*retval = msg->body.C_SetAttributeValue_Rsp.retval;
	return 0;
}

int parse_C_FindObjectsInit_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_FindObjectsInit_Rsp_msg_deserialize(&(msg->body.C_FindObjectsInit_Rsp));
	*retval = msg->body.C_FindObjectsInit_Rsp.retval;
	return 0;
}

int parse_C_FindObjects_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*object_count,
	uint32_t	*object,
	msg_t	*msg
)
{
	uint32_t i;

	C_FindObjects_Rsp_msg_deserialize(&(msg->body.C_FindObjects_Rsp));
	*retval = msg->body.C_FindObjects_Rsp.retval;
	*object_count = msg->body.C_FindObjects_Rsp.object_count;
	for (i=0; i<*object_count; i++)
	{
		object[i] = msg->body.C_FindObjects_Rsp.object[i];
	}
	return 0;
}

int parse_C_FindObjectsFinal_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_FindObjectsFinal_Rsp_msg_deserialize(&(msg->body.C_FindObjectsFinal_Rsp));
	*retval = msg->body.C_FindObjectsFinal_Rsp.retval;
	return 0;
}

int parse_C_EncryptInit_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_EncryptInit_Rsp_msg_deserialize(&(msg->body.C_EncryptInit_Rsp));
	*retval = msg->body.C_EncryptInit_Rsp.retval;
	return 0;
}

int parse_C_Encrypt_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*encrypted_data_len,
	uint8_t	*encrypted_data,
	msg_t	*msg
)
{
	uint32_t i;

	C_Encrypt_Rsp_msg_deserialize(&(msg->body.C_Encrypt_Rsp));
	*retval = msg->body.C_Encrypt_Rsp.retval;
	*encrypted_data_len = msg->body.C_Encrypt_Rsp.encrypted_data_len;
	for (i=0; i<*encrypted_data_len; i++)
	{
		encrypted_data[i] = msg->body.C_Encrypt_Rsp.encrypted_data[i];
	}
	return 0;
}

int parse_C_EncryptUpdate_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*encrypted_part_len,
	uint8_t	*encrypted_part,
	msg_t	*msg
)
{
	uint32_t i;

	C_EncryptUpdate_Rsp_msg_deserialize(&(msg->body.C_EncryptUpdate_Rsp));
	*retval = msg->body.C_EncryptUpdate_Rsp.retval;
	*encrypted_part_len = msg->body.C_EncryptUpdate_Rsp.encrypted_part_len;
	for (i=0; i<*encrypted_part_len; i++)
	{
		encrypted_part[i] = msg->body.C_EncryptUpdate_Rsp.encrypted_part[i];
	}
	return 0;
}

int parse_C_EncryptFinal_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*last_encrypted_part_len,
	uint8_t	*last_encrypted_part,
	msg_t	*msg
)
{
	uint32_t i;

	C_EncryptFinal_Rsp_msg_deserialize(&(msg->body.C_EncryptFinal_Rsp));
	*retval = msg->body.C_EncryptFinal_Rsp.retval;
	*last_encrypted_part_len = msg->body.C_EncryptFinal_Rsp.last_encrypted_part_len;
	for (i=0; i<*last_encrypted_part_len; i++)
	{
		last_encrypted_part[i] = msg->body.C_EncryptFinal_Rsp.last_encrypted_part[i];
	}
	return 0;
}

int parse_C_DecryptInit_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_DecryptInit_Rsp_msg_deserialize(&(msg->body.C_DecryptInit_Rsp));
	*retval = msg->body.C_DecryptInit_Rsp.retval;
	return 0;
}

int parse_C_Decrypt_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*data_len,
	uint8_t	*data,
	msg_t	*msg
)
{
	uint32_t i;

	C_Decrypt_Rsp_msg_deserialize(&(msg->body.C_Decrypt_Rsp));
	*retval = msg->body.C_Decrypt_Rsp.retval;
	*data_len = msg->body.C_Decrypt_Rsp.data_len;
	for (i=0; i<*data_len; i++)
	{
		data[i] = msg->body.C_Decrypt_Rsp.data[i];
	}
	return 0;
}

int parse_C_DecryptUpdate_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*part_len,
	uint8_t	*part,
	msg_t	*msg
)
{
	uint32_t i;

	C_DecryptUpdate_Rsp_msg_deserialize(&(msg->body.C_DecryptUpdate_Rsp));
	*retval = msg->body.C_DecryptUpdate_Rsp.retval;
	*part_len = msg->body.C_DecryptUpdate_Rsp.part_len;
	for (i=0; i<*part_len; i++)
	{
		part[i] = msg->body.C_DecryptUpdate_Rsp.part[i];
	}
	return 0;
}

int parse_C_DecryptFinal_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*last_part_len,
	uint8_t	*last_part,
	msg_t	*msg
)
{
	uint32_t i;

	C_DecryptFinal_Rsp_msg_deserialize(&(msg->body.C_DecryptFinal_Rsp));
	*retval = msg->body.C_DecryptFinal_Rsp.retval;
	*last_part_len = msg->body.C_DecryptFinal_Rsp.last_part_len;
	for (i=0; i<*last_part_len; i++)
	{
		last_part[i] = msg->body.C_DecryptFinal_Rsp.last_part[i];
	}
	return 0;
}

int parse_C_SignInit_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_SignInit_Rsp_msg_deserialize(&(msg->body.C_SignInit_Rsp));
	*retval = msg->body.C_SignInit_Rsp.retval;
	return 0;
}

int parse_C_Sign_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*signature_len,
	uint8_t	*signature,
	msg_t	*msg
)
{
	uint32_t i;

	C_Sign_Rsp_msg_deserialize(&(msg->body.C_Sign_Rsp));
	*retval = msg->body.C_Sign_Rsp.retval;
	*signature_len = msg->body.C_Sign_Rsp.signature_len;

	if ((*retval != CKR_OK) || (!signature))
		return 0;

	for (i=0; i<*signature_len; i++)
	{
		signature[i] = msg->body.C_Sign_Rsp.signature[i];
	}
	return 0;
}

int parse_C_SignUpdate_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_SignUpdate_Rsp_msg_deserialize(&(msg->body.C_SignUpdate_Rsp));
	*retval = msg->body.C_SignUpdate_Rsp.retval;
	return 0;
}

int parse_C_SignFinal_Rsp_msg
(
	uint32_t	*retval,
	uint32_t	*signature_len,
	uint8_t	*signature,
	msg_t	*msg
)
{
	uint32_t i;

	C_SignFinal_Rsp_msg_deserialize(&(msg->body.C_SignFinal_Rsp));
	*retval = msg->body.C_SignFinal_Rsp.retval;
	*signature_len = msg->body.C_SignFinal_Rsp.signature_len;
	for (i=0; i<*signature_len; i++)
	{
		signature[i] = msg->body.C_SignFinal_Rsp.signature[i];
	}
	return 0;
}

int parse_C_VerifyInit_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_VerifyInit_Rsp_msg_deserialize(&(msg->body.C_VerifyInit_Rsp));
	*retval = msg->body.C_VerifyInit_Rsp.retval;
	return 0;
}

int parse_C_Verify_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Verify_Rsp_msg_deserialize(&(msg->body.C_Verify_Rsp));
	*retval = msg->body.C_Verify_Rsp.retval;
	return 0;
}

int parse_C_VerifyUpdate_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_VerifyUpdate_Rsp_msg_deserialize(&(msg->body.C_VerifyUpdate_Rsp));
	*retval = msg->body.C_VerifyUpdate_Rsp.retval;
	return 0;
}

int parse_C_VerifyFinal_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_VerifyFinal_Rsp_msg_deserialize(&(msg->body.C_VerifyFinal_Rsp));
	*retval = msg->body.C_VerifyFinal_Rsp.retval;
	return 0;
}

int parse_C_GenerateKey_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_GenerateKey_Rsp_msg_deserialize(&(msg->body.C_GenerateKey_Rsp));
	*retval = msg->body.C_GenerateKey_Rsp.retval;
	return 0;
}
