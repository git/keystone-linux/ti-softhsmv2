/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __PARSE_REQ_MSG_H__
#define __PARSE_REQ_MSG_H__

#include <stdint.h>

#include "cryptoki.h"
#include "pkcs11_msg.h"

#if defined(__cplusplus)
extern "C" {
#endif

int parse_Req_msg_hdr(char *data, int length, msg_hdr_t *hdr);

int parse_C_Initialize_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Finalize_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetSlotList_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetSlotInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetTokenInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetMechanismList_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetMechanismInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_InitToken_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_InitPIN_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_SetPIN_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_OpenSession_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_CloseSession_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_CloseAllSessions_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetSessionInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Login_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Logout_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_CreateObject_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_DestroyObject_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GetAttributeValue_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_SetAttributeValue_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_FindObjectsInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_FindObjects_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_FindObjectsFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_EncryptInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Encrypt_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_EncryptUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_EncryptFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_DecryptInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Decrypt_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_DecryptUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_DecryptFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_SignInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Sign_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_SignUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_SignFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_VerifyInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_Verify_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_VerifyUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_VerifyFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

int parse_C_GenerateKey_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 );

#if defined(__cplusplus)
}
#endif

#endif /* __PARSE_REQ_MSG_H__ */

