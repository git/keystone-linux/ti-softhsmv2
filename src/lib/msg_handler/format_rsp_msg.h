/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __FORMAT_RSP_MSG_H__
#define __FORMAT_RSP_MSG_H__

#include <stdint.h>

#include "cryptoki.h"
#include "pkcs11_msg.h"

#if defined(__cplusplus)
extern "C" {
#endif

int format_C_Initialize_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Finalize_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_GetInfo_Rsp_msg
(
	uint32_t	retval,
	CK_INFO	*info,
	msg_t	**msg
);

int format_C_GetSlotList_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	uint32_t	*slot_list,
	msg_t	**msg
);

int format_C_GetSlotInfo_Rsp_msg
(
	uint32_t	retval,
	CK_SLOT_INFO	*info,
	msg_t	**msg
);

int format_C_GetTokenInfo_Rsp_msg
(
	uint32_t	retval,
	CK_TOKEN_INFO	*info,
	msg_t	**msg
);

int format_C_GetMechanismList_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	uint32_t	*mechanism_list,
	msg_t	**msg
);

int format_C_GetMechanismInfo_Rsp_msg
(
	uint32_t	retval,
	CK_MECHANISM_INFO	*info,
	msg_t	**msg
);

int format_C_InitToken_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_InitPIN_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_SetPIN_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_OpenSession_Rsp_msg
(
	uint32_t	retval,
	uint32_t	session,
	msg_t	**msg
);

int format_C_CloseSession_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_CloseAllSessions_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_GetSessionInfo_Rsp_msg
(
	uint32_t	retval,
	CK_SESSION_INFO	*info,
	msg_t	**msg
);

int format_C_Login_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Logout_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_CreateObject_Rsp_msg
(
	uint32_t	retval,
	uint32_t	object,
	msg_t	**msg
);

int format_C_DestroyObject_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_GetAttributeValue_Rsp_msg
(
	uint32_t	retval,
	uint32_t	count,
	CK_ATTRIBUTE	*templ,
	msg_t	**msg
);

int format_C_SetAttributeValue_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_FindObjectsInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_FindObjects_Rsp_msg
(
	uint32_t	retval,
	uint32_t	object_count,
	uint32_t	*object,
	msg_t	**msg
);

int format_C_FindObjectsFinal_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_EncryptInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Encrypt_Rsp_msg
(
	uint32_t	retval,
	uint32_t	encrypted_data_len,
	uint8_t	*encrypted_data,
	msg_t	**msg
);

int format_C_EncryptUpdate_Rsp_msg
(
	uint32_t	retval,
	uint32_t	encrypted_part_len,
	uint8_t	*encrypted_part,
	msg_t	**msg
);

int format_C_EncryptFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	last_encrypted_part_len,
	uint8_t	*last_encrypted_part,
	msg_t	**msg
);

int format_C_DecryptInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Decrypt_Rsp_msg
(
	uint32_t	retval,
	uint32_t	data_len,
	uint8_t	*data,
	msg_t	**msg
);

int format_C_DecryptUpdate_Rsp_msg
(
	uint32_t	retval,
	uint32_t	part_len,
	uint8_t	*part,
	msg_t	**msg
);

int format_C_DecryptFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	last_part_len,
	uint8_t	*last_part,
	msg_t	**msg
);

int format_C_SignInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Sign_Rsp_msg
(
	uint32_t	retval,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
);

int format_C_SignUpdate_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_SignFinal_Rsp_msg
(
	uint32_t	retval,
	uint32_t	signature_len,
	uint8_t	*signature,
	msg_t	**msg
);

int format_C_VerifyInit_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Verify_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_VerifyUpdate_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_VerifyFinal_Rsp_msg
(
	uint32_t	retval,
	msg_t	**msg
);

int format_C_Error_Rsp_msg
(
	uint32_t	errorval,
	msg_t	**msg
);

int format_C_GenerateKey_Rsp_msg
(
	uint32_t	errorval,
	msg_t	**msg
);
#if defined(__cplusplus)
}
#endif

#endif /* __FORMAT_RSP_MSG_H__ */

