/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "msg_deserialize.h"
#include "parse_msg_types.h"
#include "parse_req_msg.h"
#include "format_rsp_msg.h"
#include "log.h"

int parse_Req_msg_hdr(char *data, int length, msg_hdr_t *hdr)
{
	if ((int)sizeof(msg_hdr_t) > length) return -1;

	*hdr = *(msg_hdr_t *) data;

	msg_hdr_deserialize(hdr);

	if (hdr->id <= 0 || hdr->id >= MSG_ID_C_EndOfMsgId_Req) return -1;

	if ((int)(sizeof(msg_hdr_t) + hdr->len) > length) {
		ERROR_MSG("Insufficient message data expected size %d, received size = %d\n",
				(int) (sizeof(msg_hdr_t) + hdr->len), length);
		return -1;
	}

	return 0;
}

int parse_C_Initialize_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	reserved;
	int rv;

	C_Initialize_Req_msg_deserialize(&(msg->body.C_Initialize_Req));
	reserved = msg->body.C_Initialize_Req.reserved;
	rv = C_Initialize_daemon(hsm, NULL);
	rv = format_C_Initialize_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_Finalize_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	reserved;
	int rv;

	C_Finalize_Req_msg_deserialize(&(msg->body.C_Finalize_Req));
	reserved = msg->body.C_Finalize_Req.reserved;
	rv = C_Finalize_daemon(hsm, NULL);
	rv = format_C_Finalize_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_GetInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	reserved;
	int rv;
	CK_INFO  info;

	C_GetInfo_Req_msg_deserialize(&(msg->body.C_GetInfo_Req));
	reserved = msg->body.C_GetInfo_Req.reserved;
	rv = C_GetInfo_daemon(hsm, &info);
	rv = format_C_GetInfo_Rsp_msg(rv, &info, rmsg);
	return rv;
}

int parse_C_GetSlotList_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint8_t	token_present;
	unsigned long	count;
	int rv;
	CK_SLOT_ID *slot_list = NULL;

	C_GetSlotList_Req_msg_deserialize(&(msg->body.C_GetSlotList_Req));
	token_present = msg->body.C_GetSlotList_Req.token_present;
	count = msg->body.C_GetSlotList_Req.count;

	if (count) {
		slot_list = (CK_SLOT_ID *) malloc(sizeof(*slot_list) * count);

		if (slot_list == NULL) {
			return -1;
		}
	}

	rv = C_GetSlotList_daemon(hsm, token_present, slot_list, &count);

	rv = format_C_GetSlotList_Rsp_msg(rv, (uint32_t)count, (uint32_t *)slot_list, rmsg);

	if (slot_list)
		free(slot_list);

	return rv;
}

int parse_C_GetSlotInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	int rv;
	CK_SLOT_INFO slot_info;

	C_GetSlotInfo_Req_msg_deserialize(&(msg->body.C_GetSlotInfo_Req));
	slot_id = msg->body.C_GetSlotInfo_Req.slot_id;
	rv = C_GetSlotInfo_daemon(hsm, slot_id, &slot_info);
	rv = format_C_GetSlotInfo_Rsp_msg(rv, &slot_info, rmsg);
	return rv;
}

int parse_C_GetTokenInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	int rv;
	CK_TOKEN_INFO info;

	C_GetTokenInfo_Req_msg_deserialize(&(msg->body.C_GetTokenInfo_Req));
	slot_id = msg->body.C_GetTokenInfo_Req.slot_id;
	rv = C_GetTokenInfo_daemon(hsm, slot_id, &info);
	rv = format_C_GetTokenInfo_Rsp_msg(rv, &info, rmsg);
	return rv;
}

int parse_C_GetMechanismList_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	unsigned long 	count;
	int rv;
	CK_MECHANISM_TYPE *list = NULL;

	C_GetMechanismList_Req_msg_deserialize(&(msg->body.C_GetMechanismList_Req));
	slot_id = msg->body.C_GetMechanismList_Req.slot_id;
	count = msg->body.C_GetMechanismList_Req.count;

	if (count) {
		list = (CK_MECHANISM_TYPE *) malloc(sizeof(*list) * count);
		if (list == NULL) {
			return -1;
		}
	}

	rv = C_GetMechanismList_daemon(hsm, slot_id, list, &count);
	rv = format_C_GetMechanismList_Rsp_msg(rv, count, (uint32_t *)list, rmsg);
	if (list) free(list);
	return rv;
}

int parse_C_GetMechanismInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	uint32_t	type;
	int rv;
	CK_MECHANISM_INFO info;

	C_GetMechanismInfo_Req_msg_deserialize(&(msg->body.C_GetMechanismInfo_Req));
	slot_id = msg->body.C_GetMechanismInfo_Req.slot_id;
	type = msg->body.C_GetMechanismInfo_Req.type;
	rv = C_GetMechanismInfo_daemon(hsm, slot_id, type, &info);
	rv = format_C_GetMechanismInfo_Rsp_msg(rv, &info, rmsg);
	return rv;
}

int parse_C_InitToken_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	uint32_t	pin_len;
	uint8_t	*pin;
	uint8_t	label[32];
	int rv;
    uint32_t i;

	C_InitToken_Req_msg_deserialize(&(msg->body.C_InitToken_Req));
	slot_id = msg->body.C_InitToken_Req.slot_id;
	pin_len = msg->body.C_InitToken_Req.pin_len;
	pin = (uint8_t	*) malloc(sizeof(*pin) * pin_len);

	if (pin == NULL) {
		return -1;
	}

	for (i=0; i<pin_len; i++)
	{
		pin[i] = msg->body.C_InitToken_Req.pin[i];
	}

	memcpy(label, msg->body.C_InitToken_Req.label, 32);
	rv = C_InitToken_daemon(hsm, slot_id, pin, pin_len, label);
	rv = format_C_InitToken_Rsp_msg(rv, rmsg);
	free(pin);
	return rv;
}

int parse_C_InitPIN_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	pin_len;
	uint8_t	*pin;
	int rv;
    uint32_t i;

	C_InitPIN_Req_msg_deserialize(&(msg->body.C_InitPIN_Req));
	session = msg->body.C_InitPIN_Req.session;
	pin_len = msg->body.C_InitPIN_Req.pin_len;
	pin = (uint8_t	*) malloc(sizeof(*pin) * pin_len);

	if (pin == NULL) {
		return -1;
	}

	for (i=0; i<pin_len; i++)
	{
		pin[i] = msg->body.C_InitPIN_Req.pin[i];
	}

	rv = C_InitPIN_daemon(hsm, session, pin, pin_len);
	rv = format_C_InitPIN_Rsp_msg(rv, rmsg);
	free(pin);
	return rv;
}

int parse_C_SetPIN_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	old_len;
	uint8_t	*old_pin;
	uint32_t	new_len;
	uint8_t	*new_pin;
	int rv;

	C_SetPIN_Req_msg_deserialize(&(msg->body.C_SetPIN_Req));
	session = msg->body.C_SetPIN_Req.session;
	old_len = msg->body.C_SetPIN_Req.old_len;
	old_pin = (uint8_t*) malloc(sizeof(*old_pin) * old_len);

	if (old_pin == NULL) {
		return -1;
	}

	memcpy(old_pin, msg->body.C_SetPIN_Req.old_pin, old_len);

	new_len = msg->body.C_SetPIN_Req.new_len;
	new_pin = (uint8_t*) malloc(sizeof(*new_pin) * new_len);

	if (new_pin == NULL) {
		free(old_pin);
		return -1;
	}

	memcpy(new_pin, (msg->body.C_SetPIN_Req.old_pin + old_len), new_len);
	rv = C_SetPIN_daemon(hsm, session, old_pin, old_len, new_pin, new_len);
	rv = format_C_SetPIN_Rsp_msg(rv, rmsg);
	free(old_pin);
	free(new_pin);
	return rv;
}

int parse_C_OpenSession_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	uint32_t	flags;
	int rv;
	CK_SESSION_HANDLE session;

	C_OpenSession_Req_msg_deserialize(&(msg->body.C_OpenSession_Req));
	slot_id = msg->body.C_OpenSession_Req.slot_id;
	flags = msg->body.C_OpenSession_Req.flags;
	rv = C_OpenSession_daemon(hsm, slot_id, flags, NULL, NULL, &session);
	rv = format_C_OpenSession_Rsp_msg(rv, session, rmsg);
	return rv;
}

int parse_C_CloseSession_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	int rv;

	C_CloseSession_Req_msg_deserialize(&(msg->body.C_CloseSession_Req));
	session = msg->body.C_CloseSession_Req.session;
	rv = C_CloseSession_daemon(hsm, session);
	rv = format_C_CloseSession_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_CloseAllSessions_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	slot_id;
	int rv;

	C_CloseAllSessions_Req_msg_deserialize(&(msg->body.C_CloseAllSessions_Req));
	slot_id = msg->body.C_CloseAllSessions_Req.slot_id;
	rv = C_CloseAllSessions_daemon(hsm, slot_id);
	rv = format_C_CloseAllSessions_Rsp_msg(rv,rmsg);
	return rv;
}

int parse_C_GetSessionInfo_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	int rv;
	CK_SESSION_INFO info;

	C_GetSessionInfo_Req_msg_deserialize(&(msg->body.C_GetSessionInfo_Req));
	session = msg->body.C_GetSessionInfo_Req.session;
	rv = C_GetSessionInfo_daemon(hsm, session, &info);
	rv = format_C_GetSessionInfo_Rsp_msg(rv, &info, rmsg);
	return rv;
}

int parse_C_Login_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	user_type;
	uint32_t	pin_len;
	uint8_t	*pin;
	int rv;
    uint32_t i;

	C_Login_Req_msg_deserialize(&(msg->body.C_Login_Req));
	session = msg->body.C_Login_Req.session;
	user_type = msg->body.C_Login_Req.user_type;
	pin_len = msg->body.C_Login_Req.pin_len;
	pin = (uint8_t	*) malloc(sizeof(*pin) * pin_len);

	if (pin == NULL) {
		return -1;
	}

	for (i=0; i<pin_len; i++)
	{
		pin[i] = msg->body.C_Login_Req.pin[i];
	}

	rv = C_Login_daemon(hsm, session, user_type, pin, pin_len);

	rv = format_C_Login_Rsp_msg(rv, rmsg);

	free(pin);

	return rv;
}

int parse_C_Logout_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	int rv;

	C_Logout_Req_msg_deserialize(&(msg->body.C_Logout_Req));
	session = msg->body.C_Logout_Req.session;
	rv = C_Logout_daemon(hsm, session);
	rv = format_C_Logout_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_CreateObject_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	count;
	CK_ATTRIBUTE	*templ;
	int rv;
    uint32_t i;
	uint8_t *attr;
	CK_OBJECT_HANDLE  object;

	C_CreateObject_Req_msg_deserialize(&(msg->body.C_CreateObject_Req));
	session = msg->body.C_CreateObject_Req.session;
	count = msg->body.C_CreateObject_Req.count;

	templ = (CK_ATTRIBUTE*) malloc(sizeof(*templ) * count);

	if (templ == NULL) {
		return -1;
	}

	attr = (uint8_t *)&(msg->body.C_CreateObject_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		parse_ck_attribute_msg_malloc((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen) {
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}

	rv = C_CreateObject_daemon(hsm, session, templ, count, &object);
	rv = format_C_CreateObject_Rsp_msg(rv, object, rmsg);

	for (i=0; i<count; i++) {
		free_ck_attribute(&templ[i]);
	}
	free(templ);

	return rv;
}

int parse_C_DestroyObject_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	object;
	int rv;

	C_DestroyObject_Req_msg_deserialize(&(msg->body.C_DestroyObject_Req));
	session = msg->body.C_DestroyObject_Req.session;
	object = msg->body.C_DestroyObject_Req.object;
	rv = C_DestroyObject_daemon(hsm, session, object);
	rv = format_C_DestroyObject_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_GetAttributeValue_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	object;
	uint32_t	i, count;
	CK_ATTRIBUTE	*templ;
	int rv;
	struct ck_attribute_msg *attr;

	C_GetAttributeValue_Req_msg_deserialize(&(msg->body.C_GetAttributeValue_Req));
	session = msg->body.C_GetAttributeValue_Req.session;
	object = msg->body.C_GetAttributeValue_Req.object;
	count = msg->body.C_GetAttributeValue_Req.count;
	templ = (CK_ATTRIBUTE*) malloc(sizeof(*templ) * count);

	if (templ == NULL) {
		return -1;
	}

	attr = &(msg->body.C_GetAttributeValue_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		parse_get_ck_attribute_msg_malloc(&attr[i], &templ[i]);
	}

	rv = C_GetAttributeValue_daemon(hsm, session, object, templ, count);

	rv = format_C_GetAttributeValue_Rsp_msg(rv, count, templ, rmsg);

	for (i=0; i<count; i++) {
		free_ck_attribute(&templ[i]);
	}
	free(templ);

	return rv;
}

int parse_C_SetAttributeValue_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	object;
	uint32_t	i, count;
	CK_ATTRIBUTE	*templ;
	int rv;
	uint8_t *attr;

	C_SetAttributeValue_Req_msg_deserialize(&(msg->body.C_SetAttributeValue_Req));
	session = msg->body.C_SetAttributeValue_Req.session;
	object = msg->body.C_SetAttributeValue_Req.object;
	count = msg->body.C_SetAttributeValue_Req.count;
	templ = (CK_ATTRIBUTE*) malloc(sizeof(*templ) * count);

	if (templ == NULL) {
		return -1;
	}

	attr = (uint8_t *)&(msg->body.C_SetAttributeValue_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		parse_ck_attribute_msg_malloc((struct ck_attribute_msg *)attr, &templ[i]);
		attr = (attr + ((templ[i].ulValueLen + 4) & ~3));
	}


	rv = C_SetAttributeValue_daemon(hsm, session, object, templ, count);
	rv = format_C_SetAttributeValue_Rsp_msg(rv, rmsg);

	for (i=0; i<count; i++) {
		free_ck_attribute(&templ[i]);
	}
	free(templ);

	return rv;
}

int parse_C_FindObjectsInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	i, count;
	CK_ATTRIBUTE	*templ;
	int rv;
	uint8_t *attr;

	C_FindObjectsInit_Req_msg_deserialize(&(msg->body.C_FindObjectsInit_Req));
	session = msg->body.C_FindObjectsInit_Req.session;
	count = msg->body.C_FindObjectsInit_Req.count;
	templ = (CK_ATTRIBUTE*) malloc(sizeof(*templ) * count);

	if (templ == NULL) {
		return -1;
	}

	attr = (uint8_t *)&(msg->body.C_FindObjectsInit_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		parse_ck_attribute_msg_malloc((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen)
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		else
			attr = (attr + sizeof(struct ck_attribute_msg));
	}

	rv = C_FindObjectsInit_daemon(hsm, session, templ, count);
	rv = format_C_FindObjectsInit_Rsp_msg(rv, rmsg);

	for (i=0; i<count; i++) {
		free_ck_attribute(&templ[i]);
	}
	free(templ);

	return rv;
}

int parse_C_FindObjects_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	max_object_count;
	int rv;
	CK_OBJECT_HANDLE *object;
	unsigned long object_count;

	C_FindObjects_Req_msg_deserialize(&(msg->body.C_FindObjects_Req));
	session = msg->body.C_FindObjects_Req.session;
	max_object_count = msg->body.C_FindObjects_Req.max_object_count;

	object = (CK_OBJECT_HANDLE *) malloc(sizeof(*object) * max_object_count);
	if (object == NULL) return -1;

	rv = C_FindObjects_daemon(hsm, session, object, max_object_count, &object_count);
	rv = format_C_FindObjects_Rsp_msg(rv, object_count, (uint32_t*)object, rmsg);
	free(object);	
	return rv;
}

int parse_C_FindObjectsFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	int rv;

	C_FindObjectsFinal_Req_msg_deserialize(&(msg->body.C_FindObjectsFinal_Req));
	session = msg->body.C_FindObjectsFinal_Req.session;
	rv = C_FindObjectsFinal_daemon(hsm, session);
	rv = format_C_FindObjectsFinal_Rsp_msg(rv, rmsg);
	return rv;
}

int parse_C_EncryptInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	CK_MECHANISM	*mechanism;
	uint32_t	key;
	int rv;

	C_EncryptInit_Req_msg_deserialize(&(msg->body.C_EncryptInit_Req));
	session = msg->body.C_EncryptInit_Req.session;
	mechanism = (CK_MECHANISM *) malloc(sizeof(*mechanism));

	if (mechanism == NULL) {
		return -1;
	}
	parse_ck_mechanism_msg(&(msg->body.C_EncryptInit_Req.mechanism), mechanism);
	key = msg->body.C_EncryptInit_Req.key;
	rv = C_EncryptInit_daemon(hsm, session, mechanism, key);
	rv = format_C_EncryptInit_Rsp_msg(rv, rmsg);
	free(mechanism);

	return rv;
}

int parse_C_Encrypt_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long int	encrypted_data_len;
	uint32_t	i, data_len;
	uint8_t	*data, *encrypted_data = NULL;
	int rv;

	C_Encrypt_Req_msg_deserialize(&(msg->body.C_Encrypt_Req));
	session = msg->body.C_Encrypt_Req.session;
	encrypted_data_len = msg->body.C_Encrypt_Req.encrypted_data_len;
	data_len = msg->body.C_Encrypt_Req.data_len;
	data = (uint8_t	*) malloc(sizeof(*data) * data_len);

	if (data == NULL) {
		return -1;
	}

	if (encrypted_data_len) {
		encrypted_data = (uint8_t*) malloc(sizeof(*encrypted_data) * encrypted_data_len);
		if (encrypted_data == NULL) {
			free(data);
			return -1;
		}
	}

	for (i=0; i<data_len; i++)
	{
		data[i] = msg->body.C_Encrypt_Req.data[i];
	}

	rv = C_Encrypt_daemon(hsm, session, data, data_len, encrypted_data, &encrypted_data_len);
	rv = format_C_Encrypt_Rsp_msg(rv, encrypted_data_len, encrypted_data, rmsg);
	free(data);
	if (encrypted_data) free(encrypted_data);
	return rv;
}

int parse_C_EncryptUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	encrypted_part_len;
	uint32_t	i, part_len;
	uint8_t	*part, *encrypted_part;
	int rv;

	C_EncryptUpdate_Req_msg_deserialize(&(msg->body.C_EncryptUpdate_Req));
	session = msg->body.C_EncryptUpdate_Req.session;
	encrypted_part_len = msg->body.C_EncryptUpdate_Req.encrypted_part_len;
	part_len = msg->body.C_EncryptUpdate_Req.part_len;
	part = (uint8_t	*) malloc(sizeof(*part) * part_len);

	if (part == NULL) {
		return -1;
	}

	encrypted_part = (uint8_t	*) malloc(sizeof(*encrypted_part) * encrypted_part_len);

	if (encrypted_part == NULL) {
		free(part);
		return -1;
	}

	for (i=0; i<part_len; i++)
	{
		part[i] = msg->body.C_EncryptUpdate_Req.part[i];
	}

	rv = C_EncryptUpdate_daemon(hsm, session, part, part_len, encrypted_part, &encrypted_part_len);
	rv = format_C_EncryptUpdate_Rsp_msg(rv, encrypted_part_len, encrypted_part, rmsg);
	free(part);
	free(encrypted_part);
	return rv;
}

int parse_C_EncryptFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	last_encrypted_part_len;
	int rv;
	uint8_t     *last_encrypted_part;

	C_EncryptFinal_Req_msg_deserialize(&(msg->body.C_EncryptFinal_Req));
	session = msg->body.C_EncryptFinal_Req.session;
	last_encrypted_part_len = msg->body.C_EncryptFinal_Req.last_encrypted_part_len;

	last_encrypted_part = (uint8_t	*) malloc(last_encrypted_part_len);

	if (last_encrypted_part == NULL) {
		return -1;
	}

	rv = C_EncryptFinal_daemon(hsm, session, last_encrypted_part, &last_encrypted_part_len);
	rv = format_C_EncryptFinal_Rsp_msg(rv, last_encrypted_part_len, last_encrypted_part, rmsg);
	free(last_encrypted_part);
	return rv;
}

int parse_C_DecryptInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	CK_MECHANISM	*mechanism;
	uint32_t	key;
	int rv;

	C_DecryptInit_Req_msg_deserialize(&(msg->body.C_DecryptInit_Req));
	session = msg->body.C_DecryptInit_Req.session;
	mechanism = (CK_MECHANISM *) malloc(sizeof(*mechanism));

	if (mechanism == NULL) {
		return -1;
	}
	parse_ck_mechanism_msg(&(msg->body.C_DecryptInit_Req.mechanism), mechanism);
	key = msg->body.C_DecryptInit_Req.key;
	rv = C_DecryptInit_daemon(hsm, session, mechanism, key);
	rv = format_C_DecryptInit_Rsp_msg(rv, rmsg);
	free(mechanism);

	return rv;
}

int parse_C_Decrypt_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	data_len;
	uint32_t	i, encrypted_data_len;
	uint8_t	*encrypted_data, *data = NULL;
	int rv;

	C_Decrypt_Req_msg_deserialize(&(msg->body.C_Decrypt_Req));
	session = msg->body.C_Decrypt_Req.session;
	data_len = msg->body.C_Decrypt_Req.data_len;
	encrypted_data_len = msg->body.C_Decrypt_Req.encrypted_data_len;
	encrypted_data = (uint8_t*) malloc(sizeof(*encrypted_data) * encrypted_data_len);

	if (encrypted_data == NULL) {
		return -1;
	}

	if (data_len) {
		data = (uint8_t	*) malloc(sizeof(*encrypted_data) * data_len);
		if (data == NULL) {
			free(encrypted_data);
			return -1;
		}
	}

	for (i=0; i<encrypted_data_len; i++)
	{
		encrypted_data[i] = msg->body.C_Decrypt_Req.encrypted_data[i];
	}

	rv = C_Decrypt_daemon(hsm, session, encrypted_data, encrypted_data_len, data, &data_len);
	rv = format_C_Decrypt_Rsp_msg(rv, data_len, data, rmsg);
	free(encrypted_data);
	if (data) free(data);
	return rv;
}

int parse_C_DecryptUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	part_len;
	uint32_t	i, encrypted_part_len;
	uint8_t	*encrypted_part, *part;
	int rv;

	C_DecryptUpdate_Req_msg_deserialize(&(msg->body.C_DecryptUpdate_Req));
	session = msg->body.C_DecryptUpdate_Req.session;
	part_len = msg->body.C_DecryptUpdate_Req.part_len;
	encrypted_part_len = msg->body.C_DecryptUpdate_Req.encrypted_part_len;
	encrypted_part = (uint8_t*) malloc(sizeof(*encrypted_part) * encrypted_part_len);

	if (encrypted_part == NULL) {
		return -1;
	}

	part = (uint8_t	*) malloc(sizeof(*part) * part_len);

	if (part == NULL) {
		free(encrypted_part);
		return -1;
	}

	for (i=0; i<encrypted_part_len; i++)
	{
		encrypted_part[i] = msg->body.C_DecryptUpdate_Req.encrypted_part[i];
	}

	rv = C_DecryptUpdate_daemon(hsm, session, encrypted_part, encrypted_part_len, part, &part_len);
	rv = format_C_DecryptUpdate_Rsp_msg(rv, part_len, part, rmsg);
	free(encrypted_part);
	free(part);
	return rv;
}

int parse_C_DecryptFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	last_part_len;
	int rv;
	uint8_t     *last_part;

	C_DecryptFinal_Req_msg_deserialize(&(msg->body.C_DecryptFinal_Req));
	session = msg->body.C_DecryptFinal_Req.session;
	last_part_len = msg->body.C_DecryptFinal_Req.last_part_len;
	last_part = (uint8_t*) malloc(last_part_len);

	if (last_part == NULL) {
		return -1;
	}

	rv = C_DecryptFinal_daemon(hsm, session, last_part, &last_part_len);
	rv = format_C_DecryptFinal_Rsp_msg(rv, last_part_len, last_part, rmsg);
	free(last_part);
	return rv;
}

int parse_C_SignInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	CK_MECHANISM	*mechanism;
	uint32_t	key;
	int rv;

	C_SignInit_Req_msg_deserialize(&(msg->body.C_SignInit_Req));
	session = msg->body.C_SignInit_Req.session;
	mechanism = (CK_MECHANISM *) malloc(sizeof(*mechanism));

	if (mechanism == NULL) {
		return -1;
	}

	parse_ck_mechanism_msg(&(msg->body.C_SignInit_Req.mechanism), mechanism);
	key = msg->body.C_SignInit_Req.key;
	rv = C_SignInit_daemon(hsm, session, mechanism, key);
	rv = format_C_SignInit_Rsp_msg(rv, rmsg);
	free(mechanism);
	return rv;
}

int parse_C_Sign_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	signature_len;
	uint32_t	i, data_len;
	uint8_t	*data, *signature = NULL;
	int rv;

	C_Sign_Req_msg_deserialize(&(msg->body.C_Sign_Req));
	session = msg->body.C_Sign_Req.session;
	signature_len = msg->body.C_Sign_Req.signature_len;
	data_len = msg->body.C_Sign_Req.data_len;
	data = (uint8_t	*) malloc(sizeof(*data) * data_len);

	if (data == NULL) {
		return -1;
	}

	if (signature_len) {
		signature = (uint8_t*) malloc(sizeof(*signature) * signature_len);
		if (signature == NULL) {
			free(data);
			return -1;
		}
	}

	for (i=0; i<data_len; i++)
	{
		data[i] = msg->body.C_Sign_Req.data[i];
	}

	rv = C_Sign_daemon(hsm, session, data, data_len, signature, &signature_len);
	rv = format_C_Sign_Rsp_msg(rv, signature_len, signature, rmsg);
	free(data);
	if (signature) free(signature);
	return rv;
}

int parse_C_SignUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	i, part_len;
	uint8_t	*part;
	int rv;

	C_SignUpdate_Req_msg_deserialize(&(msg->body.C_SignUpdate_Req));
	session = msg->body.C_SignUpdate_Req.session;
	part_len = msg->body.C_SignUpdate_Req.part_len;
	part = (uint8_t	*) malloc(sizeof(*part) * part_len);

	if (part == NULL) {
		return -1;
	}

	for (i=0; i<part_len; i++)
	{
		part[i] = msg->body.C_SignUpdate_Req.part[i];
	}

	rv = C_SignUpdate_daemon(hsm, session, part, part_len);
	rv = format_C_SignUpdate_Rsp_msg(rv, rmsg);
	free(part);
	return rv;
}

int parse_C_SignFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	unsigned long	signature_len;
	int rv;
	uint8_t     *signature;

	C_SignFinal_Req_msg_deserialize(&(msg->body.C_SignFinal_Req));
	session = msg->body.C_SignFinal_Req.session;
	signature_len = msg->body.C_SignFinal_Req.signature_len;

	signature = (uint8_t*) malloc(signature_len);

	if (signature == NULL) {
		return -1;
	}

	rv = C_SignFinal_daemon(hsm, session, signature, &signature_len);
	rv = format_C_SignFinal_Rsp_msg(rv, signature_len, signature, rmsg);
	free(signature);
	return rv;
}

int parse_C_VerifyInit_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	CK_MECHANISM	*mechanism;
	uint32_t	key;
	int rv;

	C_VerifyInit_Req_msg_deserialize(&(msg->body.C_VerifyInit_Req));
	session = msg->body.C_VerifyInit_Req.session;
	mechanism = (CK_MECHANISM *) malloc(sizeof(*mechanism));

	if (mechanism == NULL) {
		return -1;
	}
	parse_ck_mechanism_msg(&(msg->body.C_VerifyInit_Req.mechanism), mechanism);
	key = msg->body.C_VerifyInit_Req.key;
	rv = C_VerifyInit_daemon(hsm, session, mechanism, key);
	rv = format_C_VerifyInit_Rsp_msg(rv, rmsg);
	free(mechanism);
	return rv;
}

int parse_C_Verify_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	signature_len;
	uint8_t	*signature;
	uint32_t	data_len;
	uint8_t	*data;
	int rv;

	C_Verify_Req_msg_deserialize(&(msg->body.C_Verify_Req));
	session = msg->body.C_Verify_Req.session;
	signature_len = msg->body.C_Verify_Req.signature_len;
	signature = (uint8_t*) malloc(sizeof(*signature) * signature_len);

	if (signature == NULL) {
		return -1;
	}
	memcpy(signature, msg->body.C_Verify_Req.signature, signature_len);

	data_len = msg->body.C_Verify_Req.data_len;
	data = (uint8_t	*) malloc(sizeof(*data) * data_len);

	if (data == NULL) {
		free(signature);
		return -1;
	}
	memcpy(data, (msg->body.C_Verify_Req.signature + signature_len), data_len);

	rv = C_Verify_daemon(hsm, session, data, data_len, signature, signature_len);
	rv = format_C_Verify_Rsp_msg(rv, rmsg);

	free(signature);
	free(data);
	return rv;
}

int parse_C_VerifyUpdate_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	i, part_len;
	uint8_t	*part;
	int rv;

	C_VerifyUpdate_Req_msg_deserialize(&(msg->body.C_VerifyUpdate_Req));
	session = msg->body.C_VerifyUpdate_Req.session;
	part_len = msg->body.C_VerifyUpdate_Req.part_len;
	part = (uint8_t	*) malloc(sizeof(*part) * part_len);

	if (part == NULL) {
		return -1;
	}

	for (i=0; i<part_len; i++)
	{
		part[i] = msg->body.C_VerifyUpdate_Req.part[i];
	}

	rv = C_VerifyUpdate_daemon(hsm, session, part, part_len);
	rv = format_C_VerifyUpdate_Rsp_msg(rv, rmsg);
	free(part);
	return rv;
}

int parse_C_VerifyFinal_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	i, signature_len;
	uint8_t	*signature;
	int rv;

	C_VerifyFinal_Req_msg_deserialize(&(msg->body.C_VerifyFinal_Req));
	session = msg->body.C_VerifyFinal_Req.session;
	signature_len = msg->body.C_VerifyFinal_Req.signature_len;
	signature = (uint8_t*) malloc(sizeof(*signature) * signature_len);

	if (signature == NULL) {
		return -1;
	}

	for (i=0; i<signature_len; i++)
	{
		signature[i] = msg->body.C_VerifyFinal_Req.signature[i];
	}

	rv = C_VerifyFinal_daemon(hsm, session, signature, signature_len);
	rv = format_C_VerifyFinal_Rsp_msg(rv, rmsg);
	free(signature);
	return rv;
}

int parse_C_GenerateKey_Req_msg
(
 void	*hsm,
 msg_t	*msg,
 msg_t	**rmsg
 )
{
	uint32_t	session;
	uint32_t	count;
	CK_ATTRIBUTE	*templ;
	int rv;
	uint32_t i;
	uint8_t *attr;
	CK_OBJECT_HANDLE  object;
	CK_OBJECT_HANDLE hKey;
	CK_MECHANISM mechanism = {CKM_RSA_X9_31_KEY_PAIR_GEN, NULL_PTR, 0};

	C_GenerateKey_Req_msg_deserialize(&(msg->body.C_GenerateKey_Req));
	session = msg->body.C_GenerateKey_Req.session;

	count = msg->body.C_GenerateKey_Req.count;

	templ = (CK_ATTRIBUTE*) malloc(sizeof(*templ) * count);

	if (templ == NULL) {
		return -1;
	}
	attr = (uint8_t *)&(msg->body.C_CreateObject_Req.templ[0]);
	for (i=0; i<count; i++)
	{
		parse_ck_attribute_msg_malloc((struct ck_attribute_msg *)attr, &templ[i]);
		if (templ[i].ulValueLen) {
			attr = (attr + sizeof(struct ck_attribute_msg) + ((templ[i].ulValueLen -1) & ~3));
		} else {
			attr = (attr + sizeof(struct ck_attribute_msg));
		}
	}

	rv = C_GenerateKey_daemon(hsm, session, &mechanism, templ, count, NULL);
	rv = format_C_GenerateKey_Rsp_msg(rv, rmsg);

	for (i=0; i<count; i++) {
		free_ck_attribute(&templ[i]);
	}
	free(templ);
	return rv;
}

