/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "msg_serialize.h"

#ifndef HTONL
#define HTONL(x) x
#endif

void msg_hdr_serialize(msg_hdr_t *hdr)
{
    hdr->id = HTONL(hdr->id);
    hdr->len = HTONL(hdr->len);
    return;
}

void ck_version_msg_serialize (struct ck_version_msg *msg)
{
	return;
}

void ck_info_msg_serialize (struct ck_info_msg *msg)
{
	ck_version_msg_serialize(&msg->cryptoki_version);
	msg->flags = HTONL(msg->flags);
	ck_version_msg_serialize(&msg->library_version);
	return;
}

void ck_slot_info_msg_serialize (struct ck_slot_info_msg *msg)
{
	msg->flags = HTONL(msg->flags);
	ck_version_msg_serialize(&msg->hardware_version);
	ck_version_msg_serialize(&msg->firmware_version);
	return;
}

void ck_token_info_msg_serialize (struct ck_token_info_msg *msg)
{
	msg->flags = HTONL(msg->flags);
	msg->max_session_count = HTONL(msg->max_session_count);
	msg->session_count = HTONL(msg->session_count);
	msg->max_rw_session_count = HTONL(msg->max_rw_session_count);
	msg->rw_session_count = HTONL(msg->rw_session_count);
	msg->max_pin_len = HTONL(msg->max_pin_len);
	msg->min_pin_len = HTONL(msg->min_pin_len);
	msg->total_public_memory = HTONL(msg->total_public_memory);
	msg->free_public_memory = HTONL(msg->free_public_memory);
	msg->total_private_memory = HTONL(msg->total_private_memory);
	msg->free_private_memory = HTONL(msg->free_private_memory);
	ck_version_msg_serialize(&msg->hardware_version);
	ck_version_msg_serialize(&msg->firmware_version);
	return;
}

void ck_session_info_msg_serialize (struct ck_session_info_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	msg->state = HTONL(msg->state);
	msg->flags = HTONL(msg->flags);
	msg->device_error = HTONL(msg->device_error);
	return;
}

void ck_attribute_msg_serialize (struct ck_attribute_msg *msg)
{
	msg->type = HTONL(msg->type);
	msg->value_len = HTONL(msg->value_len);
	return;
}

void ck_date_msg_serialize (struct ck_date_msg *msg)
{
	return;
}

void ck_mechanism_msg_serialize (struct ck_mechanism_msg *msg)
{
	msg->mechanism = HTONL(msg->mechanism);
	msg->parameter_len = HTONL(msg->parameter_len);
	return;
}

void ck_mechanism_info_msg_serialize (struct ck_mechanism_info_msg *msg)
{
	msg->min_key_size = HTONL(msg->min_key_size);
	msg->max_key_size = HTONL(msg->max_key_size);
	msg->flags = HTONL(msg->flags);
	return;
}

void C_Initialize_Req_msg_serialize (struct C_Initialize_Req_msg *msg)
{
	msg->reserved = HTONL(msg->reserved);
	return;
}

void C_Initialize_Rsp_msg_serialize (struct C_Initialize_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Finalize_Req_msg_serialize (struct C_Finalize_Req_msg *msg)
{
	msg->reserved = HTONL(msg->reserved);
	return;
}

void C_Finalize_Rsp_msg_serialize (struct C_Finalize_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_GetInfo_Req_msg_serialize (struct C_GetInfo_Req_msg *msg)
{
	msg->reserved = HTONL(msg->reserved);
	return;
}

void C_GetInfo_Rsp_msg_serialize (struct C_GetInfo_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	ck_info_msg_serialize(&msg->info);
	return;
}

void C_GetSlotList_Req_msg_serialize (struct C_GetSlotList_Req_msg *msg)
{
	msg->count = HTONL(msg->count);
	return;
}

void C_GetSlotList_Rsp_msg_serialize (struct C_GetSlotList_Rsp_msg *msg)
{
	uint32_t i;

	msg->retval = HTONL(msg->retval);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		msg->slot_list[i] = HTONL(msg->slot_list[i]);
	}
	return;
}

void C_GetSlotInfo_Req_msg_serialize (struct C_GetSlotInfo_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	return;
}

void C_GetSlotInfo_Rsp_msg_serialize (struct C_GetSlotInfo_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	ck_slot_info_msg_serialize(&msg->info);
	return;
}

void C_GetTokenInfo_Req_msg_serialize (struct C_GetTokenInfo_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	return;
}

void C_GetTokenInfo_Rsp_msg_serialize (struct C_GetTokenInfo_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	ck_token_info_msg_serialize(&msg->info);
	return;
}

void C_GetMechanismList_Req_msg_serialize (struct C_GetMechanismList_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	msg->count = HTONL(msg->count);
	return;
}

void C_GetMechanismList_Rsp_msg_serialize (struct C_GetMechanismList_Rsp_msg *msg)
{
	uint32_t i;

	msg->retval = HTONL(msg->retval);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		msg->mechanism_list[i] = HTONL(msg->mechanism_list[i]);
	}
	return;
}

void C_GetMechanismInfo_Req_msg_serialize (struct C_GetMechanismInfo_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	msg->type = HTONL(msg->type);
	return;
}

void C_GetMechanismInfo_Rsp_msg_serialize (struct C_GetMechanismInfo_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	ck_mechanism_info_msg_serialize(&msg->info);
	return;
}

void C_InitToken_Req_msg_serialize (struct C_InitToken_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	msg->pin_len = HTONL(msg->pin_len);
	return;
}

void C_InitToken_Rsp_msg_serialize (struct C_InitToken_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_InitPIN_Req_msg_serialize (struct C_InitPIN_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->pin_len = HTONL(msg->pin_len);
	return;
}

void C_InitPIN_Rsp_msg_serialize (struct C_InitPIN_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_SetPIN_Req_msg_serialize (struct C_SetPIN_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->old_len = HTONL(msg->old_len);
	msg->new_len = HTONL(msg->new_len);
	return;
}

void C_SetPIN_Rsp_msg_serialize (struct C_SetPIN_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_OpenSession_Req_msg_serialize (struct C_OpenSession_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	msg->flags = HTONL(msg->flags);
	return;
}

void C_OpenSession_Rsp_msg_serialize (struct C_OpenSession_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->session = HTONL(msg->session);
	return;
}

void C_CloseSession_Req_msg_serialize (struct C_CloseSession_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	return;
}

void C_CloseSession_Rsp_msg_serialize (struct C_CloseSession_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_CloseAllSessions_Req_msg_serialize (struct C_CloseAllSessions_Req_msg *msg)
{
	msg->slot_id = HTONL(msg->slot_id);
	return;
}

void C_CloseAllSessions_Rsp_msg_serialize (struct C_CloseAllSessions_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_GetSessionInfo_Req_msg_serialize (struct C_GetSessionInfo_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	return;
}

void C_GetSessionInfo_Rsp_msg_serialize (struct C_GetSessionInfo_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	ck_session_info_msg_serialize(&msg->info);
	return;
}

void C_Login_Req_msg_serialize (struct C_Login_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->user_type = HTONL(msg->user_type);
	msg->pin_len = HTONL(msg->pin_len);
	return;
}

void C_Login_Rsp_msg_serialize (struct C_Login_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Logout_Req_msg_serialize (struct C_Logout_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	return;
}

void C_Logout_Rsp_msg_serialize (struct C_Logout_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_CreateObject_Req_msg_serialize (struct C_CreateObject_Req_msg *msg)
{
	uint32_t i;

	msg->session = HTONL(msg->session);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_CreateObject_Rsp_msg_serialize (struct C_CreateObject_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->object = HTONL(msg->object);
	return;
}

void C_DestroyObject_Req_msg_serialize (struct C_DestroyObject_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->object = HTONL(msg->object);
	return;
}

void C_DestroyObject_Rsp_msg_serialize (struct C_DestroyObject_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_GetAttributeValue_Req_msg_serialize (struct C_GetAttributeValue_Req_msg *msg)
{
	uint32_t i;

	msg->session = HTONL(msg->session);
	msg->object = HTONL(msg->object);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_GetAttributeValue_Rsp_msg_serialize (struct C_GetAttributeValue_Rsp_msg *msg)
{
	uint32_t i;

	msg->retval = HTONL(msg->retval);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_SetAttributeValue_Req_msg_serialize (struct C_SetAttributeValue_Req_msg *msg)
{
	uint32_t i;

	msg->session = HTONL(msg->session);
	msg->object = HTONL(msg->object);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_SetAttributeValue_Rsp_msg_serialize (struct C_SetAttributeValue_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_FindObjectsInit_Req_msg_serialize (struct C_FindObjectsInit_Req_msg *msg)
{
	uint32_t i;

	msg->session = HTONL(msg->session);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_FindObjectsInit_Rsp_msg_serialize (struct C_FindObjectsInit_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_FindObjects_Req_msg_serialize (struct C_FindObjects_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->max_object_count = HTONL(msg->max_object_count);
	return;
}

void C_FindObjects_Rsp_msg_serialize (struct C_FindObjects_Rsp_msg *msg)
{
	uint32_t i;

	msg->retval = HTONL(msg->retval);
	msg->object_count = HTONL(msg->object_count);
	for (i=0; i<msg->object_count; i++) {
		msg->object[i] = HTONL(msg->object[i]);
	}
	return;
}

void C_FindObjectsFinal_Req_msg_serialize (struct C_FindObjectsFinal_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	return;
}

void C_FindObjectsFinal_Rsp_msg_serialize (struct C_FindObjectsFinal_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_EncryptInit_Req_msg_serialize (struct C_EncryptInit_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->key = HTONL(msg->key);
	ck_mechanism_msg_serialize(&msg->mechanism);
	return;
}

void C_EncryptInit_Rsp_msg_serialize (struct C_EncryptInit_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Encrypt_Req_msg_serialize (struct C_Encrypt_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->encrypted_data_len = HTONL(msg->encrypted_data_len);
	msg->data_len = HTONL(msg->data_len);
	return;
}

void C_Encrypt_Rsp_msg_serialize (struct C_Encrypt_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->encrypted_data_len = HTONL(msg->encrypted_data_len);
	return;
}

void C_EncryptUpdate_Req_msg_serialize (struct C_EncryptUpdate_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->encrypted_part_len = HTONL(msg->encrypted_part_len);
	msg->part_len = HTONL(msg->part_len);
	return;
}

void C_EncryptUpdate_Rsp_msg_serialize (struct C_EncryptUpdate_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->encrypted_part_len = HTONL(msg->encrypted_part_len);
	return;
}

void C_EncryptFinal_Req_msg_serialize (struct C_EncryptFinal_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->last_encrypted_part_len = HTONL(msg->last_encrypted_part_len);
	return;
}

void C_EncryptFinal_Rsp_msg_serialize (struct C_EncryptFinal_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->last_encrypted_part_len = HTONL(msg->last_encrypted_part_len);
	return;
}

void C_DecryptInit_Req_msg_serialize (struct C_DecryptInit_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->key = HTONL(msg->key);
	ck_mechanism_msg_serialize(&msg->mechanism);
	return;
}

void C_DecryptInit_Rsp_msg_serialize (struct C_DecryptInit_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Decrypt_Req_msg_serialize (struct C_Decrypt_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->data_len = HTONL(msg->data_len);
	msg->encrypted_data_len = HTONL(msg->encrypted_data_len);
	return;
}

void C_Decrypt_Rsp_msg_serialize (struct C_Decrypt_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->data_len = HTONL(msg->data_len);
	return;
}

void C_DecryptUpdate_Req_msg_serialize (struct C_DecryptUpdate_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->part_len = HTONL(msg->part_len);
	msg->encrypted_part_len = HTONL(msg->encrypted_part_len);
	return;
}

void C_DecryptUpdate_Rsp_msg_serialize (struct C_DecryptUpdate_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->part_len = HTONL(msg->part_len);
	return;
}

void C_DecryptFinal_Req_msg_serialize (struct C_DecryptFinal_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->last_part_len = HTONL(msg->last_part_len);
	return;
}

void C_DecryptFinal_Rsp_msg_serialize (struct C_DecryptFinal_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->last_part_len = HTONL(msg->last_part_len);
	return;
}

void C_SignInit_Req_msg_serialize (struct C_SignInit_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->key = HTONL(msg->key);
	ck_mechanism_msg_serialize(&msg->mechanism);
	return;
}

void C_SignInit_Rsp_msg_serialize (struct C_SignInit_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Sign_Req_msg_serialize (struct C_Sign_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->signature_len = HTONL(msg->signature_len);
	msg->data_len = HTONL(msg->data_len);
	return;
}

void C_Sign_Rsp_msg_serialize (struct C_Sign_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->signature_len = HTONL(msg->signature_len);
	return;
}

void C_SignUpdate_Req_msg_serialize (struct C_SignUpdate_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->part_len = HTONL(msg->part_len);
	return;
}

void C_SignUpdate_Rsp_msg_serialize (struct C_SignUpdate_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_SignFinal_Req_msg_serialize (struct C_SignFinal_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->signature_len = HTONL(msg->signature_len);
	return;
}

void C_SignFinal_Rsp_msg_serialize (struct C_SignFinal_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	msg->signature_len = HTONL(msg->signature_len);
	return;
}

void C_VerifyInit_Req_msg_serialize (struct C_VerifyInit_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->key = HTONL(msg->key);
	ck_mechanism_msg_serialize(&msg->mechanism);
	return;
}

void C_VerifyInit_Rsp_msg_serialize (struct C_VerifyInit_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_Verify_Req_msg_serialize (struct C_Verify_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->signature_len = HTONL(msg->signature_len);
	msg->data_len = HTONL(msg->data_len);
	return;
}

void C_Verify_Rsp_msg_serialize (struct C_Verify_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_VerifyUpdate_Req_msg_serialize (struct C_VerifyUpdate_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->part_len = HTONL(msg->part_len);
	return;
}

void C_VerifyUpdate_Rsp_msg_serialize (struct C_VerifyUpdate_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_VerifyFinal_Req_msg_serialize (struct C_VerifyFinal_Req_msg *msg)
{
	msg->session = HTONL(msg->session);
	msg->signature_len = HTONL(msg->signature_len);
	return;
}

void C_VerifyFinal_Rsp_msg_serialize (struct C_VerifyFinal_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}

void C_GenerateKey_Req_msg_serialize (struct C_GenerateKey_Req_msg *msg)
{
	uint32_t i;
	msg->session = HTONL(msg->session);
	msg->count = HTONL(msg->count);
	for (i=0; i<msg->count; i++) {
		ck_attribute_msg_serialize(&msg->templ[i]);
	}
	return;
}

void C_GenerateKey_Rsp_msg_serialize (struct C_GenerateKey_Rsp_msg *msg)
{
	msg->retval = HTONL(msg->retval);
	return;
}


