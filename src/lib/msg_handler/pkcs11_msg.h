/* pkcs11_msg.h modified copy of pkcs11.h
   Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
   Copyright 2006, 2007 g10 Code GmbH
   Copyright 2006 Andreas Jellinghaus

   This file is free software; as a special exception the author gives
   unlimited permission to copy and/or distribute it, with or without
   modifications, as long as this notice is preserved.

   This file is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY, to the extent permitted by law; without even
   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
   PURPOSE.  */

#ifndef PKCS11_MSG_H
#define PKCS11_MSG_H

#include <stdint.h>
#include <pkcs11.h>

#pragma pack(push, msg, 1)

struct ck_version_msg
{
  uint8_t major;
  uint8_t minor;
};

struct ck_info_msg
{
  struct ck_version_msg cryptoki_version;
  uint8_t manufacturer_id[32];
  uint32_t flags;
  uint8_t library_description[32];
  struct ck_version_msg library_version;
};

struct ck_slot_info_msg
{
  uint8_t slot_description[64];
  uint8_t manufacturer_id[32];
  uint32_t flags;
  struct ck_version_msg hardware_version;
  struct ck_version_msg firmware_version;
};

struct ck_token_info_msg
{
  uint8_t label[32];
  uint8_t manufacturer_id[32];
  uint8_t model[16];
  uint8_t serial_number[16];
  uint32_t flags;
  uint32_t max_session_count;
  uint32_t session_count;
  uint32_t max_rw_session_count;
  uint32_t rw_session_count;
  uint32_t max_pin_len;
  uint32_t min_pin_len;
  uint32_t total_public_memory;
  uint32_t free_public_memory;
  uint32_t total_private_memory;
  uint32_t free_private_memory;
  struct ck_version_msg hardware_version;
  struct ck_version_msg firmware_version;
  uint8_t utc_time[16];
};

struct ck_session_info_msg
{
  uint32_t slot_id;
  uint32_t state;
  uint32_t flags;
  uint32_t device_error;
};

struct ck_attribute_msg
{
  uint32_t  type;
  uint32_t  value_len;
  uint8_t   value[4];
};

struct ck_date_msg
{
  uint8_t year[4];
  uint8_t month[2];
  uint8_t day[2];
};

struct ck_mechanism_msg
{
  uint32_t mechanism;
  uint32_t parameter_len;
  uint8_t  parameter[1];
};

struct ck_mechanism_info_msg
{
  uint32_t min_key_size;
  uint32_t max_key_size;
  uint32_t flags;
};

struct C_Initialize_Req_msg 
{
    uint32_t    reserved;
};

struct C_Initialize_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Finalize_Req_msg 
{
    uint32_t    reserved;
};

struct C_Finalize_Rsp_msg 
{
    uint32_t    retval;
};

struct C_GetInfo_Req_msg 
{
    uint32_t    reserved;
};

struct C_GetInfo_Rsp_msg 
{
    uint32_t        retval;
    struct ck_info_msg  info;
};

struct C_GetSlotList_Req_msg 
{
    uint8_t     token_present;
    uint32_t    count;
};

struct C_GetSlotList_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    count;
    uint32_t    slot_list[1];
};

struct C_GetSlotInfo_Req_msg 
{
    uint32_t    slot_id;
};

struct C_GetSlotInfo_Rsp_msg 
{
    uint32_t    retval;
    struct ck_slot_info_msg info;
};

struct C_GetTokenInfo_Req_msg 
{
    uint32_t    slot_id;
};

struct C_GetTokenInfo_Rsp_msg 
{
    uint32_t    retval;
    struct ck_token_info_msg    info;
};

struct C_GetMechanismList_Req_msg 
{
    uint32_t    slot_id;
    uint32_t    count;
};

struct C_GetMechanismList_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    count;
    uint32_t    mechanism_list[1];
};

struct C_GetMechanismInfo_Req_msg 
{
    uint32_t    slot_id;
    uint32_t    type;
};

struct C_GetMechanismInfo_Rsp_msg 
{
    uint32_t                    retval;
    struct ck_mechanism_info_msg    info;
};

struct C_InitToken_Req_msg 
{
    uint32_t    slot_id;
    uint8_t     label[32];
    uint32_t    pin_len;
    uint8_t     pin[1];
};

struct C_InitToken_Rsp_msg 
{
    uint32_t    retval;
};

struct C_InitPIN_Req_msg 
{
    uint32_t    session;
    uint32_t    pin_len;
    uint8_t     pin[1];
};

struct C_InitPIN_Rsp_msg 
{
    uint32_t    retval;
};

struct C_SetPIN_Req_msg 
{
    uint32_t    session;
    uint32_t    old_len;
    uint32_t    new_len;
    uint8_t     old_pin[1];
    uint8_t     new_pin[1];
};

struct C_SetPIN_Rsp_msg 
{
    uint32_t    retval;
};

struct C_OpenSession_Req_msg 
{
    uint32_t slot_id;
    uint32_t flags;
};

struct C_OpenSession_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    session;
};

struct C_CloseSession_Req_msg 
{
    uint32_t    session;
};

struct C_CloseSession_Rsp_msg 
{
    uint32_t    retval;
};

struct C_CloseAllSessions_Req_msg 
{
    uint32_t    slot_id;
};

struct C_CloseAllSessions_Rsp_msg 
{
    uint32_t    retval;
};

struct C_GetSessionInfo_Req_msg 
{
    uint32_t    session;
};

struct C_GetSessionInfo_Rsp_msg 
{
    uint32_t                retval;
    struct ck_session_info_msg  info;
};

struct C_Login_Req_msg 
{
    uint32_t    session;
    uint32_t    user_type;
    uint32_t    pin_len;
    uint8_t     pin[1];
};

struct C_Login_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Logout_Req_msg 
{
    uint32_t    session;
};

struct C_Logout_Rsp_msg 
{
    uint32_t    retval;
};

struct C_CreateObject_Req_msg 
{
    uint32_t    session;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_CreateObject_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    object;
};

struct C_DestroyObject_Req_msg 
{
    uint32_t    session;
    uint32_t    object;
};

struct C_DestroyObject_Rsp_msg 
{
    uint32_t    retval;
};

struct C_GetAttributeValue_Req_msg 
{
    uint32_t    session;
    uint32_t    object;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_GetAttributeValue_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_SetAttributeValue_Req_msg 
{
    uint32_t    session;
    uint32_t    object;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_SetAttributeValue_Rsp_msg 
{
    uint32_t    retval;
};

struct C_FindObjectsInit_Req_msg 
{
    uint32_t    session;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_FindObjectsInit_Rsp_msg 
{
    uint32_t    retval;
};

struct C_FindObjects_Req_msg 
{
    uint32_t    session;
    uint32_t    max_object_count;
};

struct C_FindObjects_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    object_count;
    uint32_t    object[1];
};

struct C_FindObjectsFinal_Req_msg 
{
    uint32_t    session;
};

struct C_FindObjectsFinal_Rsp_msg 
{
    uint32_t    retval;
};

struct C_EncryptInit_Req_msg 
{
    uint32_t    session;
    uint32_t    key;
    struct ck_mechanism_msg mechanism;
};

struct C_EncryptInit_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Encrypt_Req_msg 
{
    uint32_t    session;
    uint32_t    encrypted_data_len;
    uint32_t    data_len;
    uint8_t     data[1];
};

struct C_Encrypt_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    encrypted_data_len;
    uint8_t     encrypted_data[1];
};

struct C_EncryptUpdate_Req_msg 
{
    uint32_t    session;
    uint32_t    encrypted_part_len;
    uint32_t    part_len;
    uint8_t     part[1];
};

struct C_EncryptUpdate_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    encrypted_part_len;
    uint8_t     encrypted_part[1];
};

struct C_EncryptFinal_Req_msg 
{
    uint32_t    session;
    uint32_t    last_encrypted_part_len;
};

struct C_EncryptFinal_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    last_encrypted_part_len;
    uint8_t     last_encrypted_part[1];
};

struct C_DecryptInit_Req_msg 
{
    uint32_t    session;
    uint32_t    key;
    struct ck_mechanism_msg mechanism;
};

struct C_DecryptInit_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Decrypt_Req_msg 
{
    uint32_t    session;
    uint32_t    data_len;
    uint32_t    encrypted_data_len;
    uint8_t     encrypted_data[1];
};

struct C_Decrypt_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    data_len;
    uint8_t     data[1];
};

struct C_DecryptUpdate_Req_msg 
{
    uint32_t    session;
    uint32_t    part_len;
    uint32_t    encrypted_part_len;
    uint8_t     encrypted_part[1];
};

struct C_DecryptUpdate_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    part_len;
    uint8_t     part[1];
};

struct C_DecryptFinal_Req_msg 
{
    uint32_t    session;
    uint32_t    last_part_len;
};

struct C_DecryptFinal_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    last_part_len;
    uint8_t     last_part[1];
};

struct C_SignInit_Req_msg 
{
    uint32_t    session;
    uint32_t    key;
    struct ck_mechanism_msg mechanism;
};

struct C_SignInit_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Sign_Req_msg 
{
    uint32_t    session;
    uint32_t    signature_len;
    uint32_t    data_len;
    uint8_t     data[1];
};

struct C_Sign_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    signature_len;
    uint8_t     signature[1];
};

struct C_SignUpdate_Req_msg 
{
    uint32_t    session;
    uint32_t    part_len;
    uint8_t     part[1];
};

struct C_SignUpdate_Rsp_msg 
{
    uint32_t    retval;
};

struct C_SignFinal_Req_msg 
{
    uint32_t    session;
    uint32_t    signature_len;
};

struct C_SignFinal_Rsp_msg 
{
    uint32_t    retval;
    uint32_t    signature_len;
    uint8_t     signature[1];
};

struct C_VerifyInit_Req_msg 
{
    uint32_t    session;
    uint32_t    key;
    struct ck_mechanism_msg mechanism;
};

struct C_VerifyInit_Rsp_msg 
{
    uint32_t    retval;
};

struct C_Verify_Req_msg 
{
    uint32_t    session;
    uint32_t    signature_len;
    uint32_t    data_len;
    uint8_t     signature[1];
    uint8_t     data[1];
};

struct C_Verify_Rsp_msg 
{
    uint32_t    retval;
};

struct C_VerifyUpdate_Req_msg 
{
    uint32_t    session;
    uint32_t    part_len;
    uint8_t     part[1];
};

struct C_VerifyUpdate_Rsp_msg 
{
    uint32_t    retval;
};

struct C_VerifyFinal_Req_msg 
{
    uint32_t    session;
    uint32_t    signature_len;
    uint8_t     signature[1];
};

struct C_VerifyFinal_Rsp_msg 
{
    uint32_t    retval;
};

struct C_GenError_Rsp_msg 
{
    uint32_t    errorval;
};



struct C_GenerateKey_Req_msg
{
    uint32_t    session;
    uint32_t    count;
    struct ck_attribute_msg templ[1];
};

struct C_GenerateKey_Rsp_msg
{
    uint32_t    retval;
};
/* Message identifiers */
enum msg_id 
{
    MSG_ID_C_Initialize_Req = 1,
    MSG_ID_C_Finalize_Req,
    MSG_ID_C_GetInfo_Req,
    MSG_ID_C_GetSlotList_Req,
    MSG_ID_C_GetSlotInfo_Req,
    MSG_ID_C_GetTokenInfo_Req,
    MSG_ID_C_GetMechanismList_Req,
    MSG_ID_C_GetMechanismInfo_Req,
    MSG_ID_C_InitToken_Req,
    MSG_ID_C_InitPIN_Req,
    MSG_ID_C_SetPIN_Req,
    MSG_ID_C_OpenSession_Req,
    MSG_ID_C_CloseSession_Req,
    MSG_ID_C_CloseAllSessions_Req,
    MSG_ID_C_GetSessionInfo_Req,
    MSG_ID_C_Login_Req,
    MSG_ID_C_Logout_Req,
    MSG_ID_C_CreateObject_Req,
    MSG_ID_C_DestroyObject_Req,
    MSG_ID_C_GetAttributeValue_Req,
    MSG_ID_C_SetAttributeValue_Req,
    MSG_ID_C_FindObjectsInit_Req,
    MSG_ID_C_FindObjects_Req,
    MSG_ID_C_FindObjectsFinal_Req,
    MSG_ID_C_EncryptInit_Req,
    MSG_ID_C_Encrypt_Req,
    MSG_ID_C_EncryptUpdate_Req,
    MSG_ID_C_EncryptFinal_Req,
    MSG_ID_C_DecryptInit_Req,
    MSG_ID_C_Decrypt_Req,
    MSG_ID_C_DecryptUpdate_Req,
    MSG_ID_C_DecryptFinal_Req,
    MSG_ID_C_SignInit_Req,
    MSG_ID_C_Sign_Req,
    MSG_ID_C_SignUpdate_Req,
    MSG_ID_C_SignFinal_Req,
    MSG_ID_C_VerifyInit_Req,
    MSG_ID_C_Verify_Req,
    MSG_ID_C_VerifyUpdate_Req,
    MSG_ID_C_VerifyFinal_Req,
    MSG_ID_C_GenerateKey_Req,
    MSG_ID_C_EndOfMsgId_Req,

    MSG_ID_C_Initialize_Rsp = 200,
    MSG_ID_C_Finalize_Rsp,
    MSG_ID_C_GetInfo_Rsp,
    MSG_ID_C_GetSlotList_Rsp,
    MSG_ID_C_GetSlotInfo_Rsp,
    MSG_ID_C_GetTokenInfo_Rsp,
    MSG_ID_C_GetMechanismList_Rsp,
    MSG_ID_C_GetMechanismInfo_Rsp,
    MSG_ID_C_InitToken_Rsp,
    MSG_ID_C_InitPIN_Rsp,
    MSG_ID_C_SetPIN_Rsp,
    MSG_ID_C_OpenSession_Rsp,
    MSG_ID_C_CloseSession_Rsp,
    MSG_ID_C_CloseAllSessions_Rsp,
    MSG_ID_C_GetSessionInfo_Rsp,
    MSG_ID_C_Login_Rsp,
    MSG_ID_C_Logout_Rsp,
    MSG_ID_C_CreateObject_Rsp,
    MSG_ID_C_DestroyObject_Rsp,
    MSG_ID_C_GetAttributeValue_Rsp,
    MSG_ID_C_SetAttributeValue_Rsp,
    MSG_ID_C_FindObjectsInit_Rsp,
    MSG_ID_C_FindObjects_Rsp,
    MSG_ID_C_FindObjectsFinal_Rsp,
    MSG_ID_C_EncryptInit_Rsp,
    MSG_ID_C_Encrypt_Rsp,
    MSG_ID_C_EncryptUpdate_Rsp,
    MSG_ID_C_EncryptFinal_Rsp,
    MSG_ID_C_DecryptInit_Rsp,
    MSG_ID_C_Decrypt_Rsp,
    MSG_ID_C_DecryptUpdate_Rsp,
    MSG_ID_C_DecryptFinal_Rsp,
    MSG_ID_C_SignInit_Rsp,
    MSG_ID_C_Sign_Rsp,
    MSG_ID_C_SignUpdate_Rsp,
    MSG_ID_C_SignFinal_Rsp,
    MSG_ID_C_VerifyInit_Rsp,
    MSG_ID_C_Verify_Rsp,
    MSG_ID_C_VerifyUpdate_Rsp,
    MSG_ID_C_VerifyFinal_Rsp,
    MSG_ID_C_GenError_Rsp,
    MSG_ID_C_GenerateKey_Rsp
};

union MSG_BODY 
{
    struct C_Initialize_Req_msg         C_Initialize_Req;
    struct C_Finalize_Req_msg           C_Finalize_Req;
    struct C_GetInfo_Req_msg            C_GetInfo_Req;
    struct C_GetSlotList_Req_msg        C_GetSlotList_Req;
    struct C_GetSlotInfo_Req_msg        C_GetSlotInfo_Req;
    struct C_GetTokenInfo_Req_msg       C_GetTokenInfo_Req;
    struct C_GetMechanismList_Req_msg   C_GetMechanismList_Req;
    struct C_GetMechanismInfo_Req_msg   C_GetMechanismInfo_Req;
    struct C_InitToken_Req_msg          C_InitToken_Req;
    struct C_InitPIN_Req_msg            C_InitPIN_Req;
    struct C_SetPIN_Req_msg             C_SetPIN_Req;
    struct C_OpenSession_Req_msg        C_OpenSession_Req;
    struct C_CloseSession_Req_msg       C_CloseSession_Req;
    struct C_CloseAllSessions_Req_msg   C_CloseAllSessions_Req;
    struct C_GetSessionInfo_Req_msg     C_GetSessionInfo_Req;
    struct C_Login_Req_msg              C_Login_Req;
    struct C_Logout_Req_msg             C_Logout_Req;
    struct C_CreateObject_Req_msg       C_CreateObject_Req;
    struct C_DestroyObject_Req_msg      C_DestroyObject_Req;
    struct C_GetAttributeValue_Req_msg  C_GetAttributeValue_Req;
    struct C_SetAttributeValue_Req_msg  C_SetAttributeValue_Req;
    struct C_FindObjectsInit_Req_msg    C_FindObjectsInit_Req;
    struct C_FindObjects_Req_msg        C_FindObjects_Req;
    struct C_FindObjectsFinal_Req_msg   C_FindObjectsFinal_Req;
    struct C_EncryptInit_Req_msg        C_EncryptInit_Req;
    struct C_Encrypt_Req_msg            C_Encrypt_Req;
    struct C_EncryptUpdate_Req_msg      C_EncryptUpdate_Req;
    struct C_EncryptFinal_Req_msg       C_EncryptFinal_Req;
    struct C_DecryptInit_Req_msg        C_DecryptInit_Req;
    struct C_Decrypt_Req_msg            C_Decrypt_Req;
    struct C_DecryptUpdate_Req_msg      C_DecryptUpdate_Req;
    struct C_DecryptFinal_Req_msg       C_DecryptFinal_Req;
    struct C_SignInit_Req_msg           C_SignInit_Req;
    struct C_Sign_Req_msg               C_Sign_Req;
    struct C_SignUpdate_Req_msg         C_SignUpdate_Req;
    struct C_SignFinal_Req_msg          C_SignFinal_Req;
    struct C_VerifyInit_Req_msg         C_VerifyInit_Req;
    struct C_Verify_Req_msg             C_Verify_Req;
    struct C_VerifyUpdate_Req_msg       C_VerifyUpdate_Req;
    struct C_VerifyFinal_Req_msg        C_VerifyFinal_Req;
    struct C_GenerateKey_Req_msg        C_GenerateKey_Req;

    struct C_Initialize_Rsp_msg         C_Initialize_Rsp;
    struct C_Finalize_Rsp_msg           C_Finalize_Rsp;
    struct C_GetInfo_Rsp_msg            C_GetInfo_Rsp;
    struct C_GetSlotList_Rsp_msg        C_GetSlotList_Rsp;
    struct C_GetSlotInfo_Rsp_msg        C_GetSlotInfo_Rsp;
    struct C_GetTokenInfo_Rsp_msg       C_GetTokenInfo_Rsp;
    struct C_GetMechanismList_Rsp_msg   C_GetMechanismList_Rsp;
    struct C_GetMechanismInfo_Rsp_msg   C_GetMechanismInfo_Rsp;
    struct C_InitToken_Rsp_msg          C_InitToken_Rsp;
    struct C_InitPIN_Rsp_msg            C_InitPIN_Rsp;
    struct C_SetPIN_Rsp_msg             C_SetPIN_Rsp;
    struct C_OpenSession_Rsp_msg        C_OpenSession_Rsp;
    struct C_CloseSession_Rsp_msg       C_CloseSession_Rsp;
    struct C_CloseAllSessions_Rsp_msg   C_CloseAllSessions_Rsp;
    struct C_GetSessionInfo_Rsp_msg     C_GetSessionInfo_Rsp;
    struct C_Login_Rsp_msg              C_Login_Rsp;
    struct C_Logout_Rsp_msg             C_Logout_Rsp;
    struct C_CreateObject_Rsp_msg       C_CreateObject_Rsp;
    struct C_DestroyObject_Rsp_msg      C_DestroyObject_Rsp;
    struct C_GetAttributeValue_Rsp_msg  C_GetAttributeValue_Rsp;
    struct C_SetAttributeValue_Rsp_msg  C_SetAttributeValue_Rsp;
    struct C_FindObjectsInit_Rsp_msg    C_FindObjectsInit_Rsp;
    struct C_FindObjects_Rsp_msg        C_FindObjects_Rsp;
    struct C_FindObjectsFinal_Rsp_msg   C_FindObjectsFinal_Rsp;
    struct C_EncryptInit_Rsp_msg        C_EncryptInit_Rsp;
    struct C_Encrypt_Rsp_msg            C_Encrypt_Rsp;
    struct C_EncryptUpdate_Rsp_msg      C_EncryptUpdate_Rsp;
    struct C_EncryptFinal_Rsp_msg       C_EncryptFinal_Rsp;
    struct C_DecryptInit_Rsp_msg        C_DecryptInit_Rsp;
    struct C_Decrypt_Rsp_msg            C_Decrypt_Rsp;
    struct C_DecryptUpdate_Rsp_msg      C_DecryptUpdate_Rsp;
    struct C_DecryptFinal_Rsp_msg       C_DecryptFinal_Rsp;
    struct C_SignInit_Rsp_msg           C_SignInit_Rsp;
    struct C_Sign_Rsp_msg               C_Sign_Rsp;
    struct C_SignUpdate_Rsp_msg         C_SignUpdate_Rsp;
    struct C_SignFinal_Rsp_msg          C_SignFinal_Rsp;
    struct C_VerifyInit_Rsp_msg         C_VerifyInit_Rsp;
    struct C_Verify_Rsp_msg             C_Verify_Rsp;
    struct C_VerifyUpdate_Rsp_msg       C_VerifyUpdate_Rsp;
    struct C_VerifyFinal_Rsp_msg        C_VerifyFinal_Rsp;
    struct C_GenError_Rsp_msg		    C_GenError_Rsp;
    struct C_GenerateKey_Rsp_msg        C_GenerateKey_Rsp;
};

typedef struct 
{
    /* Message identifier MSG_ID_* */
    enum msg_id     id;
    /* len of the message in bytes excluding this header */
    uint32_t        len;
} msg_hdr_t;

typedef struct 
{
    msg_hdr_t       hdr;
    union MSG_BODY  body;
} msg_t;

#pragma pack(pop, msg)

#endif /* PKCS11_MSG_H */

