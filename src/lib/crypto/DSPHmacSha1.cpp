/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"
#include "DSPHmacSha1.h"
#include <stdint.h>
#include "log.h"

DSPHmacSha1::DSPHmacSha1() {
    m_dspIf = NULL;
}

DSPHmacSha1::~DSPHmacSha1() {
    delete m_dspIf;
}

int DSPHmacSha1::getHashSize() {
    return 20;
}

bool DSPHmacSha1::hashInit()
{
    DEBUG_MSG("DSPHmacSha1::hashInit()");
    m_dspIf = new SecureDSP();
    return (m_dspIf != NULL);
}

bool DSPHmacSha1::hashUpdate(const ByteString& data)
{
    DEBUG_MSG("DSPHmacSha1::hashUpdate(), length of data: %u", data.size());
    return m_dspIf->hashUpdate(data);
}

bool DSPHmacSha1::hashFinal(ByteString& hashedData)
{
    DEBUG_MSG("DSPHmacSha1::hashFinal()");
    bool ret = m_dspIf->hashFinal(hashedData);
    DEBUG_MSG("DPHmacSha1::hashFinal(), len %u", hashedData.size());
    return ret;
}



