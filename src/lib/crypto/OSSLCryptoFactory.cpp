/* $Id: OSSLCryptoFactory.cpp 4922 2010-11-22 12:25:09Z rb $ */

/*
 * Copyright (c) 2010 SURFnet bv
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************
 OSSLCryptoFactory.cpp

 This is an OpenSSL based cryptographic algorithm factory
 *****************************************************************************/

#include "config.h"
#include "OSSLCryptoFactory.h"
#include "OSSLRNG.h"
#include "OSSLAES.h"
#include "OSSLDES.h"
#include "OSSLMD5.h"
#include "OSSLSHA1.h"
#include "OSSLSHA256.h"
#include "OSSLSHA512.h"
#include "OSSLRSA.h"
#include "OSSLDSA.h"
#include "Configuration.h"
#include "MemMgr.h"
#include "DSPHmacSha1.h"

#include <algorithm>
#include <string.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

// Initialise the one-and-only instance
std::auto_ptr<OSSLCryptoFactory> OSSLCryptoFactory::instance(NULL); 

void* imalloc(size_t sz)
{
	void* p = MemMgr::i()->alloc(sz);
	DEBUG_MSG("im:(%p, %d)", p, sz);
	return p;
}

void* irealloc(void *p, size_t sz)
{
	void *rp = MemMgr::i()->alloc(sz);
	DEBUG_MSG("ir:(%p, %p, %d)", p, rp, sz);
	memcpy(rp, p, sz);
	MemMgr::i()->free(p);
	return rp;
}

void ifree(void *p)
{
	DEBUG_MSG("if:(%p)", p);
	MemMgr::i()->free(p);
}

// Constructor
OSSLCryptoFactory::OSSLCryptoFactory()
{

	std::string heapStr;
	heapStr = Configuration::i()->getString("secstore.osslheap.internal", DEFAULT_OSSL_HEAP_FLAG);

	if (!heapStr.compare("yes")) {
		INFO_MSG("using internal memory for OSSL heap");	
		internalHeap = true;
	} else {
		INFO_MSG("NOT using internal memory for OSSL heap");	
		internalHeap = false;
	}

	// Replace OpenSSL memory allocator
	if (internalHeap) {
	    if (!MemMgr::i()->isValid())
	    {
		    ERROR_MSG("MemMgr instance not valid");
	    }

		if (!CRYPTO_set_mem_functions(imalloc, irealloc, ifree)) {
			ERROR_MSG("CRYPTO_set_mem_functions failed");
		}
	}

	// Initialise OpenSSL
	OpenSSL_add_all_algorithms();

	// Initialise the one-and-only RNG
	rng = new OSSLRNG();
}

// Destructor
OSSLCryptoFactory::~OSSLCryptoFactory()
{
	// Destroy the one-and-only RNG
	delete rng;

	// Clean up OpenSSL
	ERR_remove_state(0);
	EVP_cleanup();
	CRYPTO_cleanup_all_ex_data();
}

// Return the one-and-only instance
OSSLCryptoFactory* OSSLCryptoFactory::i()
{
	if (!instance.get())
	{
		instance = std::auto_ptr<OSSLCryptoFactory>(new OSSLCryptoFactory());
	}

	return instance.get();
}

// Create a concrete instance of a symmetric algorithm
SymmetricAlgorithm* OSSLCryptoFactory::getSymmetricAlgorithm(std::string algorithm)
{
	std::string lcAlgo;
	lcAlgo.resize(algorithm.size());
	std::transform(algorithm.begin(), algorithm.end(), lcAlgo.begin(), tolower);

	if (!lcAlgo.compare("aes"))
	{
		return new OSSLAES();
	}
	else if (!lcAlgo.compare("des") || !lcAlgo.compare("3des"))
	{
		return new OSSLDES();
	}
	else 
	{
		// No algorithm implementation is available
		ERROR_MSG("Unknown algorithm '%s'", lcAlgo.c_str());

		return NULL;
	}
}

// Create a concrete instance of an asymmetric algorithm
AsymmetricAlgorithm* OSSLCryptoFactory::getAsymmetricAlgorithm(std::string algorithm)
{
	std::string lcAlgo;
	lcAlgo.resize(algorithm.size());
	std::transform(algorithm.begin(), algorithm.end(), lcAlgo.begin(), tolower);

	if (!lcAlgo.compare("rsa"))
	{
		return new OSSLRSA();
	}
	else if (!lcAlgo.compare("dsa"))
	{
		return new OSSLDSA();
	}
	else
	{
		// No algorithm implementation is available
		ERROR_MSG("Unknown algorithm '%s'", algorithm.c_str());

		return NULL;
	}
}

// Create a concrete instance of a hash algorithm
HashAlgorithm* OSSLCryptoFactory::getHashAlgorithm(std::string algorithm)
{
	std::string lcAlgo;
	lcAlgo.resize(algorithm.size());
	std::transform(algorithm.begin(), algorithm.end(), lcAlgo.begin(), tolower);

	if (!lcAlgo.compare("md5"))
	{
		return new OSSLMD5();
	}
	else if (!lcAlgo.compare("sha1"))
	{
		return new OSSLSHA1();
	}
	else if (!lcAlgo.compare("sha256"))
	{
		return new OSSLSHA256();
	}
	else if (!lcAlgo.compare("sha512"))
	{
		return new OSSLSHA512();
	}
	else if (!lcAlgo.compare("hmacsha1"))
	{
		return new DSPHmacSha1();
	}
	else
	{
		// No algorithm implementation is available
		ERROR_MSG("Unknown algorithm '%s'", algorithm.c_str());

		return NULL;
	}

	// No algorithm implementation is available
	return NULL;
}

// Get the global RNG (may be an unique RNG per thread)
RNG* OSSLCryptoFactory::getRNG(std::string name /* = "default" */)
{
	std::string lcAlgo;
	lcAlgo.resize(name.size());
	std::transform(name.begin(), name.end(), lcAlgo.begin(), tolower);

	if (!lcAlgo.compare("default"))
	{
		return rng;
	}
	else
	{
		// No algorithm implementation is available
		ERROR_MSG("Unknown algorithm '%s'", name.c_str());

		return NULL;
	}
}

