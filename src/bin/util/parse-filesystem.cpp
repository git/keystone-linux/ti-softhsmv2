/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* This file parses and prints content of the file system binary blob */

#include <stdint.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>

using namespace std;

#pragma pack(push, file_data, 1)
typedef struct hdr {
	uint32_t	magic;
	uint32_t	tag;
	uint32_t	size;
} hdr_t;

#define DBSTORE_MAX_NAME_LENGTH 256
//#define DBSTORE_MAX_NAME_LENGTH 64

typedef struct node {
	uint32_t	type;
	uint32_t	name_len;
	char		name[DBSTORE_MAX_NAME_LENGTH];
	uint32_t	data_len;
	uint32_t	alloc_len;
} node_t;
#pragma pack(pop, file_data)

int main(int argc, char *argv[])
{
	ifstream fs;
	hdr_t hdr;
	node_t node;
	struct stat results;
	int filesize;
	int offset = 0;
	int index = 0;

	if (argc < 2) return -1;

	if (!stat(argv[1], &results) == 0) return -1;

	cout << "Size of file " << argv[1] << " is ";
	cout <<results.st_size << endl << endl;
	cout << "Hdr size " << sizeof(hdr_t) << endl;
	cout << "Node size " << sizeof(node_t);
	cout << ", Max name length " << DBSTORE_MAX_NAME_LENGTH << endl<< endl;

	if (results.st_size < sizeof(hdr_t)) {
		cout << "Size of file is smaller than header" << endl;
		return -1;
	}

	filesize = results.st_size;

	fs.open (argv[1], ios::binary | ios::in);

	if (!fs.is_open()) {
		cout << "File open failed for " << argv[1] << endl;
		return -1;
	}

	fs.read ((char *) &hdr, sizeof(hdr_t));
	offset += sizeof(hdr_t);

	cout << "Magic:\t" << hex << hdr.magic << dec << endl;
	cout << "Tag:\t" << hdr.tag << endl;
	cout << "Size:\t" << hdr.size << endl << endl;

	if (hdr.size > filesize) {
		cout << "Filesystem corrupted:";
		cout << " file size is greater than filesystem size" << endl;
		return -1;
	}

	if (hdr.size <= offset) return 0;

	while (1) {
		if (offset + sizeof(node_t) > hdr.size)
			return -1;

		fs.read ((char *) &node, sizeof(node_t));
		offset += sizeof(node_t);

		cout << "Node Index\t" << index << endl;
		cout << "\tType:\t" << node.type << endl;
		if (node.type != 1 && node.type != 2) {
			cout << "Filesystem corrupted at offset " << offset << endl;
			return -1;
		}
		cout << "\tName Length:\t" << node.name_len << endl;
		if (node.name_len > DBSTORE_MAX_NAME_LENGTH) {
			cout << "Filesystem corrupted at offset " << offset << endl;
			return -1;
		}
		cout << "\tName:\t";
		cout.width(node.name_len);
		cout << left << node.name << endl;
		cout.width();
		cout << "\tData Length:\t" << node.data_len << endl;
		cout << "\tAlloc Length:\t" << node.alloc_len << endl;
		index++;
		if (offset + node.alloc_len > hdr.size)
			return -1;
	}

	fs.close();
	return 0;
}
