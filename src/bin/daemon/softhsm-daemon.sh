#! /bin/sh

softhsm_daemon=/usr/bin/softhsm-daemon

test -x "$softhsm_daemon" || exit 0

case "$1" in
  start)
    echo -n "Starting softhsm daemon"
    start-stop-daemon --start --quiet --exec $softhsm_daemon
    echo "."
    ;;
  stop)
    echo -n "Stopping softhsm daemon"
    start-stop-daemon --stop --quiet --pidfile /var/run/softhsm-daemon.pid
    echo "."
    ;;
  *)
    echo "Usage: /etc/init.d/softhsm-daemon.sh {start|stop}"
    exit 1
esac

exit 0
