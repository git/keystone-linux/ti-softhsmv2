#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <log.h>

#include <libdaemon/daemon.h>

using namespace std;

#include "config.h"
#include "log.h"
#include "Configuration.h"
#include "SimpleConfigLoader.h"
#include "UnixDatagramSocket.h"
#include "softhsm-daemon.h"

#include "pkcs11_msg_daemon.h"

#define ERRNOSTR(x) x.errnoStr().c_str()
#define ERRNOSTRP(x) x->errnoStr().c_str()

#ifdef SOFTHSM_DAEMON_TEST
static void *temp_client_thread(void *arg)
{
	struct sockaddr_un srv;
	int size = 0;
	int count = 5;
	std::string clientName;
	std::string serverName;
	char *msg;

	INFO_MSG("Starting temp_client_thread\n");

	if (!Configuration::i()->reload(SimpleConfigLoader::i()))
	{
		return (void *)NULL;
	}

	serverName = Configuration::i()->getString("sockets.serversock", DEFAULT_SERVERSOCK);

	if (serverName == std::string("")) {
		cout << "Can't find sockets.serversock from configuration file" << endl;
		return (void *)NULL;
	}

	clientName = Configuration::i()->getString("sockets.clientsock", DEFAULT_CLIENTSOCK);

	if (clientName == std::string("")) {
		cout << "Can't find sockets.clientsock from configuration file" << endl;
		return (void *)NULL;
	}

	UnixDatagramSocket s(clientName);

	sleep(1);

	if (format_C_Initialize_Req_msg (0, (msg_t **) &msg) < 0) {
		ERROR_MSG("format_C_Initialize_Req_msg failed\n");
		pthread_exit((void *) -1);
	}

	size = get_msg_length(msg);

	DEBUG_MSG ("%s, sending %d bytes for C_Initialize_Req\n",
			__func__, size);

	int retval = s.sendData(msg, size, serverName);
	if (retval < 0) {
		ERROR_MSG("Can't send data to server (%s)\n", ERRNOSTR(s));
		return 0;
	}
	free (msg);

	retval = s.waitData(size);
	DEBUG_MSG ("%s: received data of size %d\n", __func__, size);

	msg = (char *) malloc(size);

	size = s.recvData(msg, size, &srv);

	parse_C_Initialize_Rsp_msg ((uint32_t *) &retval, (msg_t *) msg);

	DEBUG_MSG ("%s: resp %d size %d from %s\n", __func__, retval, size, srv.sun_path);
	free (msg);

	if (format_C_Finalize_Req_msg (0, (msg_t **) &msg) < 0) {
		ERROR_MSG("format_C_Finalize_Req_msg failed\n");
		pthread_exit((void *) -1);
	}

	size = get_msg_length(msg);

	DEBUG_MSG ("%s, sending %d bytes for C_Finalize_Req\n",
			__func__, size);

	s.sendData(msg, size, &srv);
	free (msg);

	retval = s.waitData(size);
	DEBUG_MSG ("%s: received data of size %d\n", __func__, size);

	msg = (char *) malloc(size);

	size = s.recvData(msg, size, &srv);

	parse_C_Finalize_Rsp_msg ((uint32_t *) &retval, (msg_t *) msg);
	DEBUG_MSG ("%s: resp %d size %d from %s\n", __func__, retval, size, srv.sun_path);
	free (msg);

	pthread_exit(0);
}
#endif

typedef struct thread_info {
	struct sockaddr_un clientAddr;
	MsgBuf *pbuf;
} thread_info_t;

static	pthread_mutex_t service_mutex = PTHREAD_MUTEX_INITIALIZER;

static void *service_thread(void *arg)
{
	int dataLen;
	bool init = true;
	std::string socketName;
	int recvBufLength;
	int max_count = 5;
	bool finalize = false;
	CK_RV rv;
	pthread_mutex_t mutex;
	int retval;

	/* Create a softHSM instance */
	void *hsm = createHSM();

	if (!hsm) {
		cout << "Can't create softHSM instance" << endl;
		pthread_exit((void *)-1);
	}

	INFO_MSG("SoftHSM daemon version = %d", Configuration::i()->getInt("secstore.version", 2));

	socketName = Configuration::i()->getString("sockets.serversock", DEFAULT_SERVERSOCK);
	if (socketName == std::string("")) {
		cout << "Can't find sockets.serversock from configuration file" << endl;
		if (hsm) deleteHSM(hsm);
		pthread_exit((void *)-1);
	}
	ostringstream thread_id;
	thread_id << pthread_self();
	socketName += ".";
	socketName += thread_id.str();

	INFO_MSG("Starting with service thread %s\n", socketName.c_str());

	/* Unpack the arguments */
	thread_info_t *tinfo = (thread_info_t *)arg;
	struct sockaddr_un clientAddr = tinfo->clientAddr;

	msgbuf_auto pbuf(new MsgBuf(tinfo->pbuf->length));
	memcpy (pbuf->data, tinfo->pbuf->data, tinfo->pbuf->length);

	delete tinfo->pbuf;
	delete tinfo;

	UnixDatagramSocket srvSock(socketName);

	if (srvSock.errnoVal) {
		ERROR_MSG ("Socket open failed for %s (%s)\n", socketName.c_str(), ERRNOSTR(srvSock));
		goto close_n_exit;
	}
#if 0
	// Initialize the library
	rv = C_Initialize(hsm, NULL_PTR);
	if (rv != CKR_OK)
	{
		ERROR_MSG ("Could not initialize PKCS11 library for %s\n", socketName.c_str());
		goto close_n_exit;
	}
#endif
	do {
		if (!init)  {

			if (srvSock.waitData(recvBufLength) < 0) {
				ERROR_MSG ("waitData returned error %d\n", srvSock.errnoVal);
				break;
			}

			if (recvBufLength < 0) {
				ERROR_MSG ("waitData returned invalid packet length error %d\n", srvSock.errnoVal);
				continue;
			}
			pbuf = (msgbuf_auto) new MsgBuf((unsigned int) recvBufLength);
			dataLen = srvSock.recvData(pbuf->data, pbuf->length);
			if (dataLen <= 0) {
				cout << "Can read from server socket "<< srvSock.errnoStr() << endl;
				goto close_n_exit;
			}
		} else {
			init = false;
		}

		pthread_mutex_lock(&service_mutex);
		retval = process_pkcs11_daemon_message(hsm, pbuf, finalize);
		pthread_mutex_unlock(&service_mutex);

		if (!retval)
			break;

		srvSock.sendData (pbuf->data, pbuf->length, &clientAddr);

		if (finalize) break;

	} while (1);

close_n_exit:

	if (hsm) deleteHSM(hsm);
	INFO_MSG("Done with service thread %s\n", socketName.c_str());
	pthread_exit(0);

	return 0;
}

int main(int argc, char *argv[])
{
	int rc, recvBufLength, dataLen;
	int retVal, signal, signalFD = -1;
	int index, argIndex = 0;
	bool kill = false, daemonize = true;
	bool stdoutlogging = false;
	pid_t pid;
	pthread_t tid;
	pthread_attr_t attr;
	thread_info_t *tinfo = 0;
	std::string socketName;
	UnixDatagramSocket *srvSock = NULL;

	const struct option longopts[] =
	{
		{"nodaemon",	no_argument,	0,	'n'},
		{"kill",	no_argument,	0,	'k'},
		{"help",	no_argument,	0,	'h'},
		{"verbose",	no_argument,	0,	'v'},
		{0,0,0,0},
	};

	opterr = 1;

	while(argIndex != -1)
	{
		argIndex = getopt_long(argc, argv, "nkhv", longopts, &index);
		switch (argIndex)
		{
			case 'n':
				daemonize = false;
				break;
			case 'k':
				kill = true;
				break;
			case 'v':
				softHSMLogStdOut(true);
				break;
			case 'h':
			default:
				printf ("Usage: argv[0] [OPTION]...\n\n"
						" Following are the command line options\n\n"
						" -n, --nodaemon\t do not daemonize, run in foreground\n"
						" -k, --kill\t kill the existing daemon\n"
						" -h, --help\t print this message\n"
						" -v, --verbose\t log to console also"
						"\n In default mode it will run as a daemon.\n\n");
				return 0;

			case -1:
				break;
		}
	}

	if (daemonize) {

		syslog(LOG_INFO, "Starting %s", argv[0]);

		/* Reset signal handlers */
		if (daemon_reset_sigs(-1) < 0) {
			syslog(LOG_ERR, "Failed to reset all signal handlers: %s", strerror(errno));
			return 1;
		}

		/* Unblock signals */
		if (daemon_unblock_sigs(-1) < 0) {
			syslog(LOG_ERR, "Failed to unblock all signals: %s", strerror(errno));
			return 1;
		}

		/* set daemon id string */
		daemon_log_ident = daemon_ident_from_argv0(argv[0]);
		daemon_pid_file_ident = daemon_log_ident;

		if (kill) {
			if (daemon_pid_file_kill_wait(SIGTERM, 5) < 0)
				syslog(LOG_WARNING, "Failed to kill daemon: %s", strerror(errno));
			return 0;
		}

		/* Single instance */
		if ((pid = daemon_pid_file_is_running()) >= 0) {
			syslog(LOG_ERR, "Daemon already running on PID file %u", pid);
			return 1;
		}
		if (daemon_retval_init() < 0) {
			syslog(LOG_ERR, "Failed to create pipe.");
			return 1;
		}

		/* Do the fork */
		if ((pid = daemon_fork()) < 0) {

			daemon_retval_done();
			syslog(LOG_INFO, "Error in daemon fork %s", strerror(errno));
			return 1;

		} else if (pid) { /* The parent */
			int ret;

			/* Wait for 20 seconds for the return value passed from the daemon process */
			if ((ret = daemon_retval_wait(20)) < 0) {
				syslog(LOG_ERR, "Could not recieve return value from daemon process: %s", strerror(errno));
				return -1;
			}
			if (!ret)
				syslog(LOG_ERR, "Daemon returned %i as return value.", ret);

			return ret;
		}

		/* Close FDs */
		if (daemon_close_all(-1) < 0) {
			syslog(LOG_ERR, "Failed to close all file descriptors: %s", strerror(errno));

			/* Send the error condition to the parent process */
			daemon_retval_send(1);
			goto close_n_exit;
		}

		/* Create the PID file */
		if (daemon_pid_file_create() < 0) {
			syslog(LOG_ERR, "Could not create PID file (%s).", strerror(errno));
			daemon_retval_send(2);
			goto close_n_exit;
		}

		/* Initialize signal handling */
		if (daemon_signal_init(SIGINT, SIGTERM, SIGQUIT, SIGHUP, 0) < 0) {
			syslog(LOG_ERR, "Could not register signal handlers (%s).", strerror(errno));
			daemon_retval_send(3);
			goto close_n_exit;
		}

		/* Send OK to parent process */
		daemon_retval_send(0);

		signalFD = daemon_signal_fd();
	}

#ifdef SOFTHSM_DAEMON_TEST
	rc = pthread_create(&tid, NULL, 
			temp_client_thread, NULL);
	if (rc){
		syslog(LOG_ERR, "Error:unable to create thread %d", rc);
		goto close_n_exit;
	}
#endif

	if (!Configuration::i()->reload(SimpleConfigLoader::i()))
	{
		syslog(LOG_ERR, "Can't reload SimpleConfigLoader");
		goto close_n_exit;
	}

	socketName = Configuration::i()->getString("sockets.serversock", DEFAULT_SERVERSOCK);
	if (socketName == std::string("")) {
		syslog(LOG_ERR, "Can't find sockets.serversock from configuration file");
		goto close_n_exit;
	}

	srvSock = new UnixDatagramSocket(socketName);
	if (srvSock->errnoVal) {
		syslog(LOG_ERR, "Can't create datagram socket for %s (%s)", socketName.c_str(), ERRNOSTRP(srvSock));
		goto close_n_exit;
	}

	for(;;) {
		tinfo = new(thread_info_t)();
		if (!tinfo) {
			syslog(LOG_ERR, "Can't allocate memory for tinfo %s", strerror(errno));
			goto close_n_exit;
		}

		retVal = srvSock->waitData(recvBufLength, NULL, signalFD);

		if ((daemonize) && (retVal == 1)) {
			int quit = 0;
			signal = daemon_signal_next();
			if (signal <= 0) continue;
			syslog(LOG_ERR, "Received signal %d", signal);
			/* Dispatch signal */
			switch(signal) {
				case SIGHUP:
				syslog(LOG_INFO, "Ignoring SIGHUP");
				break;

				default:
				quit = 1;
				break;
			}

			if (quit)
				goto close_n_exit;
			else
				continue;
		}

		if (retVal < 0) {
			syslog(LOG_ERR, "waitData returned error %d for server", srvSock->errnoVal);
			break;
		}

		if (recvBufLength < 0) {
			syslog(LOG_ERR, "waitData returned invalid packet length error %d", srvSock->errnoVal);
			continue;
		}

		tinfo->pbuf = new MsgBuf(recvBufLength+1);

		dataLen = srvSock->recvData(tinfo->pbuf->data, tinfo->pbuf->length, &tinfo->clientAddr);
		if (dataLen <= 0) {
			syslog(LOG_ERR, "Can't read from server socket %s", strerror(srvSock->errnoVal));
			goto close_n_exit;
		}
		tinfo->pbuf->data[tinfo->pbuf->length] = '\0';

		syslog(LOG_INFO, "server msg, length %d from %s",
				tinfo->pbuf->length, tinfo->clientAddr.sun_path);
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		// Spawn a thread, pass client_fd to it for further processing
		rc = pthread_create(&tid, &attr,
				service_thread, (void *) tinfo);
		if (rc){
			syslog(LOG_ERR, "Error:unable to create thread %d", rc);
			goto close_n_exit;
		}
	}

close_n_exit:
	syslog(LOG_ERR, "Exiting softhsm daemon");
	if (daemonize) {
		daemon_retval_send(255);
		daemon_signal_done();
		daemon_pid_file_remove();
	}
	pthread_mutex_destroy(&service_mutex);
	if (srvSock) delete srvSock;
	return 0;
}
