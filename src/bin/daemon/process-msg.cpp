#include "pkcs11_msg_daemon.h"
#include "msg_deserialize.h"
#include "format_rsp_msg.h"
#include "parse_req_msg.h"
#include "log.h"

#define add_case(x) \
case MSG_ID_ ## x: \
{ \
	retval = parse_ ## x ## _msg(hsm, (msg_t *)pbuf->data, \
			(msg_t **)&mb.data); \
	if (retval) { \
		ERROR_MSG ("parse_" #x "_msg returned error (%d)\n", \
				retval); \
		goto return_error; \
	} \
	mb.length = get_msg_length(mb.data); \
} \
break

int get_msg_length (char *msg)
{
	msg_hdr_t hdr = * (msg_hdr_t *)msg;
	msg_hdr_deserialize(&hdr);
	return (sizeof(msg_hdr_t) + hdr.len);
}

int process_pkcs11_daemon_message (void *hsm, msgbuf_auto &pbuf, bool &finalize)
{
	MsgBuf mb;
	int retval;
	msg_hdr_t hdr;

	if (parse_Req_msg_hdr(pbuf->data, pbuf->length, &hdr))
	{
		ERROR_MSG ("invalid message header received\n");
		goto return_error;
	}

	if (hdr.id == MSG_ID_C_Finalize_Req) finalize = true;

	switch (hdr.id) {

		add_case(C_Initialize_Req);
		add_case(C_Finalize_Req);
		add_case(C_GetInfo_Req);
		add_case(C_GetSlotList_Req);
		add_case(C_GetSlotInfo_Req);
		add_case(C_GetTokenInfo_Req);
		add_case(C_GetMechanismList_Req);
		add_case(C_GetMechanismInfo_Req);
		add_case(C_InitToken_Req);
		add_case(C_InitPIN_Req);
		add_case(C_SetPIN_Req);
		add_case(C_OpenSession_Req);
		add_case(C_CloseSession_Req);
		add_case(C_CloseAllSessions_Req);
		add_case(C_GetSessionInfo_Req);
		add_case(C_Login_Req);
		add_case(C_Logout_Req);
		add_case(C_CreateObject_Req);
		add_case(C_DestroyObject_Req);
		add_case(C_GetAttributeValue_Req);
		add_case(C_SetAttributeValue_Req);
		add_case(C_FindObjectsInit_Req);
		add_case(C_FindObjects_Req);
		add_case(C_FindObjectsFinal_Req);
		add_case(C_EncryptInit_Req);
		add_case(C_Encrypt_Req);
		add_case(C_EncryptUpdate_Req);
		add_case(C_EncryptFinal_Req);
		add_case(C_DecryptInit_Req);
		add_case(C_Decrypt_Req);
		add_case(C_DecryptUpdate_Req);
		add_case(C_DecryptFinal_Req);
		add_case(C_SignInit_Req);
		add_case(C_Sign_Req);
		add_case(C_SignUpdate_Req);
		add_case(C_SignFinal_Req);
		add_case(C_VerifyInit_Req);
		add_case(C_Verify_Req);
		add_case(C_VerifyUpdate_Req);
		add_case(C_VerifyFinal_Req);
		add_case(C_GenerateKey_Req);

		default:
			ERROR_MSG ("invalid message id %d\n", hdr.id);
			goto return_error;

	}

	pbuf = (msgbuf_auto) new MsgBuf(mb);

	return true;

return_error:
	retval = format_C_Error_Rsp_msg (CKR_GENERAL_ERROR, (msg_t **) &mb.data);
	if (retval) {
		ERROR_MSG ("format_C_Error_Rsp_msg returned error \n");
		return false;
	}
	mb.length = get_msg_length(mb.data);

	pbuf = (msgbuf_auto) new MsgBuf(mb);

	return true;
}

#ifdef SOFTHSM_DAEMON_TEST

int format_C_Initialize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Initialize_Req_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Initialize_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Initialize_Req.reserved = reserved;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Initialize_Req_msg_serialize(&(pmsg->body.C_Initialize_Req));
	*msg = pmsg;
	return 0;
}

int format_C_Finalize_Req_msg
(
	uint32_t	reserved,
	msg_t	**msg
)
{
	msg_t *pmsg;
	int len = sizeof(struct C_Finalize_Req_msg);

	pmsg = (msg_t *) malloc(len + sizeof(msg_hdr_t));
	if (pmsg == NULL)
	{
		return -1;
	}

	pmsg->hdr.id = MSG_ID_C_Finalize_Req;
	pmsg->hdr.len = len;
	pmsg->body.C_Finalize_Req.reserved = reserved;
	msg_hdr_serialize(&(pmsg->hdr));
	C_Finalize_Req_msg_serialize(&(pmsg->body.C_Finalize_Req));
	*msg = pmsg;
	return 0;
}

int parse_C_Initialize_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Initialize_Rsp_msg_deserialize(&(msg->body.C_Initialize_Rsp));
	*retval = msg->body.C_Initialize_Rsp.retval;
	return 0;
}

int parse_C_Finalize_Rsp_msg
(
	uint32_t	*retval,
	msg_t	*msg
)
{
	C_Finalize_Rsp_msg_deserialize(&(msg->body.C_Finalize_Rsp));
	*retval = msg->body.C_Finalize_Rsp.retval;
	return 0;
}

#endif
