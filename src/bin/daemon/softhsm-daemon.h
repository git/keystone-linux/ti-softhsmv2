/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _SOFTHSM_V2_SOFTHSM_DAEMON_H
#define _SOFTHSM_V2_SOFTHSM_DAEMON_H

//#define SOFTHSM_DAEMON_TEST

#include <stdio.h>
#include "cryptoki.h"

#ifdef __cplusplus
#define msgbuf_auto std::auto_ptr<MsgBuf>
#include <memory>
#endif

#define printf(...)

class MsgBuf
{
public:
	void bufReset()
	{
		data = 0;
		length = 0;
	}

	MsgBuf(unsigned int size = 0)
	{
		data = (size) ? new char[size]() : (char *) 0;
		length = (data) ? size : 0;
		printf ("Allocated %d bytes\n", size);
	}

	MsgBuf(MsgBuf &mb)
	{
		data = mb.data;
		length = mb.length;
		mb.bufReset();
		printf ("Reset to %d bytes\n", length);
	}

	~MsgBuf()
	{
		if (length) delete []data;
		printf ("Freed %d bytes\n", length);
	}

	char		*data;
	unsigned int	length;
};

#undef printf

#endif // !_SOFTHSM_V2_SOFTHSM_DAEMON_H
