#!/usr/bin/env python

# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Input format: 128 bit or 16 bytes of AES key in ascii or binary format
# Output format: 'securedb' + key + checksum of the key

import os, getopt, sys, struct, binascii

def usage():
	print ""
	print "Usage: " + sys.argv[0] + " [options]..."
	print "Converts or prints AES 128bit Key file to the format"
	print "required by softHSM daemon."
	print ""
	print " -i, --input=<fname>	input key file name"
	print " -o, --output=<fname>	output file name"
	print " -b, --binary		input file is in binary format"
	print "			(default ascii)"
	print " -v, --verbose		print the output"
	print "			note: verbose is true when output"
	print "			filename is not present"
	print " -g, --generate		generate a random AES key and pack it"
	print " -h, --help		print this message"
	print "Note: Both -i and -g should not be present together"
	print ""

def main():

	# AES 128 bit key
	keysize = 128/8
	crcsize = 4

	header = 'securedb'

	try:
		opts, args = getopt.getopt(sys.argv[1:],
				"hi:o:vbg", ["help", "input=", "output=", "verbose",
					"binary", 'generate'])
	except getopt.GetoptError as err:
		print str(err)
		usage()
		sys.exit(1)
	outfile = None
	verbose = False
	binary = False
	infile = None
	generate = False

	for o, a in opts:
		if o in ("-v", "--verbose"):
			verbose = True
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-o", "--output"):
			outfile = a
		elif o in ("-i", "--input"):
			infile = a
		elif o in ("-b", "--binary"):
			binary = True
		elif o in ("-g", "--generate"):
			generate = True
		else:
			print "Error: Input filename not present"
			usage()
			sys.exit()

	if infile != None and generate == True:
		print "Error: -i and -g are present together"
		usage()
		sys.exit()

	if infile == None and generate == False:
		print "Error: Input filename not present"
		usage()
		sys.exit()

	if generate == True:
		keyfiledata = os.urandom(keysize)
	else:
		if binary == True:
			with open(infile, "rb") as fin:
				keyfiledata = fin.read(keysize)
				fin.close()
		else:
			with open(infile, "r") as fin:
				keyfiledata = binascii.unhexlify(fin.read(keysize*2))
				fin.close()

	keydata = struct.unpack("i"*(keysize/crcsize), keyfiledata)

	outbuf = [keydata[0]]
	crcdata = keydata[0]
	for i in range(1, keysize/crcsize):
		outbuf.append(keydata[i])
		crcdata ^= keydata[i]
	outbuf.append(crcdata)

	outbufpack = struct.pack("<" + "s" * len(header), *header)
	outbufpack += struct.pack("<" + "i" * (keysize/crcsize + 1), *outbuf)

	if outfile != None:
		with open(outfile, "wb") as fout:
			fout.write(outbufpack)
			fout.close()

	if verbose == True or outfile == None:
		print "output: ", binascii.hexlify(outbufpack)

if __name__ == "__main__":
	main()

